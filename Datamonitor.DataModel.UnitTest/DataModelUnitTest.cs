using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.Providers;
using Datamonitor.DataModel.Structures;
using NUnit.Mocks;
using Datamonitor.DataModel.DataAccess;

namespace Datamonitor.DataModel.UnitTest
{
    /// <summary>
    /// Provides asserting methods for testing with nunit
    /// </summary>
    [TestFixture]
    public class DataModelUnitTest
    {
        private DynamicMock repositoryMock;
        private IRepository repository;
        private List<User> usersList;
        private List<User> gkUsersList;
        private Session session;
        private Company company;
        private Contract contract;
        private List<IPAddressRange> ipRanges;
        private List<ContractService> services;
        private Password password;
        private Signature signature;

        private const int USER_LIST_SIZE = 3;
        private const int IPRANGE_LIST_SIZE = 3;
        private const int SERVICE_LIST_SIZE = 2;
        private const double SESSION_EXPIRY_DAYS = 30;
        private const int SIGN_IN_TIME_OUT_MINUTES = 30;
        private const int MAX_SIGNIN_ATTEMPTS = 3;

        private User userOne = new User();
        private User userTwo = new User();
        private User userThree = new User();
        private Guid contractGuid = Guid.NewGuid();
        private Guid sessionID = Guid.NewGuid();
        private DateTime sessionCreateDate = DateTime.Now;
        private Guid companyGuid = Guid.NewGuid();
        private Guid signatureGuid = Guid.NewGuid();
        private IPAddressRange ipRangeOne;
        private IPAddressRange ipRangeTwo;
        private ContractService serviceOne;
        private ContractService serviceTwo;

        [SetUp]
        public void SetUp()
        {
            repositoryMock = new DynamicMock(typeof(IRepository));

            serviceOne = new ContractService();
            serviceOne.Identifier = Guid.NewGuid().ToString("B");
            serviceOne.Name = "serviceOne";
            serviceOne.IsActive = true;
            serviceOne.IsServiceActive = true;
            serviceTwo = new ContractService();
            serviceTwo.Identifier = Guid.NewGuid().ToString("B");
            serviceTwo.Name = "serviceTwo";
            serviceTwo.IsActive = true;
            serviceTwo.IsServiceActive = true;
            services = new List<ContractService>(SERVICE_LIST_SIZE);
            services.Add(serviceOne);
            services.Add(serviceTwo);

            signature = new Signature(signatureGuid);
            signature.DateCreated = DateTime.Now;
            signature.UserName = "user2Test";
            
            password = new Password("hash", "salt");
            password.LastSignInAttempt = DateTime.Now;
            password.SignInAttempts = MAX_SIGNIN_ATTEMPTS;

            ipRangeOne.LowerBound = 127000000001;
            ipRangeOne.LowerBound = 127000000100;
            ipRangeTwo.LowerBound = 192168000001;
            ipRangeTwo.LowerBound = 192168000010;
            ipRanges = new List<IPAddressRange>(IPRANGE_LIST_SIZE);
            ipRanges.Add(ipRangeOne);
            ipRanges.Add(ipRangeTwo);

            contract = new Contract();
            contract.Identifier = contractGuid;
            
            userOne.Username = "user1Test";
            userOne.Guid = Guid.NewGuid();
            userOne.ContractID = contractGuid;
            userTwo.Username = "user2Test";
            userTwo.Guid = Guid.NewGuid();
            userTwo.ContractID = contractGuid;
            userTwo.Password = password;
            userThree.Username = "user3Test";
            userThree.Guid = Guid.NewGuid();
            userThree.ContractID = contractGuid;
            userThree.IsGatekeeper = true;

            usersList = new List<User>(USER_LIST_SIZE);
            usersList.Add(userOne);
            usersList.Add(userTwo);
            usersList.Add(userThree);

            gkUsersList = new List<User>(1);
            gkUsersList.Add(userThree);

            session = new Session(sessionID);
            session.CreateDate = sessionCreateDate;
            session.ExpiryDate = sessionCreateDate.AddDays(DataModelUnitTest.SESSION_EXPIRY_DAYS);
            session.IsExpired = false;

            company = new Company();
            company.Identifier = companyGuid;
            company.IsActive = true;

            
        }

        [Test]
        public void Session_Get_Test()
        { 
            repositoryMock.ExpectAndReturn("GetSession", session, sessionID);
            repository = (IRepository)repositoryMock.MockInstance;

            DataService dataService = new DataService(repository);
            Session newSession = dataService.GetSession(sessionID);

            Assert.IsNotNull(session);
            Assert.AreEqual(session.Identifier, newSession.Identifier);
        }

        [Test]
        public void User_Get_Via_Contract_Test()
        {
            repositoryMock.ExpectAndReturn("GetUsers", usersList.ToArray(), contractGuid);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            User[] users = dataService.GetUsers(contractGuid);
            Assert.IsNotNull(users);
            Assert.AreEqual(users.Length, USER_LIST_SIZE);
        }

        [Test]
        public void User_Get_Test()
        {
            repositoryMock.ExpectAndReturn("GetUser", userOne, "user1Test");
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);
            
            User user = dataService.GetUser("user1Test");
            Assert.IsNotNull(user);
        }

        [Test]
        public void Gatekeepers_Get_Test()
        {
            repositoryMock.ExpectAndReturn("GetGatekeepers", gkUsersList.ToArray(), contractGuid);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            User[] gkUsers = dataService.GetGatekeepers(contractGuid);            
            Assert.IsNotNull(gkUsers);
            Assert.IsNotEmpty(gkUsers);
        }

        [Test]
        public void Get_Contract_Test()
        {
            repositoryMock.ExpectAndReturn("GetContract", contract, contractGuid);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            Contract newContract = dataService.GetContract(contractGuid);
            Assert.IsNotNull(newContract);
            Assert.AreEqual(contract.Identifier, newContract.Identifier);
        }

        [Test]
        public void Get_Company_Test()
        {
            repositoryMock.ExpectAndReturn("GetCompany", company, companyGuid);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            Company newCompany = dataService.GetCompany(companyGuid);
            Assert.IsNotNull(company);
            Assert.AreEqual(company.Identifier, newCompany.Identifier);
        }

        [Test]
        public void Get_IPRanges_Test()
        {
            repositoryMock.ExpectAndReturn("GetIPRanges", ipRanges.ToArray(), contractGuid);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            IPAddressRange[] newRanges = dataService.GetIPRanges(contractGuid);

            Assert.IsNotNull(newRanges);
            Assert.IsNotEmpty(newRanges);
        }

        [Test]
        public void Get_Services_Test()
        {
            repositoryMock.ExpectAndReturn("GetContractServices", services.ToArray(), contractGuid);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            ContractService[] newServices = dataService.GetContractServices(contractGuid);

            Assert.IsNotNull(newServices);
            Assert.IsNotEmpty(newServices);
        }

        [Test]
        public void User_SignIn_Locked()
        {
            repositoryMock.ExpectAndReturn("GetUser", userTwo, "user2Test");
            repositoryMock.ExpectAndReturn("IsUserSignInLocked", true, userTwo, MAX_SIGNIN_ATTEMPTS, SIGN_IN_TIME_OUT_MINUTES);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            bool isLocked = dataService.IsUserSignInLocked(userTwo, MAX_SIGNIN_ATTEMPTS, SIGN_IN_TIME_OUT_MINUTES);

            Assert.IsTrue(isLocked);            
        }

        [Test]
        public void Get_Signature_Test()
        {
            repositoryMock.ExpectAndReturn("GetSignature", signature, signatureGuid);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            Signature newSignature = dataService.GetSignature(signatureGuid);

            Assert.IsNotNull(newSignature);
            Assert.AreEqual(newSignature.Identifier, signature.Identifier);
        }      

        [Test]
        public void Persist_User_Test()
        {
            repositoryMock.ExpectAndReturn("PersistUser", userOne, userOne);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            User editedUser = dataService.PersistUser(userOne);

            Assert.IsNotNull(editedUser);
            Assert.AreEqual(userOne.Username, editedUser.Username);
        }
                
        [Test]
        public void Persist_User_Authentication_Test()
        {
            repositoryMock.ExpectAndReturn("PersistUserAuthentication", userOne, userOne);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            User editedUser = dataService.PersistUserAuthentication(userOne);

            Assert.IsNotNull(editedUser);
            Assert.AreEqual(userOne.Username, editedUser.Username);
        }

        [Test]
        public void Persist_Session_Test()
        {
            repositoryMock.ExpectAndReturn("PersistSession", session, session);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            Session editedSession = dataService.PersistSession(session);
            Assert.IsNotNull(editedSession);
            Assert.AreEqual(editedSession.Identifier, session.Identifier);
        }

        [Test]
        public void User_Update_Test()
        {
            repositoryMock.Expect("UpdateSignInCredentials", userThree, true);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            dataService.UpdateSignInCredentials(userThree.Username, true);

            Assert.IsNotNull(userThree);
        }

        [Test]
        public void Create_Signature_Test()
        {
            repositoryMock.ExpectAndReturn("CreateSignature", signature, signature);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            Signature newSig = dataService.CreateSignature(signature);

            Assert.IsNotNull(newSig);
        }

        [Test]
        public void Update_Signature_Test()
        {
            repositoryMock.ExpectAndReturn("UpdateSignature", signature, signature);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            Signature newSig = dataService.UpdateSignature(signature);

            Assert.IsNotNull(newSig);
        }

        [Test]
        public void Deactivate_User_Test()
        {
            repositoryMock.ExpectAndReturn("DeactivateUser", true, userTwo.Username, contractGuid);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            bool isDeactived = dataService.DeactivateUser(userTwo.Username, contractGuid);

            Assert.IsTrue(isDeactived);
        }

        [Test]
        public void Session_Kill_Test()
        {
            repositoryMock.Expect("DeactivateUser", session);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            dataService.KillSession(session);

            Assert.IsNotNull(session);
        }

        [Test]
        public void Sessions_For_User_Kill_Test()
        {
            repositoryMock.Expect("KillAllUserSessions", userThree);
            repository = (IRepository)repositoryMock.MockInstance;
            DataService dataService = new DataService(repository);

            dataService.KillAllUserSessions(userThree);

            Assert.IsNotNull(userThree);
        }
      
    }
}
