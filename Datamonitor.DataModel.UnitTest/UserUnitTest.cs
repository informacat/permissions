using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace Datamonitor.DataModel.UnitTest
{
    [TestFixture]
    public class UserUnitTest
    {
        User user;

        const string TEST_USERNAME = "NewTestUser";
        const string TEST_SALT = "TNewSalt";
        const string TEST_PASSWORD = "TestPassword";
        const string TEST_UPDATED_PASSWORD = "TestUpdatedPassword";

        [SetUp]
        public void SetUp()
        {
            user = new User();
            user.Username = TEST_USERNAME;
            user.Password = new Password(TEST_PASSWORD, TEST_SALT);
        }

        [Test]
        public void Password_Is_Valid()
        {
            Assert.IsTrue(user.Password == TEST_PASSWORD);
        }

        [Test]
        public void Password_Is_Not_Valid()
        {
            Assert.IsFalse(user.Password == TEST_UPDATED_PASSWORD);
        }
    }
}
