using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Datamonitor.DataModel.Interfaces;

namespace Datamonitor.DataModel.UnitTest
{
    [TestFixture]
    public class DatabaseRepositoryUnitTest
    {
        IRepository repository;

        const string TEST_USERNAME = "TestUser";
        const string TEST_SALT = "TestSalt";
        const string TEST_PASSWORD = "TestPassword";
        const string TEST_UPDATED_PASSWORD = "TestUpdatedPassword";

        [SetUp]
        public void Setup()
        {
            repository = new Datamonitor.DataModel.DataAccess.DatabaseRepository(); 
        }

        [Test]
        public void Inserts_New_User()
        {
            User user = new User();
            user.Username = TEST_USERNAME;
            user.Password = new Password(TEST_PASSWORD, TEST_SALT);

            repository.PersistUser(user);

            User DBUser = repository.GetUser(TEST_USERNAME);

            Assert.IsNotNull(DBUser);
        }

        [Test]
        public void Returns_A_User()
        {
            Assert.DoesNotThrow(delegate() { repository.GetUser(TEST_USERNAME); });

            User user = repository.GetUser(TEST_USERNAME);

            Assert.IsNotNull(user);
        }        

        [Test]
        public void Updates_User_Password()
        {
            User user = repository.GetUser(TEST_USERNAME);

            user.Password = new Password(TEST_UPDATED_PASSWORD, TEST_SALT);

            repository.PersistUser(user);

            User DBUser = repository.GetUser(TEST_USERNAME);

            Assert.IsTrue(DBUser.Password == TEST_UPDATED_PASSWORD);
        }

        [Test]
        public void Inserts_New_Session()
        {
            Session session = new Session();
            session.User = repository.GetUser(TEST_USERNAME);
            session.TimeOutDays = 30;

            repository.PersistSession(session);

            Assert.IsTrue(true);
        }

        [Test]
        public void Returns_A_Session()
        {
            Session session = repository.GetSession(new Guid("5F817633-30C3-483A-82E2-42992C96EF3E"));

            Assert.IsNotNull(session);
        }
    }
}
