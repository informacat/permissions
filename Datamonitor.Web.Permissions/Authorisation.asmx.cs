using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using Datamonitor.Permissions;
using System.Configuration;
using Datamonitor.DataModel;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.Permissions.Providers;
using Datamonitor.Permissions.Entities;

namespace Datamonitor.Web.Permissions
{
    /// <summary>
    /// Summary description for Authorisation
    /// </summary>
    [WebService(Namespace = "http://datamonitor.com/Authorisation", Description = "Webservice class provides authorisation methods to show access rights to services for their given restrictions")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class Authorisation : System.Web.Services.WebService
    {

        [WebMethod]
        public AuthorisationResult Authorise(string authenticationToken, int service, long ipAddress)
        {
            IAuthorisationService authService = AuthorisationProvider.GetServiceAuthorisationService(service.ToString(), ipAddress);
            return authService.Authorise(new Guid(authenticationToken));
        }

    }
}
