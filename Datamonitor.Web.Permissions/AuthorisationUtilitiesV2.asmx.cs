using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using Datamonitor.DataModel;
using System.Xml;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.Providers;
using Datamonitor.Permissions;
using Datamonitor.Data.Hashing;
using Datamonitor.DataModel.Structures;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Datamonitor.Web.Permissions
{
    /// <summary>
    /// Summary description for Container
    /// </summary>
    [WebService(Namespace = "http://datamonitor.com/AuthorisationUtilitiesV2", Name = "AuthorisationUtilitiesV2", Description = "Webservice class provides methods to obtain information about entities. This Webservice inherits from the original AuthorisationUtilities Webservice")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class AuthorisationUtilitiesV2 : AuthorisationUtilities
    {

        // VERSION 1 methods of the existing AuthorisationUtilities Web service can be found in AuthorisationUtilities.asmx.cs
        // VERSION 1 methods can be overridden here...

        // VERSION 2 Specific methods

        /// <summary>
        /// Method to return one or many User objects given a specific email address
        /// </summary>
        /// <param name="email">The email address to return User(s) for</param>
        /// <returns>One or many User objects</returns>
        [WebMethod]
        public User[] GetActiveUsersByEmail(string email)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            User[] users = dataService.GetActiveUsersByEmail(email);

            return users;
        }

        /// <summary>
        /// Method to return one or many User objects given a specific email address and array of Service IDs
        /// </summary>
        /// <param name="email">The email address to return User(s) for</param>
        /// <param name="serviceIds">An array of Service IDs to check against (i.e. return all Consumer KC users for email address X)</param>
        /// <returns>One or many User objects</returns>
        [WebMethod]
        public User[] GetActiveUsersByEmailAndService(string email, int[] serviceIds)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            User[] users = dataService.GetActiveUsersByEmailAndService(email, serviceIds);

            return users;
        }

        /// <summary>
        /// Method to return a value as a string based on the key passed in
        /// </summary>
        /// <param name="authenticationToken">session token of user</param>
        /// <param name="keyName">name of the key to return the value of</param>
        /// <returns>String value</returns>
        [WebMethod]
        public string GetSessionKeyValue(string authenticationToken, string keyName)
        {
            string sessionKeyValue = string.Empty;

            // check to see we've been passed all the right information
            if (authenticationToken.Length != 0 && keyName.Length != 0)
            {
                IDataService dataService = ServiceProvider.GetDataService();
                Session session = dataService.GetSession(new Guid(authenticationToken));
                sessionKeyValue = dataService.GetSessionKeyValue(session, keyName);
            }
         
            return sessionKeyValue;
        }

        /// <summary>
        /// Method to insert/update a name/value pair into the DB table based on a session
        /// </summary>
        /// <param name="authenticationToken">session token of user</param>
        /// <param name="keyName">name of the key to insert/update</param>
        /// <param name="keyValue">value of the key to insert/update</param>
        /// <returns>boolean</returns>
        [WebMethod]
        public bool InsertSessionKeyValue(string authenticationToken, string keyName, string keyValue)
        {
            bool insertSuccess = false;

            // check to see we've been passed all the right information
            if (authenticationToken.Length != 0 && keyValue.Length != 0 && keyName.Length != 0)
            {
                IDataService dataService = ServiceProvider.GetDataService();
                Session session = dataService.GetSession(new Guid(authenticationToken));
                insertSuccess = dataService.InsertSessionKeyValue(session, keyName, keyValue);
            }

            return insertSuccess;
        }
    }
}
