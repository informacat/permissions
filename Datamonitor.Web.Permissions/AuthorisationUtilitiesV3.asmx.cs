using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using Datamonitor.DataModel;
using System.Xml;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.Providers;
using Datamonitor.DataModel.Utilities;
using Datamonitor.Permissions;
using Datamonitor.Data.Hashing;
using Datamonitor.DataModel.Structures;
using System.Collections.Generic;
using System.Collections.Specialized;


namespace Datamonitor.Web.Permissions
{
    /// <summary>
    /// Summary description for AuthorisationUtilitiesV3
    /// </summary>
    [WebService(Namespace = "http://datamonitor.com/AuthorisationUtilitiesV3", Name = "AuthorisationUtilitiesV3", Description = "Webservice class provides methods to obtain information about entities. This Webservice inherits from the AuthorisationUtilitiesV2 (which in-turn inherits from the original AuthorisationUtilities) Webservice")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class AuthorisationUtilitiesV3 : AuthorisationUtilitiesV2
    {
        // VERSION 1 methods of the existing AuthorisationUtilities Web service can be found in AuthorisationUtilities.asmx.cs
        // VERSION 2 methods of the existing AuthorisationUtilitiesV2 Web service can be found in AuthorisationUtilitiesV2.asmx.cs
        // VERSION 1 & 2 methods can be overridden here...

        // VERSION 3 Specific methods


        /// <summary>
        /// Method to return one or many Contracts by a user's IP address
        /// </summary>
        /// <param name="ipAddress">The IP address to return Contract(s) for</param>
        /// <returns>One or many Contract objects</returns>
        [WebMethod]
        public Contract[] GetContractsByIPAddress(string ipAddress)
        {
            // firstly, convert the string to a long
            long correctedIpAddress = IPConverter.ParseIP(ipAddress);
            IDataService dataService = ServiceProvider.GetDataService();
            Contract[] contracts = dataService.GetContracts(correctedIpAddress);

            return contracts;
        }

        /// <summary>
        /// Method to return one or many Contracts by a username
        /// </summary>
        /// <param name="username">The username to return Contract(s) for</param>
        /// <returns>One or many Contract objects</returns>
        [WebMethod]
        public Contract[] GetContractByUsername(string username)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            Contract[] contracts = dataService.GetContractByUsername(username);

            return contracts;
        }

        /// <summary>
        /// Method to return one or many services by a user's IP address
        /// </summary>
        /// <param name="ipAddress">The IP address to return Service(s) for</param>
        /// <returns>One or many Service objects</returns>
        [WebMethod]
        public ContractService[] GetContractServicesByIPAddress(string ipAddress)
        {
            // firstly, convert the string to a long
            long correctedIpAddress = IPConverter.ParseIP(ipAddress);

            IDataService dataService = ServiceProvider.GetDataService();
            ContractService[] contractServices = dataService.GetContractServicesByIPAddress(correctedIpAddress);
            
            return contractServices;
        }

        /// <summary>
        /// Method to return one or many ACTIVE services by a user's IP address
        /// </summary>
        /// <param name="ipAddress">The IP address to return Active Service(s) for</param>
        /// <returns>One or many Service objects</returns>
        [WebMethod]
        public ContractService[] GetActiveContractServicesByIPAddress(string ipAddress)
        {
            // firstly, convert the string to a long
            long correctedIpAddress = IPConverter.ParseIP(ipAddress);

            IDataService dataService = ServiceProvider.GetDataService();
            ContractService[] contractServices = dataService.GetActiveContractServicesByIPAddress(correctedIpAddress);

            return contractServices;
        }

        /// <summary>
        /// Method to return one or many services by a username
        /// </summary>
        /// <param name="username">The username to return Service(s) for</param>
        /// <returns>One or many User objects</returns>
        [WebMethod]
        public ContractService[] GetContractServicesByUsername(string username)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            ContractService[] contractServices = dataService.GetContractServicesByUsername(username);

            return contractServices;
        }

        /// <summary>
        /// Method to return one or many ACTIVE services by a user's IP address
        /// </summary>
        /// <param name="username">The username to return ACTIVE Service(s) for</param>
        /// <returns>One or many User objects</returns>
        [WebMethod]
        public ContractService[] GetActiveContractServicesByUsername(string username)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            ContractService[] contractServices = dataService.GetActiveContractServicesByUsername(username);

            return contractServices;
        }

        /// <summary>
        /// Method to return one or many Contracts by a user's IP address - specific to the HC Services
        /// Stored Proc does a string match to ensure Contract has "DMHC -%" serviceNames assigned to it
        /// </summary>
        /// <param name="ipAddress">The IP address to return Contract(s) for</param>
        /// <returns>One or many Contract objects</returns>
        [WebMethod]
        public Contract[] GetContractsByIPAddressForHCServices(string ipAddress)
        {
            // firstly, convert the string to a long
            long correctedIpAddress = IPConverter.ParseIP(ipAddress);
            IDataService dataService = ServiceProvider.GetDataService();
            Contract[] contracts = dataService.GetContractsForHCServices(correctedIpAddress);

            return contracts;
        }

        /// <summary>
        /// Method to return one or many Contracts by a user's IP address - specific to the VERDICT Services
        /// Stored Proc does a string match to ensure Contract has "Verdict%" serviceNames assigned to it
        /// </summary>
        /// <param name="ipAddress">The IP address to return Contract(s) for</param>
        /// <returns>One or many Contract objects</returns>
        [WebMethod]
        public Contract[] GetContractsByIPAddressForVerdictServices(string ipAddress)
        {
            // firstly, convert the string to a long
            long correctedIpAddress = IPConverter.ParseIP(ipAddress);
            IDataService dataService = ServiceProvider.GetDataService();
            Contract[] contracts = dataService.GetContractsForVerdictServices(correctedIpAddress);

            return contracts;
        }

        [WebMethod]
        public bool CheckContractByStringIPAddress(Contract contract, string ipAddress)
        {
            // specific for HKC as they're having trouble converting the string to a long
            // check if there are any IP addresses stored against the contract in the IPAddressObject
            // DB table
            // if there are, then we need to see if our passed-in IP is between any of the range(s)
            // if it is then return the contract, if it's not, then return null and let the calling code handle it (outside Permissions)
            bool matchedIP = false;

            // firstly, convert the string to a long
            long correctedIpAddress = IPConverter.ParseIP(ipAddress);

            // loop round any possible range(s)
            foreach (IPAddressRange ipRange in contract.IPRanges)
            {
                if (ipRange.IsBetween(correctedIpAddress))
                {
                    matchedIP = true;
                    break;
                }
            }
            // if there were IP ranges against the contract
            if (contract.IPRanges.Length > 0 && !matchedIP)
            {
                // the passed-in IP didn't fall inside any of the range(s)
                return false;
            }
            else
            {
                // either there are no IP range(s) or there are range(s) and the passed-in IP falls between them
                return true;
            }
        }

        /// <summary>
        /// Method to return one or many ACTIVE services by a user's GUID (g_user_id) - primarily for Premium Tools as they only capture the userGUID in the SSO request
        /// </summary>
        /// <param name="username">The username to return ACTIVE Service(s) for</param>
        /// <returns>One or many ContractService objects</returns>
        [WebMethod]
        public ContractService[] GetActiveContractServicesByUserGUID(Guid userGUID)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            ContractService[] contractServices = dataService.GetActiveContractServicesByUserGUID(userGUID);

            return contractServices;
        }

        /// <summary>
        /// Method to return a User's details based on their GUID (primarily for Premium Tool usage)
        /// </summary>
        /// <param name="userGUID">The user's GUID</param>
        /// <returns>One or many ContractService objects</returns>
        [WebMethod]
        public User GetUserByUserGUID(Guid userGUID)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            return dataService.GetUserByUserGUID(userGUID);
        }

    }
}
