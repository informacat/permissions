using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using Datamonitor.DataModel;
using System.Xml;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.Providers;
using Datamonitor.Permissions;
using Datamonitor.Data.Hashing;
using Datamonitor.DataModel.Structures;

namespace Datamonitor.Web.Permissions
{
    /// <summary>
    /// Summary description for Container
    /// </summary>
    [WebService(Namespace = "http://datamonitor.com/AuthorisationUtilities", Description = "Webservice class provides methods to obtain information about entities")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class AuthorisationUtilities : System.Web.Services.WebService
    {
        [WebMethod]
        public bool IsUser(string username)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            try
            {
                bool userExists = dataService.CheckUserExists(username);

                return userExists;
            }
            catch (Exception) { return false; }
        }

        [WebMethod]
        public User GetUser(string username)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            return dataService.GetUser(username);
        }

        [WebMethod]
        public bool UpdateUserAutentication(string username, string password)
        {
            bool result = false;

            IDataService dataService = ServiceProvider.GetDataService();

            User user = dataService.GetUser(username);

            if (user == null)
            {
                user = new User();
                user.Username = username;
            }

            user.Password = new Password(password);

            User updatedUser = dataService.PersistUserAuthentication(user);

            if (updatedUser != null)
            {
                result = true;
            }

            return result;
        }

        [WebMethod]
        public User[] GetGatekeepers(Guid contractId)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            User[] gatekeepers = dataService.GetGatekeepers(contractId);

            return gatekeepers;
        }

        [WebMethod]
        public User[] GetUsers(Guid contractId)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            User[] users = dataService.GetUsers(contractId);

            return users;
        }

        [WebMethod]
        public bool UpdateUser(string username,
            string firstname,
            string lastname,
            string jobFunction,
            string address,
            string city,
            string postCode,
            string country,
            string telephone,
            string email,
            string salesContact,
            bool receiveRegularUpdates,
            bool isGatekeeper,
            bool isSharedPassword,
            Guid contractID,
            string jobTitle,
            string state,
            string fax,
            string title)
        {
            try
            {
                IDataService dataService = ServiceProvider.GetDataService();
                User user = dataService.GetUser(username);

                user.FirstName = firstname;
                user.LastName = lastname;
                user.JobFunction = jobFunction;
                user.Address = address;
                user.City = city;
                user.PostCode = postCode;
                user.Country = country;
                user.Telephone = telephone;
                user.Email = email;
                user.SalesContact = salesContact;
                user.ReceiveRegularUpdates = receiveRegularUpdates;
                user.IsGatekeeper = isGatekeeper;
                user.IsSharedPassword = isSharedPassword;
                user.ContractID = contractID;
                user.JobTitle = jobTitle;
                user.State = state;
                user.Fax = fax;
                user.Title = title;
                user.IsProfileUpdated = true;

                dataService.PersistUser(user);                

                return true;
            }
            catch (Exception) { return false; }
        }
        /// <summary>
        /// Updates user profile including interests
        /// </summary>
        /// <param name="username"></param>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="jobFunction"></param>
        /// <param name="address"></param>
        /// <param name="city"></param>
        /// <param name="postCode"></param>
        /// <param name="country"></param>
        /// <param name="telephone"></param>
        /// <param name="email"></param>
        /// <param name="salesContact"></param>
        /// <param name="receiveRegularUpdates"></param>
        /// <param name="isGatekeeper"></param>
        /// <param name="isSharedPassword"></param>
        /// <param name="contractID"></param>
        /// <param name="jobTitle"></param>
        /// <param name="state"></param>
        /// <param name="fax"></param>
        /// <param name="title"></param>
        /// <param name="interests"></param>
        /// <returns></returns>
        [WebMethod]
        public bool UpdateUserWithInterest(string username,
            string firstname,
            string lastname,
            string jobFunction,
            string address,
            string city,
            string postCode,
            string country,
            string telephone,
            string email,
            string salesContact,
            bool receiveRegularUpdates,
            bool isGatekeeper,
            bool isSharedPassword,
            Guid contractID,
            string jobTitle,
            string state,
            string fax,
            string title,
            string interests,
            int serviceId)
        {
            try
            {
                IDataService dataService = ServiceProvider.GetDataService();
                User user = dataService.GetUser(username);

                user.FirstName = firstname;
                user.LastName = lastname;
                user.JobFunction = jobFunction;
                user.Address = address;
                user.City = city;
                user.PostCode = postCode;
                user.Country = country;
                user.Telephone = telephone;
                user.Email = email;
                user.SalesContact = salesContact;
                user.ReceiveRegularUpdates = receiveRegularUpdates;
                user.IsGatekeeper = isGatekeeper;
                user.IsSharedPassword = isSharedPassword;
                user.ContractID = contractID;
                user.JobTitle = jobTitle;
                user.State = state;
                user.Fax = fax;
                user.Title = title;
                //user.Interests = interests; //Add to  User interests table
                user.IsProfileUpdated = true;

                dataService.PersistUser(user);
                //Update user interests
                dataService.SetUserInterests(user.Guid, serviceId, interests);

                return true;
            }
            catch (Exception) { return false; }
        }
       
        [WebMethod]
        public bool RegisterNewUserWithInterest(
            string username,
            string firstname,
            string lastname,
            string jobFunction,
            string address,
            string city,
            string postCode,
            string country,
            string telephone,
            string email,
            string salesContact,
            bool receiveRegularUpdates,
            bool isGatekeeper,
            bool isSharedPassword,
            string password,
            Guid contractID,
            bool isIPRestrict,
            bool isAccountActive,
            bool isChangePasswordNextLogin,
            bool isPressUser,
            int purchasedAmountToDate,
            int purchaseLimit,
            bool isBillingCodeRequired,
            string preferredCurrency,
            bool isNewUser,
            bool isAffiliateUser,
            bool isMarketingContact,
            bool isNotifyOfNewProducts,
            int userType,
            DateTime dateCreated,
            bool isAutoLogin,
            string preferredEmailFormat,
            string interests, //This field now holds the new requirement data (user's interest) BugzID:36104
            string sectorForURLRedirection,
            string jobTitle,
            string state,
            string fax,
            string title,
            int serviceId
            )
        {
        

            try
            {
                User user = new User();
                user.Guid = Guid.NewGuid();
                user.Username = username;
                user.FirstName = firstname;
                user.LastName = lastname;
                user.JobFunction = jobFunction;
                user.Address = address;
                user.City = city;
                user.PostCode = postCode;
                user.Country = country;
                user.Telephone = telephone;
                user.Email = email;
                user.SalesContact = salesContact;
                user.ReceiveRegularUpdates = receiveRegularUpdates;
                user.IsGatekeeper = isGatekeeper;
                user.IsSharedPassword = isSharedPassword;
                user.Password = new Password(password);
                user.ContractID = contractID;
                user.ReceiveRegularUpdates = receiveRegularUpdates;
                user.IsGatekeeper = isGatekeeper;
                user.IsSharedPassword = isSharedPassword;
                user.IsIPRestrict = isIPRestrict;
                user.IsAccountActive = isAccountActive;
                user.IsChangePasswordNextLogin = isChangePasswordNextLogin;
                user.PurchasedAmountToDate = purchasedAmountToDate;
                user.PurchaseLimit = purchaseLimit;
                user.IsBillingCodeRequired = isBillingCodeRequired;
                user.PreferredCurrency = preferredCurrency;
                user.IsNewUser = isNewUser;
                user.IsAffiliateUser = isAffiliateUser;
                user.IsPressUser = isPressUser;
                user.IsMarketingContact = isMarketingContact;
                user.IsNotifyOfNewProducts = isNotifyOfNewProducts;
                user.UserType = userType;
                user.DateCreated = dateCreated;
                user.IsAutoLogin = isAutoLogin;
                user.PreferredEmailFormat = preferredEmailFormat;
                user.Interests = string.Empty;
                user.SectorForURLRedirection = sectorForURLRedirection;
                user.JobTitle = jobTitle;
                user.State = state;
                user.Fax = fax;
                user.Title = title;
                user.IsProfileUpdated = true;

                IDataService dataService = ServiceProvider.GetDataService();

                dataService.PersistUser(user);
                dataService.SetUserInterests(user.Guid, serviceId, interests);
                dataService.PersistUserAuthentication(user);

                return true;
            }
            catch (Exception) { return false; }
        }

        [WebMethod]
        public bool RegisterNewUser(
            string username,
            string firstname,
            string lastname,
            string jobFunction,
            string address,
            string city,
            string postCode,
            string country,
            string telephone,
            string email,
            string salesContact,
            bool receiveRegularUpdates,
            bool isGatekeeper,
            bool isSharedPassword,
            string password,
            Guid contractID,
            bool isIPRestrict,
            bool isAccountActive,
            bool isChangePasswordNextLogin,
            bool isPressUser,
            int purchasedAmountToDate,
            int purchaseLimit,
            bool isBillingCodeRequired,
            string preferredCurrency,
            bool isNewUser,
            bool isAffiliateUser,
            bool isMarketingContact,
            bool isNotifyOfNewProducts,
            int userType,
            DateTime dateCreated,
            bool isAutoLogin,
            string preferredEmailFormat,
            string interests,
            string sectorForURLRedirection,
            string jobTitle,
            string state,
            string fax,
            string title
            )
        {
        

            try
            {
                User user = new User();
                user.Guid = Guid.NewGuid();
                user.Username = username;
                user.FirstName = firstname;
                user.LastName = lastname;
                user.JobFunction = jobFunction;
                user.Address = address;
                user.City = city;
                user.PostCode = postCode;
                user.Country = country;
                user.Telephone = telephone;
                user.Email = email;
                user.SalesContact = salesContact;
                user.ReceiveRegularUpdates = receiveRegularUpdates;
                user.IsGatekeeper = isGatekeeper;
                user.IsSharedPassword = isSharedPassword;
                user.Password = new Password(password);
                user.ContractID = contractID;
                user.ReceiveRegularUpdates = receiveRegularUpdates;
                user.IsGatekeeper = isGatekeeper;
                user.IsSharedPassword = isSharedPassword;
                user.IsIPRestrict = isIPRestrict;
                user.IsAccountActive = isAccountActive;
                user.IsChangePasswordNextLogin = isChangePasswordNextLogin;
                user.PurchasedAmountToDate = purchasedAmountToDate;
                user.PurchaseLimit = purchaseLimit;
                user.IsBillingCodeRequired = isBillingCodeRequired;
                user.PreferredCurrency = preferredCurrency;
                user.IsNewUser = isNewUser;
                user.IsAffiliateUser = isAffiliateUser;
                user.IsPressUser = isPressUser;
                user.IsMarketingContact = isMarketingContact;
                user.IsNotifyOfNewProducts = isNotifyOfNewProducts;
                user.UserType = userType;
                user.DateCreated = dateCreated;
                user.IsAutoLogin = isAutoLogin;
                user.PreferredEmailFormat = preferredEmailFormat;
                user.Interests = interests;
                user.SectorForURLRedirection = sectorForURLRedirection;
                user.JobTitle = jobTitle;
                user.State = state;
                user.Fax = fax;
                user.Title = title;
                user.IsProfileUpdated = true;

                IDataService dataService = ServiceProvider.GetDataService();

                dataService.PersistUser(user);
                dataService.PersistUserAuthentication(user);

                return true;
            }
            catch (Exception) { return false; }
        }

        [WebMethod]
        public bool RegisterNewUserWithRandomPassword(
            string username,
            string firstname,
            string lastname,
            string jobFunction,
            string address,
            string city,
            string postCode,
            string country,
            string telephone,
            string email,
            string salesContact,
            bool receiveRegularUpdates,
            bool isGatekeeper,
            bool isSharedPassword,
            Guid contractID,
            bool isIPRestrict,
            bool isAccountActive,
            bool isChangePasswordNextLogin,
            bool isPressUser,
            int purchasedAmountToDate,
            int purchaseLimit,
            bool isBillingCodeRequired,
            string preferredCurrency,
            bool isNewUser,
            bool isAffiliateUser,
            bool isMarketingContact,
            bool isNotifyOfNewProducts,
            int userType,
            DateTime dateCreated,
            bool isAutoLogin,
            string preferredEmailFormat,
            string interests,
            string sectorForURLRedirection,
            string jobTitle,
            string state,
            string fax,
            string title
            )
        {


            try
            {
                User user = new User();
                user.Guid = Guid.NewGuid();
                user.Username = username;
                user.FirstName = firstname;
                user.LastName = lastname;
                user.JobFunction = jobFunction;
                user.Address = address;
                user.City = city;
                user.PostCode = postCode;
                user.Country = country;
                user.Telephone = telephone;
                user.Email = email;
                user.SalesContact = salesContact;
                user.ReceiveRegularUpdates = receiveRegularUpdates;
                user.IsGatekeeper = isGatekeeper;
                user.IsSharedPassword = isSharedPassword;
                user.Password = new Password(HashUtilities.GeneratePassword());
                user.ContractID = contractID;
                user.ReceiveRegularUpdates = receiveRegularUpdates;
                user.IsGatekeeper = isGatekeeper;
                user.IsSharedPassword = isSharedPassword;
                user.IsIPRestrict = isIPRestrict;
                user.IsAccountActive = isAccountActive;
                user.IsChangePasswordNextLogin = isChangePasswordNextLogin;
                user.PurchasedAmountToDate = purchasedAmountToDate;
                user.PurchaseLimit = purchaseLimit;
                user.IsBillingCodeRequired = isBillingCodeRequired;
                user.PreferredCurrency = preferredCurrency;
                user.IsNewUser = isNewUser;
                user.IsAffiliateUser = isAffiliateUser;
                user.IsPressUser = isPressUser;
                user.IsMarketingContact = isMarketingContact;
                user.IsNotifyOfNewProducts = isNotifyOfNewProducts;
                user.UserType = userType;
                user.DateCreated = dateCreated;
                user.IsAutoLogin = isAutoLogin;
                user.PreferredEmailFormat = preferredEmailFormat;
                user.Interests = interests;
                user.SectorForURLRedirection = sectorForURLRedirection;
                user.JobTitle = jobTitle;
                user.State = state;
                user.Fax = fax;
                user.Title = title;
                user.IsProfileUpdated = true;

                IDataService dataService = ServiceProvider.GetDataService();

                dataService.PersistUser(user);
                dataService.PersistUserAuthentication(user);

                return true;
            }
            catch (Exception) { return false; }
        }

        [WebMethod]
        public Session GetSession(string authenticationToken)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            Session session = dataService.GetSession(new Guid(authenticationToken));

            return session;
        }

        [WebMethod]
        public Contract GetContract(Guid contractId)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            Contract contract = dataService.GetContract(contractId);

            return contract;
        }

        [WebMethod]
        public Contract GetContractByContractUsername(string username)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            Contract contract = dataService.GetContract(username);
            return contract;           
        }

        [WebMethod]
        public bool CheckContractByIPAddress(Contract contract, long ipAddress)
        {
            // check if there are any IP addresses stored against the contract in the IPAddressObject
            // DB table
            // if there are, then we need to see if our passed-in IP is between any of the range(s)
            // if it is then return the contract, if it's not, then return null and let the calling code handle it (outside Permissions)
            bool matchedIP = false;

            // loop round any possible range(s)
            foreach (IPAddressRange ipRange in contract.IPRanges)
            {
                if (ipRange.IsBetween(ipAddress))
                {
                    matchedIP = true;
                    break;
                }
            }
            // if there were IP ranges against the contract
            if (contract.IPRanges.Length > 0 && !matchedIP)
            {
                // the passed-in IP didn't fall inside any of the range(s)
                return false;
            }
            else
            {
                // either there are no IP range(s) or there are range(s) and the passed-in IP falls between them
                return true;
            }
        }

        [WebMethod]
        public ContractService[] GetContractServices(Guid contractId)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            ContractService[] contractServices = dataService.GetContractServices(contractId);

            return contractServices;
        }

        [WebMethod]
        public ContractService[] GetActiveContractServices(Guid contractId)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            ContractService[] contractServices = dataService.GetActiveContractServices(contractId);

            return contractServices;
        }

        [WebMethod]
        public Company GetCompany(Guid companyId)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            Company company = dataService.GetCompany(companyId);

            return company;
        }

        [WebMethod]
        public Signature GetSignature(Guid signatureId)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            Signature signature = dataService.GetSignature(signatureId);

            return signature;
        }

        [WebMethod]
        public void PersistSession(string authenticationToken)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            Session session = dataService.GetSession(new Guid(authenticationToken));
            session.TimeOutDays = AuthenticationConfiguration.AuthenticationServiceTimeoutDays;
            dataService.PersistSession(session);
        }
        /// <summary>
        /// Stores the created signature in DB
        /// </summary>
        /// <param name="userName"></param>
        /// <returns><Signature/returns>
        [WebMethod]
        public Signature CreateSignature(string userName)
        {
            IDataService dataService = ServiceProvider.GetDataService();

            Signature signature = new Signature();
            signature.UserName = userName;
            signature.TimeOutDays = AuthenticationConfiguration.SignatureTimeoutDays;
            signature.IsActive = true;
            return  dataService.CreateSignature(signature);

        }

      
        /// <summary>
        /// Stores updated signature in DB
        /// </summary>
        /// <param name="signature"></param>
        /// <returns></returns>
        [WebMethod]
        public Signature UpdateSignature(Signature signature)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            return dataService.UpdateSignature(signature);
        }

        [WebMethod]
        public bool KillSession(string authenticationToken)
        {
            try
            {
                IDataService dataService = ServiceProvider.GetDataService();
                Session session = dataService.GetSession(new Guid(authenticationToken));
                dataService.KillSession(session);

                return true;
            }
            catch (Exception) { return false; }
        }

        [WebMethod]
        public bool KillSessions(string username)
        {
            try
            {
                IDataService dataService = ServiceProvider.GetDataService();
                User user = dataService.GetUser(username);
                dataService.KillAllUserSessions(user);

                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        [WebMethod]
        public bool DeactivateUser(string username, Guid contractId)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            return dataService.DeactivateUser(username, contractId);
        }
        /// <summary>
        /// Gets the user interests associated with the user for the KC
        /// </summary>
        /// <param name="username"></param>
        /// <param name="kcname"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetUserInterests(Guid userId, int serviceId)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            string interests = dataService.GetUserInterests(userId, serviceId);

            return interests;
        }
        /// <summary>
        /// Adds/updates the interests for a particular user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="kcname"></param>
        /// <param name="interests"></param>
        [WebMethod]
        public void SetUserInterests(Guid userId, int serviceId, string interests)
        {
            IDataService dataService = ServiceProvider.GetDataService();
            dataService.SetUserInterests(userId, serviceId, interests);
        }
    }
}
