/*
	Script for creating procedures for permissions webservice.
*/
USE dmv_profiles
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAuthenticationGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.UserAuthenticationGet
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsGetUserAuthentication]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsGetUserAuthentication
GO

CREATE PROCEDURE dbo.pwsGetUserAuthentication
	@username				NVARCHAR(255)
AS
SELECT 
		u.*,
		a.hash AS 'PasswordHash', 
		a.salt AS 'PasswordSalt',
		a.[timeStamp] AS 'PasswordTimeStamp',
		a.signInAttempts AS 'SignInAttempts',
		a.lastSignInAttempt AS 'LastSignInAttempt'
FROM 
		dbo.UserAuthentication a RIGHT JOIN (
			dbo.UserAuthenticationLink l RIGHT JOIN dbo.UserObject u
				ON l.u_logon_name = u.u_logon_name
			) ON a.id = l.id
WHERE 
		u.u_logon_name = @username
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GatekeeperGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.GatekeeperGet
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsGetGatekeepersByContract]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsGetGatekeepersByContract
GO

CREATE PROCEDURE dbo.pwsGetGatekeepersByContract
	@contractId				UNIQUEIDENTIFIER
AS
SELECT 
		u.*	
FROM 
		dbo.UserObject u
WHERE 
		u.Contract_ID = @contractId
AND
		u.gatekeeperUser = 1
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UsersGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.UsersGet
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsGetActiveUsersByContract]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsGetActiveUsersByContract
GO

CREATE PROCEDURE dbo.pwsGetActiveUsersByContract
	@contractId				UNIQUEIDENTIFIER
AS
SELECT 
		u.*	
FROM 
		dbo.UserObject u
WHERE 
		u.Contract_ID = @contractId
AND
		u.i_account_status = 1
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsGetActiveSharedUsersByContract]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsGetActiveSharedUsersByContract
GO

CREATE PROCEDURE dbo.pwsGetActiveSharedUsersByContract
	@contractId				UNIQUEIDENTIFIER
AS
SELECT *	
FROM dbo.UserObject
WHERE Contract_ID = @contractId
	AND i_account_status = 1
	AND sharedPassword = 1
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SessionGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.SessionGet
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsGetSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsGetSession
GO

CREATE PROCEDURE dbo.pwsGetSession
	@sessionId			UNIQUEIDENTIFIER
AS
SELECT
		*,
		'IsExpired' = Case When ExpiryDate >= GetDate() Then 0 Else 1 End
FROM
		kcSession
WHERE
		sessionId = @sessionId
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContractInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.ContractInsert
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsInsertContract]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsInsertContract
GO

CREATE PROCEDURE dbo.pwsInsertContract
	@contractIdentifier				UNIQUEIDENTIFIER,
	@name							NVARCHAR(255),
	@status							NVARCHAR(255),
	@startDate						DATETIME = NULL,
	@endDate						DATETIME = NULL,
	@companyIdentifier				UNIQUEIDENTIFIER,
	@isSSOContract					BIT = 0,
	@ipRestrict						BIT = 0,
	@salesContact					NVARCHAR(255),
	@contractUsername				NVARCHAR(255) = NULL
AS
INSERT INTO dbo.ContractObject (Contract_ID, name, startDate, endDate, status, Company_ID, IsSSOContract, ipRestrict, salesContact)
VALUES ('{' + UPPER(CONVERT(CHAR(36), @contractIdentifier)) + '}', @name, @startDate, @endDate, @status, '{' + UPPER(CONVERT(CHAR(36), @companyIdentifier)) + '}', @isSSOContract, @ipRestrict, @salesContact)
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContractServiceInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.ContractServiceInsert
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsInsertContractServices]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsInsertContractServices
GO

CREATE PROCEDURE dbo.pwsInsertContractServices
	@contractIdentifier				UNIQUEIDENTIFIER,
	@serviceIDs						VARCHAR(1000),
	@startDate						DATETIME,
	@endDate						DATETIME,
	@status							BIT
AS
SELECT *, 0 As 'IsProcessed'
INTO #tmpServiceList
FROM fn_ConvertDelimitedStringToTable(@serviceIDs, ',')

DECLARE @isProcessed BIT
DECLARE @value INT
DECLARE @contractId CHAR(38)

SET @contractId = '{' + UPPER(CONVERT(CHAR(36), @contractIdentifier)) + '}'

WHILE EXISTS (
	SELECT *
	FROM #tmpServiceList
	WHERE IsProcessed = 0
)
BEGIN
	SELECT @value = CONVERT(INT, value)
	FROM #tmpServiceList
	WHERE IsProcessed = 0

	INSERT INTO dbo.ContractServices (contractID, serviceID, startDate, endDate, status)
	VALUES (@contractId, @value, @startDate, @endDate, @status)

	UPDATE #tmpServiceList
	SET IsProcessed = 1
	WHERE value = @value
END

DROP TABLE #tmpServiceList
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateContractServices]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.CreateContractServices
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsInsertContractAndServices]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsInsertContractAndServices
GO

CREATE PROCEDURE dbo.pwsInsertContractAndServices
	@contractIdentifier					UNIQUEIDENTIFIER,
	@name								NVARCHAR(255),
	@status								NVARCHAR(255) = 'Active',
	@startDate							DATETIME,
	@endDate							DATETIME,
	@companyIdentifier					UNIQUEIDENTIFIER,
	@isSSOContract						BIT = 0,
	@ipRestrict							BIT = 0,
	@salesContact						NVARCHAR(255),
	@contractUsername					NVARCHAR(255) = NULL,
	@serviceIDs							VARCHAR(1000) = NULL,
	@contractServiceStatus				BIT = 1
AS
EXEC dbo.pwsInsertContract @contractIdentifier, @name, @status, DEFAULT, DEFAULT, @companyIdentifier, @isSSOContract, @ipRestrict, @salesContact, DEFAULT

EXEC dbo.pwsInsertContractServices @contractIdentifier, @serviceIDs, @startDate, @endDate, @contractServiceStatus
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContractGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.ContractGet
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsGetContract]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsGetContract
GO

CREATE PROCEDURE dbo.pwsGetContract
	@contractIdentifier					UNIQUEIDENTIFIER
AS
SELECT 
		con.name AS 'ContractName',
		CONVERT(UNIQUEIDENTIFIER, con.Contract_ID) AS 'ContractID',
		con.status AS 'ContractStatus',
		con.startDate AS 'ContractStartDate',
		con.endDate AS 'ContractEndDate',
		CONVERT(UNIQUEIDENTIFIER, con.Company_ID) AS 'CompanyID',
		con.IsSSOContract AS 'IsSSOContract',
		con.ipRestrict AS 'IPRestrict',
		con.salesContact AS 'SalesContact',
		con.contractUsername AS 'ContractUsername'
FROM 
		dbo.ContractObject con
WHERE 
		con.Contract_ID = @contractIdentifier 
/*OR 
		con.contractUsername = @contractIdentifier
*/
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContractGetByIPAddress]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.ContractGetByIPAddress
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsGetContractByIPAddress]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsGetContractByIPAddress
GO
/*
CREATE PROCEDURE dbo.pwsGetContractByIPAddress
	@ipAddress						BIGINT
AS
DECLARE @contractIdentifier UNIQUEIDENTIFIER

SET @contractIdentifier = (
	SELECT CONVERT(UNIQUEIDENTIFIER, Contract_ID)
	FROM IPAddressObject
	WHERE
		((CAST(Start_Octet1 AS BIGINT) * 1000000000) + (CAST(Start_Octet2 AS BIGINT) * 1000000) + (CAST(Start_Octet3 AS BIGINT) * 1000) + (CAST(Start_Octet4 AS BIGINT))) <= @ipAddress
		AND ((CAST(End_Octet1 AS BIGINT) * 1000000000) + (CAST(End_Octet2 AS BIGINT) * 1000000) + (CAST(End_Octet3 AS BIGINT) * 1000) + (CAST(End_Octet4 AS BIGINT))) >= @ipAddress
)
	
EXEC dbo.pwsGetContract @contractIdentifier
GO
*/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsGetContractsByIPAddress]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsGetContractsByIPAddress
GO

CREATE PROCEDURE dbo.pwsGetContractsByIPAddress
	@ipAddress						BIGINT
AS
DECLARE @contracts TABLE (
	contractId			UNIQUEIDENTIFIER,
	processed			BIT
)

INSERT INTO @contracts (contractId, processed)
SELECT CONVERT(UNIQUEIDENTIFIER, Contract_ID), 0
FROM IPAddressObject
WHERE
	((CAST(Start_Octet1 AS BIGINT) * 1000000000) + (CAST(Start_Octet2 AS BIGINT) * 1000000) + (CAST(Start_Octet3 AS BIGINT) * 1000) + (CAST(Start_Octet4 AS BIGINT))) <= @ipAddress
	AND ((CAST(End_Octet1 AS BIGINT) * 1000000000) + (CAST(End_Octet2 AS BIGINT) * 1000000) + (CAST(End_Octet3 AS BIGINT) * 1000) + (CAST(End_Octet4 AS BIGINT))) >= @ipAddress

CREATE TABLE #tmpContracts (
	ContractName			NVARCHAR(255),
	ContractID				UNIQUEIDENTIFIER,
	ContractStatus			NVARCHAR(255),
	ContractStartDate		DATETIME,
	ContractEndDate			DATETIME,
	CompanyID				UNIQUEIDENTIFIER,
	IsSSOContract			BIT,
	IPRestrict				BIT,
	SalesContact			NVARCHAR(255),
	ContractUsername		NVARCHAR(255)
)

WHILE EXISTS (
	SELECT *
	FROM @contracts
	WHERE processed = 0
)
BEGIN
	DECLARE @contractIdentifier UNIQUEIDENTIFIER
	
	SELECT TOP 1 @contractIdentifier = contractId
	FROM @contracts
	WHERE processed = 0
	
	INSERT INTO #tmpContracts
	EXEC dbo.pwsGetContract @contractIdentifier
	
	UPDATE @contracts
	SET processed = 1
	WHERE contractId = @contractIdentifier
END

SELECT *
FROM #tmpContracts

DROP TABLE #tmpContracts
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContractGetByNameAndCompany]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.ContractGetByNameAndCompany
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsGetContractByCompanyIdentifierAndName]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsGetContractByCompanyIdentifierAndName
GO

CREATE PROCEDURE dbo.pwsGetContractByCompanyIdentifierAndName
	@companyIdentifier				UNIQUEIDENTIFIER,
	@contractName					NVARCHAR(255)
AS
DECLARE @contractIdentifier UNIQUEIDENTIFIER

SET @contractIdentifier = (
	SELECT TOP 1 CONVERT(UNIQUEIDENTIFIER, Contract_ID)
	FROM dbo.ContractObject
	WHERE Company_ID = @companyIdentifier
		AND name = @contractName
)

EXEC dbo.pwsGetContract @contractIdentifier
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CompanyGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.CompanyGet
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsGetCompany]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsGetCompany
GO

CREATE PROCEDURE dbo.pwsGetCompany
	@companyIdentifier					UNIQUEIDENTIFIER
AS
SELECT 	 
		org.u_Name AS 'CompanyName',
		CONVERT(UNIQUEIDENTIFIER, org.g_org_id) AS 'CompanyID',
		org.status AS 'CompanyStatus',
		org.type AS 'CompanyType'
FROM 
		dbo.OrganizationObject org
WHERE 
		org.g_org_id = @companyIdentifier
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IPAddressRangeGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.IPAddressRangeGet
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsGetIPAddressRangesByContract]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsGetIPAddressRangesByContract
GO

CREATE PROCEDURE dbo.pwsGetIPAddressRangesByContract
	@contractIdentifier					UNIQUEIDENTIFIER
AS
SELECT IPAddress_ID, Contract_ID, CAST(Start_Octet1 AS BIGINT) * 1000000000 + 
				CAST(Start_Octet2 AS BIGINT) * 1000000 + 
				CAST(Start_Octet3 AS BIGINT) * 1000 + 
				CAST(Start_Octet4 AS BIGINT) AS 'Lower_IP', 
	CAST(End_Octet1 AS BIGINT) * 1000000000 + 
				CAST(End_Octet2 AS BIGINT)* 1000000 + 
				CAST(End_Octet3 AS BIGINT) * 1000 + 
				CAST(End_Octet4 AS BIGINT) AS 'Upper_IP'
FROM dbo.IPAddressObject
WHERE Contract_ID = @contractIdentifier
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContractServiceGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.ContractServiceGet
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsGetContractServices]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsGetContractServices
GO

CREATE PROCEDURE dbo.pwsGetContractServices
	@contractIdentifier					UNIQUEIDENTIFIER
AS
SELECT 
	s.serviceID AS 'ServiceID',
	s.serviceName AS 'ServiceName',
	s.active AS 'ServiceStatus',
	cs.startDate AS 'StartDate',
	cs.endDate AS 'EndDate',
	cs.status AS 'ContractServiceStatus',
	rs.isRestricted AS 'IsOwnershipRestricted'
FROM dbo.Services s
LEFT JOIN dbo.ContractServices cs
ON s.serviceID = cs.serviceID
LEFT JOIN dbo.restrictedServices rs
ON cs.serviceID = rs.serviceID
WHERE cs.contractID = @contractIdentifier
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContractServiceSet]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.ContractServiceSet
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsUpdatedContractServices]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsUpdatedContractServices
GO

CREATE PROCEDURE dbo.pwsUpdatedContractServices
	@contractIdentifier				UNIQUEIDENTIFIER,
	@serviceIDs						VARCHAR(1000),
	@startDate						DATETIME,
	@endDate						DATETIME,
	@status							BIT
AS
BEGIN TRANSACTION
	-- remove existing services
	DELETE FROM dbo.ContractServices
	WHERE contractID = @contractIdentifier

	EXEC dbo.pwsInsertContractServices @contractIdentifier, @serviceIDs, @startDate, @endDate, @status
COMMIT TRANSACTION
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAuthenticationCredentialsUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.UserAuthenticationCredentialsUpdate
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsUpdateUserAuthenticationCredentials]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsUpdateUserAuthenticationCredentials
GO

CREATE PROCEDURE dbo.pwsUpdateUserAuthenticationCredentials
	@u_logon_name nvarchar(255),
	@isSuccesfulSignIn Bit
AS
Declare @userAuthenticationID Int

SELECT 
		@userAuthenticationID  = a.id
FROM 
		dbo.UserObject u
		LEFT JOIN 
			dbo.UserAuthentication a
		ON
			u.u_logon_name = a.u_logon_name

		LEFT JOIN 
			dbo.UserAuthenticationLink l
		ON
			a.id = l.id
WHERE 
		u.u_logon_name = @u_logon_name

If @isSuccesfulSignIn = 1 
Begin

	Update UserAuthentication Set
	signInAttempts = 0,
	lastSignInAttempt = GetDate()
	Where id = @userAuthenticationID

End
Else
Begin

	Update UserAuthentication Set
	signInAttempts = signInAttempts + 1,
	lastSignInAttempt = GetDate()
	Where id = @userAuthenticationID

End

--Exec [UserAuthenticationCredentialsUpdate] 'arundevelopment', 0
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SessionUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.SessionUpdate
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsUpdateSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsUpdateSession
GO

CREATE PROCEDURE dbo.pwsUpdateSession
	@sessionId uniqueIdentifier Output,
	@logonname nvarchar(255),
	@timeSpanDays int,
	@ExpiryDate DateTime Output,
	@CreateDate DateTime Output
AS
Declare @rows Int
Set @rows = 0

Select @rows = count(*) From 
kcSession Where sessionId = @sessionId

If(@rows = 0)
Begin

	Set @CreateDate = GetDate()
	Set @ExpiryDate = DateAdd(d, @timeSpanDays, @CreateDate)
	Set @sessionId = NewID()
	
	Insert Into kcSession(sessionId,u_logon_name,CreateDate,ExpiryDate)
	Values(@sessionId,@logonname,@CreateDate,@ExpiryDate)

End
Else
Begin

	Select @ExpiryDate = ExpiryDate,
	@CreateDate = CreateDate From
	kcSession Where sessionId = @sessionId	
	
	Declare @DaysTillExpiry int
	Set @DaysTillExpiry = DateDiff(d, GetDate(), @ExpiryDate)

	-- Only update the expiry if it hasn't already expired
	If (@DaysTillExpiry > 0)
	Begin

		--Reset Expiry Date Parameter
		Set @ExpiryDate = DateAdd(d, @timeSpanDays - @DaysTillExpiry, @ExpiryDate)

		Update kcSession
		Set ExpiryDate = @ExpiryDate
		Where sessionId = @sessionId

	End

End
GO

/*
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[kcUserAuthenticationInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.kcUserAuthenticationInsert
*/
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsInsertUserAuthentication]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsInsertUserAuthentication
GO

CREATE PROCEDURE dbo.pwsInsertUserAuthentication
	@u_logon_name nvarchar(255),
	@hash varchar(1000),
	@salt varchar(8),
	@timeStamp DateTime
AS
Declare @latestID int

Begin Tran 

Insert Into dbo.UserAuthentication(u_logon_name,hash,salt,[timeStamp])
Values(@u_logon_name,@hash,@salt,@timeStamp)

Set @latestID = @@Identity

Delete From dbo.UserAuthenticationLink Where u_logon_name = @u_logon_name

Insert Into dbo.UserAuthenticationLink(id,u_logon_name)
Values(@latestID, @u_logon_name)

If(@@Error <> 0)
Begin
	Rollback Tran
End
Else
Begin
	Commit Tran 
End
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.UserUpdate
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsUpdateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsUpdateUser
GO

CREATE PROCEDURE dbo.pwsUpdateUser
	@Username					nvarchar(255),
	@UserId						uniqueidentifier,
	@FirstName					nvarchar(255),
	@LastName					nvarchar(255),
	@Email						nvarchar(255),
	@JobFunction				nvarchar(255),
	@Address					nvarchar(255),
	@City						nvarchar(255),
	@PostCode					nvarchar(15),
	@Telephone					nvarchar(255),	
	@ReceiveRegularUpdates		bit,
	@SalesContact				nvarchar(255),
	@GateKeeperUser				bit,
	@SharedPassword				bit,
	@ContractID					uniqueidentifier,
	@IPRestrict					nvarchar(255),
	@IsAccountActive bit,
	@ChangePasswordNextLogin bit,
	@PurchasedAmountToDate int, 
	@PurchaseLimit int, 
	@IsBillingCodeRequired bit,
	@PreferredCurrency nvarchar(255), 
	@IsNewUser bit, 
	@IsAffiliateUser bit,
	@IsPressUser bit, 
	@IsMarketingContact bit, 
	@IsNotifyOfNewProducts bit,
	@UserType int, 
	@DateCreated DateTime,
	@IsAutoLogin bit,
	@PreferredEmailFormat nvarchar(255),
	@Interests nvarchar(255),
	@SectorForURLRedirection nvarchar(255) ,
	@JobTitle nvarchar(255),
	@State nvarchar(255),
	@Fax nvarchar(255),
	@Title nvarchar(255),
	@Country nvarchar(255)

AS
BEGIN
	SET NOCOUNT ON;
	SET DATEFORMAT dmy

    If (SELECT COUNT(*) FROM dbo.UserObject WHERE u_logon_name = @Username) > 0
		BEGIN
			UPDATE 
				dbo.UserObject
			SET
				u_first_name = @FirstName,
				u_last_name = @lastName,
				u_email_address = @Email,
				jobFunction = @JobFunction,
				address = @Address,
				city = @City,
				postCode = @PostCode,
				u_tel_number = @Telephone,
				receiveRegularUpdates = @ReceiveRegularUpdates,
				salesContact = @SalesContact,
				gateKeeperUser = @GateKeeperUser,
				sharedPassword = @SharedPassword,
				ipRestrict = @IPRestrict,
				i_account_status = @IsAccountActive,
				changePasswordNextLogin = @ChangePasswordNextLogin,
				purchasedAmountToDate = @PurchasedAmountToDate,
				purchaseLimit = @PurchaseLimit,
				billingCodeRequired = @IsBillingCodeRequired,
				preferrredCurrency = @PreferredCurrency,
				newUser = @IsNewUser,
				affiliateUser = @IsAffiliateUser,
				pressUser = @IsPressUser,
				marketingContact = @IsMarketingContact,
				notifyOfNewProducts = @IsNotifyOfNewProducts,
				i_user_type = @UserType,
				d_date_created = @DateCreated,
				autoLogin = @IsAutoLogin,
				preferredEmailFormat = @PreferredEmailFormat,
				interest = @Interests,
				sectorForURLRedirection = @SectorForURLRedirection,
				jobTitle = @JobTitle,
				state = @State,
				u_fax_number = @Fax,
				u_user_title = @Title,
				country = @Country
			WHERE
				u_logon_name = @Username
		END
	ELSE
		BEGIN			
			INSERT INTO
				dbo.UserObject
			(	
				u_logon_name,
				g_user_id,
				u_first_name,
				u_last_name,
				u_email_address,
				jobFunction,
				address,
				city,
				postCode,
				u_tel_number,
				receiveRegularUpdates,
				salesContact,
				gateKeeperUser,
				sharedPassword,
				Contract_ID,
				ipRestrict,
				i_account_status,
				changePasswordNextLogin,
				purchasedAmountToDate,
				purchaseLimit,
				billingCodeRequired,
				preferrredCurrency,
				newUser,
				affiliateUser,
				pressUser,
				marketingContact,
				notifyOfNewProducts,
				i_user_type,
				d_date_created,
				autoLogin,
				preferredEmailFormat,
				interest,
				sectorForURLRedirection,
				jobTitle,
				state,
				u_fax_number,
				u_user_title,
				country
			)
			VALUES
			(
				@Username,
				'{' + UPPER(CONVERT(CHAR(36), @UserId)) + '}',
				@FirstName,
				@LastName,
				@Email,
				@JobFunction,
				@Address,
				@City,
				@PostCode,
				@Telephone,
				@ReceiveRegularUpdates,
				@SalesContact,
				@GateKeeperUser,
				@SharedPassword,
				'{' + UPPER(CONVERT(CHAR(36), @ContractID)) + '}',
				@IPRestrict,
				@IsAccountActive,
				@ChangePasswordNextLogin,
				@PurchasedAmountToDate,
				@PurchaseLimit,
				@IsBillingCodeRequired,
				@PreferredCurrency,
				@IsNewUser,
				@IsAffiliateUser,
				@IsPressUser,
				@IsMarketingContact,
				@IsNotifyOfNewProducts ,
				@UserType,
				@DateCreated,
				@IsAutoLogin,
				@PreferredEmailFormat,
				@Interests,
				@SectorForURLRedirection,
				@JobTitle,
				@State,
				@Fax,
				@Title,
				@Country
			)

		END
END
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeactivateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.DeactivateUser
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsDeactivateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsDeactivateUser
GO

CREATE PROCEDURE dbo.pwsDeactivateUser
	-- Add the parameters for the stored procedure here
	@username nvarchar(255),
	@contractId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 UPDATE dbo.UserObject 
	 SET 
		i_account_status = 0 
	 WHERE
		u_logon_name = @username 
	AND 
		CONVERT(UNIQUEIDENTIFIER, Contract_id) = @contractId
END

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SessionDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.SessionDelete
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsDeleteSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsDeleteSession
GO

CREATE PROCEDURE dbo.pwsDeleteSession
	@sessionId uniqueIdentifier
AS
DELETE FROM dbo.kcSession
WHERE sessionId = @sessionId
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SessionMultiDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.SessionMultiDelete
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsDeleteSessionsByUsername]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsDeleteSessionsByUsername
GO

CREATE PROCEDURE dbo.pwsDeleteSessionsByUsername
	@username nvarchar(255)
AS
DELETE FROM dbo.kcSession
WHERE u_logon_name = @username
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSignature]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.GetSignature
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsGetSignature]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsGetSignature
GO

CREATE PROCEDURE dbo.pwsGetSignature
	-- Add the parameters for the stored procedure here
	@signatureId				UNIQUEIDENTIFIER
AS

DECLARE @dateCreated datetime,@timeOutDays int, @row int

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT   @row  = COUNT(*),
			@dateCreated = DateCreated,
			@timeOutDays = TimeOutDays
	FROM
			dbo.Signature
	WHERE
			signatureId = @signatureId
	GROUP BY
			DateCreated,TimeOutDays
	
	IF ( @row  > 0)
		BEGIN
			IF ( getDate() > DateAdd(d,@timeOutDays ,@dateCreated) )
				BEGIN
					UPDATE dbo.Signature
					SET isActive = 0
					WHERE
						signatureId = @signatureId
				END
		END

	SELECT 
			SignatureId,
			UserName,
			DateCreated,
			TimeOutDays,
			IsActive
	FROM
			dbo.Signature
	WHERE
	signatureId = @signatureId
	
END

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateSignature]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.CreateSignature
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsCreateSignature]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsCreateSignature
GO

CREATE PROCEDURE dbo.pwsCreateSignature
	-- Add the parameters for the stored procedure here
	@signatureId				UNIQUEIDENTIFIER OUT,
	@userName					NVARCHAR(255),
	@dateCreated				DATETIME OUT,
	@timeOutDays				INT,
	@isActive					BIT
	
AS
BEGIN
	Set @signatureId = NewID()
	INSERT INTO Signature 
			(
				SignatureId,
				UserName,
				TimeOutDays,
				IsActive
			)
	VALUES
			(
				@signatureId,
				@userName,
				@timeOutDays,
				@isActive	
			)
	SELECT @dateCreated =  DateCreated FROM Signature WHERE SignatureId = @signatureId
END

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateSignature]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.UpdateSignature
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsUpdateSignature]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsUpdateSignature
GO

CREATE PROCEDURE dbo.pwsUpdateSignature
	-- Add the parameters for the stored procedure here
	@signatureId				UNIQUEIDENTIFIER,
	@isActive					BIT
	
AS
BEGIN
	UPDATE Signature 
	SET			
			IsActive		= @isActive
	
	WHERE	signatureId = @signatureId
	
END
GO


IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[UserObject_ContractID_Update]'))
DROP TRIGGER [dbo].[UserObject_ContractID_Update]
GO

CREATE TRIGGER dbo.UserObject_ContractID_Update
ON dbo.UserObject
AFTER INSERT, UPDATE
AS
IF UPDATE(Contract_ID)
BEGIN
	UPDATE dbo.UserObject
	SET g_org_id = o.g_org_id, organizationName = o.u_Name, contractName = c.name
	FROM dbo.OrganizationObject o JOIN (
		dbo.ContractObject c JOIN (
			dbo.UserObject u JOIN inserted i
				ON u.u_logon_name = i.u_logon_name
			) ON c.Contract_ID = u..Contract_ID
		) ON o.g_org_id = c.Company_ID
END
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pwsGetContractByContractUsername]') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.pwsGetContractByContractUsername
GO

CREATE PROCEDURE dbo.pwsGetContractByContractUsername
	@contractUsername					NVARCHAR(255)
AS
DECLARE @contractID UNIQUEIDENTIFIER
DECLARE	@return_value INT

SELECT @contractID = CONVERT(UNIQUEIDENTIFIER, Contract_ID)
FROM dbo.ContractObject
WHERE contractUsername = @contractUsername

EXEC @return_value = dbo.pwsGetContract @contractIdentifier = @contractID

RETURN @return_value
GO
