using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using Datamonitor.Permissions;
using System.Configuration;
using Datamonitor.DataModel;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.Permissions.Providers;
using Datamonitor.Permissions.Entities;

namespace Datamonitor.Web.Permissions
{
    /// <summary>
    /// Summary description for Authentication
    /// </summary>
    [WebService(Namespace = "http://datamonitor.com/Authentication", Description = "Webservice class provides authentication methods to verify and validate users and their permissions")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class Authentication : System.Web.Services.WebService
    {     
        private const string CONNECTIONSTRING_PARAM = "dmvProfiles";
        private const string EKEY_PARAM = "eKey";        
        
        [WebMethod]
        public AuthenticationResult AuthenticateCredentials(string username, string password)
        {        
            IAuthenticationService authService = 
                AuthenticationProvider.GetSignInAuthenticationService(username, password);
           return authService.Authenticate();
        }

        [WebMethod]
        public AuthenticationResult AuthenticateEncryptedLogin(string encryptedLogonPassword)
        {
            IAuthenticationService authService = 
                AuthenticationProvider.GetELPAuthenticationService(encryptedLogonPassword, ConfigurationManager.AppSettings[Authentication.EKEY_PARAM]);
            return authService.Authenticate();
        }

        [WebMethod]
        public AuthenticationResult AuthenticateAthens(int[] services, Guid companyID, string companyName, string athensOrgId)
        {
            IAuthenticationService authService =
                AuthenticationProvider.GetAthensAuthenticationService(services, companyID, companyName, athensOrgId);
            return authService.Authenticate();
        }

        [WebMethod]
        public AuthenticationResult AuthenticateWebSSO(string webSSOToken)
        {
            IAuthenticationService authService =
                AuthenticationProvider.GetWebSSOAuthenticationService(webSSOToken);
            return authService.Authenticate();
        }

        [WebMethod]
        public string AuthenticateToken(string authenticationToken)
        {
            IAuthenticationService authService = 
                AuthenticationProvider.GetTokenAuthenticationService(authenticationToken);
            return string.Empty;
        }

        [WebMethod]
        public AuthenticationResult AuthenticateIPAddress(long ipAddress, int[] targetServices)
        {
            IAuthenticationService authService =
                AuthenticationProvider.GetIPAddressAuthenticationService(ipAddress, targetServices);
            return authService.Authenticate();
        }

        [WebMethod]
        public AuthenticationResult AuthenticateAdHoc(int[] services, string companyName, string userEmail, string firstName, string lastName, string userpassword, DateTime startDate, DateTime endDate)
        {
            IAuthenticationService authService =
                AuthenticationProvider.GetAdHocAuthenticationService(services, companyName, userEmail, firstName, lastName, userpassword, startDate, endDate);
            return authService.Authenticate();
        }
     
    }
}