using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Datamonitor.Data
{
    [Guid("976C12EA-E343-40d5-9A40-D72B4238110A")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface ICrypto
    {
        string Decrypt(string cipherText);
        string Encrypt(string plainText);
    }
}
