using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace Datamonitor.Data.Hashing
{
    public abstract class Hash : IHash
    {
        private byte[] saltBytes;
        private HashAlgorithm hashAlgorithm;

        public Hash(byte[] saltBytes, HashAlgorithm hashAlgorithm) 
        {
            this.saltBytes = saltBytes;
            this.hashAlgorithm = hashAlgorithm;
        }

        public string ComputeHash(string plainText)
        {
            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Allocate array, which will hold plain text and salt.
            byte[] toHash = new byte[plainTextBytes.Length + saltBytes.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
            {
                toHash[i] = plainTextBytes[i];
            }

            // Append salt bytes to the resulting array.
            for (int i = 0; i < saltBytes.Length; i++)
            {
                toHash[plainTextBytes.Length + i] = saltBytes[i];
            }

            // Compute hash value of our plain text with appended salt.
            byte[] fromHash = hashAlgorithm.ComputeHash(toHash);

            // Convert result into a base64-encoded string.
            return Convert.ToBase64String(fromHash);
        }

        public bool VerifyHash(string plainText, string hashValue)
        {
            string newHashValue = this.ComputeHash(plainText);

            return newHashValue == hashValue;
        }
    }
}
