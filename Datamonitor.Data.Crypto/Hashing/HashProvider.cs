using System;
using System.Collections.Generic;
using System.Text;

namespace Datamonitor.Data.Hashing
{
    public static class HashProvider
    {
        public static Hash GetHash(string salt)
        {
            byte[] bytes = Convert.FromBase64String(salt);
            Hash MD5Hash = new MD5Hash(bytes);

            return MD5Hash;
        }
    }
}
