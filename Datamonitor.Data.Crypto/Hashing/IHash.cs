﻿using System;
namespace Datamonitor.Data.Hashing
{
    interface IHash
    {
        string ComputeHash(string plainText);
        bool VerifyHash(string plainText, string hashValue);
    }
}
