using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace Datamonitor.Data.Hashing
{
    public class MD5Hash : Hash
    {
        public MD5Hash(byte[] saltBytes)
            : base(saltBytes, new MD5CryptoServiceProvider()) { }
    }
}
