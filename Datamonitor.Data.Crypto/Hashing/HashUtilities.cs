using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace Datamonitor.Data.Hashing
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>got from ms sourcve code</remarks>
    public static class HashUtilities
    {
        public static string GeneratePassword()
        {
            // TODO.. Get Password Length From Config
            return GenerateRandomString(8);
        }

        public static string GenerateSalt()
        {
            return GenerateRandomString(8);
        }

        public static string GenerateRandomString(int length)
        {
            
            char[] punctuations = "!@#$%^&*()_-+=[{]};:>|./?".ToCharArray();

            string str;
            int num;
            if ((length < 1) || (length > 128))
            {
                throw new ArgumentException("Membership password length incorrect");
            }
            do
            {
                byte[] data = new byte[length];
                char[] chArray = new char[length];
                int num2 = 0;
                Random random = new Random();
                new RNGCryptoServiceProvider().GetBytes(data);
                for (int i = 0; i < length; i++)
                {
                    int num4 = data[i] % 87;
                    
                    if (num4 >= 62)
                    {
                        num4 = random.Next(0, 61);
                    }

                    if (num4 < 10)
                    {
                        chArray[i] = (char)(48 + num4);
                    }
                    else if (num4 < 36)
                    {
                        chArray[i] = (char)((65 + num4) - 10);
                    }
                    else if (num4 < 62)
                    {
                        chArray[i] = (char)((97 + num4) - 36);
                    }
                    else
                    {
                        chArray[i] = punctuations[num4 - 62];
                    }
                }
                str = new string(chArray);
            }
            while (IsDangerousString(str, out num));
            return str;
        }

        private static bool IsDangerousString(string s, out int matchIndex)
        {
            char[] startingChars = new char[] { '<', '&' };

            matchIndex = 0;
            int startIndex = 0;
            while (true)
            {
                int num2 = s.IndexOfAny(startingChars, startIndex);
                if (num2 < 0)
                {
                    return false;
                }
                if (num2 == (s.Length - 1))
                {
                    return false;
                }
                matchIndex = num2;
                char ch = s[num2];
                if (ch != '&')
                {
                    if ((ch == '<') && ((IsAtoZ(s[num2 + 1]) || (s[num2 + 1] == '!')) || ((s[num2 + 1] == '/') || (s[num2 + 1] == '?'))))
                    {
                        return true;
                    }
                }
                else if (s[num2 + 1] == '#')
                {
                    return true;
                }
                startIndex = num2 + 1;
            }
        }

        private static bool IsAtoZ(char c)
        {
            return (((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')));
        }
    }
}
