using System;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;

namespace Datamonitor.Data
{
    [Guid("DF46AB41-042B-4c0e-8530-E7BF3FD5FB70")]
    [ProgId("Datamonitor.Crypto")]
    [ClassInterface(ClassInterfaceType.None)]
    public class Crypto : ICrypto
    {
        private string _passphrase;

        public Crypto()
        {
            System.Resources.ResourceManager _resManager = new System.Resources.ResourceManager("Datamonitor.Data.Properties.Resources", System.Reflection.Assembly.GetExecutingAssembly());
            _passphrase = _resManager.GetString("PassPhrase");
            _resManager = null;
        }

        #region ICrypto Members

        /// <summary>
        /// Decrypts the cipher text provided based on the passphrase within the resource file.
        /// </summary>
        /// <param name="cipherText">The encoded text to decrypt.</param>
        /// <returns>Decrypted string (plain text)</returns>
        public string Decrypt(string cipherText)
        {
            try
            {
                byte[] encodedBytes = Convert.FromBase64String(cipherText);

                byte[] nullByte = { 0, 0, 0, 0, 0, 0, 0, 0 };

                byte[] password = ASCIIEncoding.ASCII.GetBytes
                        (this._passphrase);

                byte[] salt = ASCIIEncoding.ASCII.GetBytes(cipherText);

                PasswordDeriveBytes passwordBytes = new PasswordDeriveBytes(password, salt, "MD5", 1);

                byte[] SessionKey = passwordBytes.CryptDeriveKey("TripleDES", "MD5", 0, nullByte);

                return DecryptTextFromMemory(encodedBytes, SessionKey, nullByte);

            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /// <summary>
        /// Encrypts the plain text string using the TripleDES MD5 alorithem.
        /// </summary>
        /// <param name="plainText">The plain text to be encrypted</param>
        /// <returns>Base64 encoded string of the plain text</returns>
        public string Encrypt(string plainText)
        {
            try
            {
                byte[] nullByte = { 0, 0, 0, 0, 0, 0, 0, 0 };

                byte[] password = ASCIIEncoding.ASCII.GetBytes
                        (this._passphrase);

                byte[] salt = ASCIIEncoding.ASCII.GetBytes(plainText);

                PasswordDeriveBytes passwordBytes = new PasswordDeriveBytes(password, salt, "MD5", 1);

                byte[] SessionKey = passwordBytes.CryptDeriveKey("TripleDES", "MD5", 0, nullByte);

                byte[] Data = EncryptTextToMemory(plainText, SessionKey, nullByte);

                return Convert.ToBase64String(Data);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        #endregion
    

        /// <summary>
        /// Encrypts the text to a memory stream using the 3DES CreateEncryptor methods
        /// </summary>
        /// <param name="Data">The unencoded text to be encrypted</param>
        /// <param name="Key">The password or phrase used to encrypt the data</param>
        /// <param name="IV">Blank array of bytes</param>
        /// <returns>Encrypted string</returns>
        /// <remarks>IV parameter is kept "null" to keep the encryption and decryption constant</remarks>
        private static byte[] EncryptTextToMemory(string Data, byte[] Key, byte[] IV)
        {
            try
            {
                // Create a MemoryStream.
                MemoryStream mStream = new MemoryStream();


                // Create a CryptoStream using the MemoryStream 
                // and the passed key and initialization vector (IV).
                CryptoStream cStream = new CryptoStream(mStream,
                    new TripleDESCryptoServiceProvider().CreateEncryptor(Key, IV),
                    CryptoStreamMode.Write);

                // Convert the passed string to a byte array.
                byte[] toEncrypt = new ASCIIEncoding().GetBytes(Data);

                // Write the byte array to the crypto stream and flush it.
                cStream.Write(toEncrypt, 0, toEncrypt.Length);
                cStream.FlushFinalBlock();

                // Get an array of bytes from the 
                // MemoryStream that holds the 
                // encrypted data.
                byte[] ret = mStream.ToArray();

                // Close the streams.
                cStream.Close();
                mStream.Close();

                // Return the encrypted buffer.
                return ret;
            }
            catch (CryptographicException e)
            {
                Console.WriteLine("A Cryptographic error occurred: {0}", e.Message);
                return null;
            }

        }

        /// <summary>
        /// Decrypts the text to a memory stream using the 3DES CreateDecryptor methods
        /// </summary>
        /// <param name="Data">The unencoded text to be decrypted</param>
        /// <param name="Key">The password or phrase used to decrypt the data</param>
        /// <param name="IV">Blank array of bytes</param>
        /// <returns>Decrypted string</returns>
        /// <remarks>IV parameter is kept "null" to keep the encryption and decryption constant</remarks>
        private static string DecryptTextFromMemory(byte[] Data, byte[] Key, byte[] IV)
        {
            try
            {
                // Create a new MemoryStream using the passed 
                // array of encrypted data.
                MemoryStream msDecrypt = new MemoryStream(Data);

                // Create a CryptoStream using the MemoryStream 
                // and the passed key and initialization vector (IV).
                CryptoStream csDecrypt = new CryptoStream(msDecrypt,
                    new TripleDESCryptoServiceProvider().CreateDecryptor(Key, IV),
                    CryptoStreamMode.Read);

                // Create buffer to hold the decrypted data.
                byte[] fromEncrypt = new byte[Data.Length];

                // Read the decrypted data out of the crypto stream
                // and place it into the temporary buffer.
                csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);

                //Convert the buffer into a string and return it.
                return new ASCIIEncoding().GetString(fromEncrypt);
            }
            catch (CryptographicException e)
            {
                Console.WriteLine("A Cryptographic error occurred: {0}", e.Message);
                return null;
            }
        }

        /// <summary>
        /// Used to hash the user's password and returns both the cipher text and the salt used.
        /// 
        /// </summary>
        /// <param name="password">User's password</param>
        /// <param name="salt">The salt value that was used</param>
        /// <returns></returns>
        private static string HashPassword(string password, out string salt)
        {
            // Define min and max salt sizes.
            int minSaltSize = 32;
            int maxSaltSize = 32;

            // Generate a random number for the size of the salt.
            Random random = new Random();
            int saltSize = random.Next(minSaltSize, maxSaltSize);

            // Allocate a byte array, which will hold the salt.
            byte[] saltBytes = new byte[saltSize];

            // Initialize a random number generator.
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

            // Fill the salt with cryptographically strong byte values.
            rng.GetNonZeroBytes(saltBytes);


            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(password);

            // Allocate array, which will hold plain text and salt.
            byte[] plainTextWithSaltBytes =
                    new byte[plainTextBytes.Length + saltBytes.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            // Append salt bytes to the resulting array.
            for (int i = 0; i < saltBytes.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];

            // Because we support multiple hashing algorithms, we must define
            // hash object as a common (abstract) base class. We will specify the
            // actual hashing algorithm class later during object creation.
            HashAlgorithm hash = new SHA1Managed();


            // Compute hash value of our plain text with appended salt.
            byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            // Create array which will hold hash and original salt bytes.
            byte[] hashWithSaltBytes = new byte[hashBytes.Length +
                                                saltBytes.Length];

            // Copy hash bytes into resulting array.
            for (int i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            // Append salt bytes to the result.
            for (int i = 0; i < saltBytes.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];

            salt = ASCIIEncoding.ASCII.GetString(saltBytes);
            // Convert result into a base64-encoded string.
            string hashValue = ASCIIEncoding.ASCII.GetString(hashWithSaltBytes);

            // Return the result.
            return hashValue;
        }
    }
}
