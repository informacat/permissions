using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.Data;
using Datamonitor.Data.Hashing;

namespace CryptoConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string strInput = "";

            do
            {
                strInput = Console.ReadLine();

                Crypto crypto = new Crypto();
                string cipherText = crypto.Encrypt(strInput);

                Console.WriteLine(cipherText);
            }
            while (strInput.ToUpper().Trim() != "EXIT");
        }
    }
}
