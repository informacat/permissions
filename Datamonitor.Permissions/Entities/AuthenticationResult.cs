using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.Permissions.Utilities;
using Datamonitor.DataModel;

namespace Datamonitor.Permissions.Entities
{
    
    public class AuthenticationResult
    {
        private bool isAuthenticated;

        public bool IsAuthenticated
        {
            get { return isAuthenticated; }
            set { isAuthenticated = value; }
        }

        private Guid authenticationToken;

        public Guid AuthenticationToken
        {
            get { return authenticationToken; }
            set { authenticationToken = value; }
        }

        private AuthenticationStatusType authenticationStatusType;

        public AuthenticationStatusType AuthenticationStatusType
        {
            get { return authenticationStatusType; }
            set { authenticationStatusType = value; }
        }

        public AuthenticationResult() { }

        public AuthenticationResult(Guid authenticationToken)
        {
            this.authenticationToken = authenticationToken;
        }
	
    }
}
