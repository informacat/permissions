using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel;
using System.Collections;
using System.Xml.Serialization;
using Datamonitor.Permissions.Rules;

namespace Datamonitor.Permissions.Entities
{
    public class AuthorisationResult
    {
        private bool isAuthorised;

        public bool IsAuthorised
        {
            get { return isAuthorised; }
            set { isAuthorised = value; }
        }

        public string[] AuthorisationResultsArray;
        
        [XmlIgnore]
        public Dictionary<string, bool> authorisationResults;

        public AuthorisationResult() { }

        public AuthorisationResult(Dictionary<string, bool> authorisationResults)
        {
            this.authorisationResults = authorisationResults;

            AuthorisationResultsArray = new string[authorisationResults.Count];
            int i = 0;
            foreach(KeyValuePair<string,bool> kvp in authorisationResults)
            {
                AuthorisationResultsArray[i] = String.Format("{0}:{1}",kvp.Key.ToString(),kvp.Value.ToString());                
                i++;
            }

            this.isAuthorised = !this.authorisationResults.ContainsValue(false);
        }
    }
}
