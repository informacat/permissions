using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.Data;

namespace Datamonitor.Permissions.Entities
{
    public class EncryptedLogin
    {
        /// <summary>
        /// Null Initialization Vector
        /// </summary>
        private const string IV = "\0\0\0\0\0\0\0\0";

        private string username;
        private string password;
        private ICrypto crypto;

        public EncryptedLogin(ICrypto crypto)
            : base()
        {
            this.crypto = crypto;
        }

        public void Decrypt (string encryption, string key)
        {
            char[] split = new char[1] { ':' };

            string decryption = crypto.Decrypt(encryption.Replace(" ","+"));

            string[] combo = decryption.Split(split);
            username = combo[0];
            password = combo[1].TrimEnd(new char[1] { '\0' });
        }

        public string Username { get { return this.username; } }

        public string Password { get { return this.password; } }

    }
}
