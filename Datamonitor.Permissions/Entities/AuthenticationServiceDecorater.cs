using System;
using System.Collections.Generic;
using System.Text;

namespace Datamonitor.Permissions.Entities
{
    public abstract class AuthenticationServiceDecorater : AuthenticationService
    {
        protected AuthenticationService authenticationService;

        public AuthenticationServiceDecorater(AuthenticationService authenticationService)
        {
            this.authenticationService = authenticationService;
        }
    } 
}
