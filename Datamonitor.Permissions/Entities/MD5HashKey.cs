using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using Datamonitor.Permissions.Interfaces;

namespace Datamonitor.Permissions.Entities
{
    /// <summary>
    /// Provides a method to decrypt text that has been encrypted using the
    /// MD5 hash and TripleDES algorithm with a null initialisation vector
    /// </summary>
    /// <remarks>Encrypt is not implemented</remarks>
    public class MD5HashKey : ISymetricKey
    {
        /// <summary>
        /// 1
        /// </summary>
        private const int PASSWORD_ITERATIONS = 1;
        /// <summary>
        /// TripleDES
        /// </summary>
        private const string ALG_NAME = "TripleDES";
        /// <summary>
        /// MD5
        /// </summary>
        private const string HASH_NAME = "MD5";
        /// <summary>
        /// 0
        /// </summary>
        private const int KEY_SIZE = 0;
        
        #region IPublicKey Members

        /// <summary>
        /// Encrypts a string with the given private key
        /// </summary>
        /// <param name="unencodedText">unencrypted string</param>
        /// <param name="privateKey">password to encrypt with</param>
        /// <param name="initVector">null initialisation vector</param>
        /// <returns>encrytped text</returns>
        /// <remarks>
        /// Although there is an implementation of this Encrypt method here, the method still throws an
        /// exception as a consumer of this class should not be using this class to encrypt text. There
        /// are more secure hashing algorithms that should be used.
        /// </remarks>
        public string Encrypt(string unencodedText, string privateKey, string initVector)
        {  
            //TODO: remove all code and methods referred to by this method as it should never be used. See remark.
            throw new Exception("The method or operation is not implemented.");
            
            try
            {
                byte[] password = ASCIIEncoding.ASCII.GetBytes(privateKey);

                byte[] salt = ASCIIEncoding.ASCII.GetBytes(unencodedText);

                byte[] IV = ASCIIEncoding.ASCII.GetBytes(initVector);

                PasswordDeriveBytes passwordBytes = new PasswordDeriveBytes(password, salt, HASH_NAME, PASSWORD_ITERATIONS);

                byte[] SessionKey = passwordBytes.CryptDeriveKey(ALG_NAME, HASH_NAME, KEY_SIZE, IV);

                byte[] Data = EncryptTextToMemory(unencodedText, SessionKey, IV, privateKey);

                return Convert.ToBase64String(Data);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Decrypts a string using the given password
        /// </summary>
        /// <param name="encodedText">Encrypted text</param>
        /// <param name="privateKey">Password text</param>
        /// <param name="initVector">Null initialisation vector</param>
        /// <returns>Decrypted string</returns>
        public string Decrypt(string encodedText, string privateKey, string initVector)
        {
            //try
            //{
            //    byte[] encodedBytes = Convert.FromBase64String(encodedText);

            //    byte[] password = ASCIIEncoding.ASCII.GetBytes(privateKey);

            //    byte[] salt = ASCIIEncoding.ASCII.GetBytes(encodedText);

            //    byte[] IV = ASCIIEncoding.ASCII.GetBytes(initVector);

            //    PasswordDeriveBytes passwordBytes = new PasswordDeriveBytes(password, salt, HASH_NAME, PASSWORD_ITERATIONS);

            //    byte[] SessionKey = passwordBytes.CryptDeriveKey(ALG_NAME, HASH_NAME, KEY_SIZE, IV);

            //    return DecryptTextFromMemory(encodedBytes, SessionKey, IV, privateKey);

            //}
            //catch
            //{
            //    return null;
            //}
            return encodedText;
        }

        #endregion

        #region private methods

        /// <summary>
        /// Encrypts the text to a memory stream using the 3DES CreateEncryptor methods
        /// </summary>
        /// <param name="Data">The unencoded text to be encrypted</param>
        /// <param name="Key">The password or phrase used to encrypt the data</param>
        /// <param name="IV">Blank array of bytes</param>
        /// <returns>Encrypted string</returns>
        /// <remarks>IV parameter is kept "null" to keep the encryption and decryption constant</remarks>
        private byte[] EncryptTextToMemory(string Data, byte[] Key, byte[] IV, string privateKey)
        {
            try
            {
                // Create a MemoryStream.
                MemoryStream mStream = new MemoryStream();


                // Create a CryptoStream using the MemoryStream 
                // and the passed key and initialization vector (IV).
                CryptoStream cStream = new CryptoStream(mStream,
                    new TripleDESCryptoServiceProvider().CreateEncryptor(Key, IV),
                    CryptoStreamMode.Write);

                // Convert the passed string to a byte array.
                byte[] toEncrypt = new ASCIIEncoding().GetBytes(privateKey);

                // Write the byte array to the crypto stream and flush it.
                cStream.Write(toEncrypt, 0, toEncrypt.Length);
                cStream.FlushFinalBlock();

                // Get an array of bytes from the 
                // MemoryStream that holds the 
                // encrypted data.
                byte[] ret = mStream.ToArray();

                // Close the streams.
                cStream.Close();
                mStream.Close();

                // Return the encrypted buffer.
                return ret;
            }
            catch (CryptographicException e)
            {
                Console.WriteLine("A Cryptographic error occurred: {0}", e.Message);
                return null;
            }

        }

        /// <summary>
        /// Decrypts the text to a memory stream using the 3DES CreateDecryptor methods
        /// </summary>
        /// <param name="Data">The unencoded text to be decrypted</param>
        /// <param name="Key">The password or phrase used to decrypt the data</param>
        /// <param name="IV">Blank array of bytes</param>
        /// <returns>Decrypted string</returns>
        /// <remarks>IV parameter is kept "null" to keep the encryption and decryption constant</remarks>
        private string DecryptTextFromMemory(byte[] Data, byte[] Key, byte[] IV, string privateKey)
        {
            try
            {
                // Create a new MemoryStream using the passed 
                // array of encrypted data.
                MemoryStream msDecrypt = new MemoryStream(Data);

                // Create a CryptoStream using the MemoryStream 
                // and the passed key and initialization vector (IV).
                CryptoStream csDecrypt = new CryptoStream(msDecrypt,
                    new TripleDESCryptoServiceProvider().CreateDecryptor(Key, IV),
                    CryptoStreamMode.Read);

                // Create buffer to hold the decrypted data.
                byte[] fromEncrypt = new byte[Data.Length];

                // Read the decrypted data out of the crypto stream
                // and place it into the temporary buffer.
                csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);

                //Convert the buffer into a string and return it.
                return new ASCIIEncoding().GetString(fromEncrypt).TrimEnd(new char[1] { '\0' });
            }
            catch (CryptographicException e)
            {
                Console.WriteLine("A Cryptographic error occurred: {0}", e.Message);
                return null;
            }
        }

        #endregion
    }
}
