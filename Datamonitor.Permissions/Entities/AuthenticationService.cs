using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.Permissions.Providers;
using Datamonitor.Permissions.Entities;

namespace Datamonitor.Permissions.Entities
{
    public class AuthenticationService : IService
    {
        /// <summary>
        /// Null Initialization Vector
        /// </summary>
        private const string IV = "\0\0\0\0\0\0\0\0";

        private IRepository repository;
        private ISymetricKey symetricKey;

        public AuthenticationService()
            : this(RepositoryProvider.GetRepository(), SymetricKeyProvider.GetSymetricKey())
        {

        }

        public AuthenticationService(IRepository repository, ISymetricKey symetricKey)
        {
            this.repository = repository;
            this.symetricKey = symetricKey;
        }

        #region IService Members

        public string Authenticate()
        {
            return null;
        }

        public bool Authenticate(string username, string password)
        {
            return AuthenticateUserCredentials(username, password);

        }

        public bool Authenticate(string username, string password, Uri referrer)
        {
            User user = null;

            if (AuthenticateUserCredentials(username, password, out user))
            {
                return referrer.AbsoluteUri == user.Contract.Referrer.AbsoluteUri
                    ? true : false;
            }
            else { }

            return false;
        }

        public bool Authenticate(string username, string password, long ipAddress)
        {
            User user = repository.GetUser(username);
            Contract contract = user.Contract;
            bool isIPAddressinRange = false;

            foreach (IPAddressRange range in contract.IPRanges)
            {
                if (ipAddress >= range.LowerBound && ipAddress <= range.UpperBound)
                {
                    isIPAddressinRange = true;
                }
                else { }
            }

            return isIPAddressinRange;            
        }

        public List<Session> GetActiveSessions()
        {
            List<Session> sessions = repository.GetSessions();

            return sessions.FindAll(
                delegate(Session session)
                {
                    return IsValid(session.Identifier);
                }
            );
        }

        public List<Session> GetActiveSessionsForUser(string username)
        {
            List<Session> sessions = repository.GetSessions();

            return sessions.FindAll(
                delegate(Session session)
                {
                    return session.User.Username == username;
                }
            );
        }


        public Datamonitor.Permissions.Entities.User GetUser(string username)
        {
            return repository.GetUser(username);
        }

        public bool IsValid(string sessionToken)
        {
            Session session = repository.GetSession(sessionToken);

            if (DateTime.Now >= session.CreateDate && DateTime.Now <= session.ExpiryDate)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool TerminateSession(string sessionToken)
        {
            Session session = repository.GetSession(sessionToken);
            if (session != null)
            {
                repository.TerminateSession(session);
                return true;
            }
            else
            {
                return false;
            }
        }

        public User NewUserCredentials(string username, string password, string contractId)
        {
            string encrypedPassword = symetricKey.Encrypt(password, "", IV);

            User user = new User(username, encrypedPassword, this.repository.GetContract(contractId));

            repository.SaveUser(user);

            return user;
        }

        public Session NewSession(string username,  DateTime createDate, int timeOutMinutes)
        {
            Session session = new Session(GetUser(username), createDate, timeOutMinutes);
            return this.repository.SaveSession(session);               
        }

        #endregion

        #region private methods

        protected bool AuthenticateUserCredentials(string username, string password)
        {
            User user = repository.GetUser(username);

            if (user != null)
            {
                string d = symetricKey.Decrypt(user.PasswordHash, password, IV);
                return String.Compare(d, password) == 0;
            }
            else
            {
                return false;
            }
        }

        protected bool AuthenticateUserCredentials(string username, string password, out User user)
        {
            user = repository.GetUser(username);
            return AuthenticateUserCredentials(username, password);
        }
        #endregion
    }

}