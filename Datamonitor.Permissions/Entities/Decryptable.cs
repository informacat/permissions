using System;
using System.Collections.Generic;
using System.Text;

namespace Datamonitor.Permissions.Entities
{ 
    public class Decryptable : AuthenticationServiceDecorater
    {

        public Decryptable(AuthenticationService authenticationService)
            : base(authenticationService)
        {

        }

        public new bool Authenticate(string encryptedLoginText, string privateKey)
        {
            EncryptedLogin encryptedLogin = new EncryptedLogin(encryptedLoginText, privateKey);
            return authenticationService.Authenticate(encryptedLogin.Username, encryptedLogin.Password);
        }

        public new bool Authenticate(string encryptedLoginText, string privateKey, long ipAddress)
        {
            EncryptedLogin encryptedLogin = new EncryptedLogin(encryptedLoginText, privateKey);
            return authenticationService.Authenticate(encryptedLogin.Username, encryptedLogin.Password, ipAddress);
        }

        public new bool Authenticate(string encryptedLoginText, string privateKey, Uri referrer)
        {
            EncryptedLogin encryptedLogin = new EncryptedLogin(encryptedLoginText, privateKey);
            return authenticationService.Authenticate(encryptedLogin.Username, encryptedLogin.Password, referrer);
        }
    }
}