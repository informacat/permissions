using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.Permissions.Providers;

namespace Datamonitor.Permissions.Entities
{
    public class UserCredentials
    {
        private ISymetricKey symetricKey;

        private string username;

        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        private string passowrd;

        public string Password
        {
            get { return passowrd; }
            set { passowrd = symetricKey.Encrypt(value, value, ""); }
        }

        public UserCredentials() : this(SymetricKeyProvider.GetSymetricKey()) { }

        public UserCredentials(ISymetricKey symetricKey)
        {
            this.symetricKey = symetricKey;
        }
	
	
    }
}
