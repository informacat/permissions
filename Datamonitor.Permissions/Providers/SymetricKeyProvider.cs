using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.DataModel;
using Datamonitor.Permissions.Utilities;

namespace Datamonitor.Permissions.Providers
{
    public static class SymetricKeyProvider
    {
        public static ISymetricKey GetSymetricKey()
        {
            Type skType = Type.GetType(AuthenticationConfiguration.SymetricKeyProvider);
            Object skObject = Activator.CreateInstance(skType);
            return skObject as ISymetricKey;
        }
    }
}
