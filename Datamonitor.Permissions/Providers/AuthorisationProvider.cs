using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.Permissions.Services;
using Datamonitor.Permissions.Interfaces;

namespace Datamonitor.Permissions.Providers
{
    public static class AuthorisationProvider
    {
        public static ServiceAuthorisationService GetServiceAuthorisationService(string serviceIdentifier, long ipAddress)
        {
            return new ServiceAuthorisationService(serviceIdentifier, ipAddress);
        }

        internal static RuleBasedAuthorisationService GetRuleBasedAuthorisation(IList<IAuthorisationRule> rules)
        {
            return new RuleBasedAuthorisationService(rules);
        }
    }
}
