using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.DataModel;
using Datamonitor.Permissions.Utilities;
using Datamonitor.Data;
using Datamonitor.Permissions.Services;

namespace Datamonitor.Permissions.Providers
{
    public static class AuthenticationProvider
    {
        public static IAuthenticationService GetSignInAuthenticationService(string username, string password)
        {
            return new SignInAuthenticationService(username, password);          
        }

        public static IAuthenticationService GetELPAuthenticationService(string elp, string privateKey)
        {
            return new ELPAuthenticationService(elp, privateKey, new Crypto());
        }

        public static IAuthenticationService GetTokenAuthenticationService(string authenticationToken)
        {
            return new TokenAuthenticationService(authenticationToken);
        }

        public static IAuthenticationService GetAthensAuthenticationService(int[] services, Guid companyID, string companyName, string athensOrgId)
        {
            return new AthensAuthenticationService(services, companyID, companyName, athensOrgId);
        }

        public static IAuthenticationService GetWebSSOAuthenticationService(string serviceValues)
        {
            return new WebSSOAuthenticationService(serviceValues);
        }

        public static IAuthenticationService GetIPAddressAuthenticationService(long ipAddress, int[] targetServices)
        {
            return new IPAddressAuthenticationService(ipAddress, targetServices);
        }

        public static IAuthenticationService GetAdHocAuthenticationService(int[] services, string companyName, string userEmail, string firstName, string lastName, string userpassword, DateTime startDate, DateTime endDate)
        {
            return new AdHocAuthenticationService(services, companyName, userEmail, firstName, lastName, userpassword, startDate, endDate);
        }
    }
}
