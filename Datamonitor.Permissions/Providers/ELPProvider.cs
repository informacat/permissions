using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel;
using Datamonitor.Permissions.Entities;
using Datamonitor.Data;

namespace Datamonitor.Permissions.Providers
{
    public static class ELPProvider
    {
        public static EncryptedLogin GetActiveDirectoryEncryptedLogin()
        {
            return new EncryptedLogin(new Crypto());
        }

        public static EncryptedLogin GetEncryptedLogin()
        {
            return new EncryptedLogin(new Crypto());
        }
    }
}
