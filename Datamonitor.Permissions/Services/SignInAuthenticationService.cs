using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.Permissions.Providers;
using Datamonitor.DataModel.Providers;
using Datamonitor.DataModel;
using Datamonitor.Permissions.Utilities;
using Datamonitor.Permissions.Entities;
using Datamonitor.DataModel.Interfaces;

namespace Datamonitor.Permissions.Services
{
    public class SignInAuthenticationService : IAuthenticationService
    {
        /// <summary>
        /// Null Initialization Vector
        /// </summary>
        private const string IV = "\0\0\0\0\0\0\0\0";

        private string username;
        private string password;

        public SignInAuthenticationService(string username, string password)
        {
            this.username = username;
            this.password = password;
        }

        public AuthenticationResult Authenticate()
        {
            IDataService dataService = ServiceProvider.GetDataService();

            AuthenticationResult result = new AuthenticationResult();

            User user = null;

            try
            {
                // try and get the user here (it was trying to get the user in the IsUserSignInLocked method - which would result in an extra call
                user = dataService.GetUser(username);

                if (user == null)
                {
                    result.IsAuthenticated = false;
                    result.AuthenticationStatusType = AuthenticationStatusType.BadUsername;
                }
                else if (user.Password == null)
                {
                    result.IsAuthenticated = false;
                    result.AuthenticationStatusType = AuthenticationStatusType.BadPassword;
                }
                else if (user.IsAccountActive == false)
                {
                    result.IsAuthenticated = false;
                    result.AuthenticationStatusType = AuthenticationStatusType.UserInactive;
                }
                else if (dataService.IsUserSignInLocked(
                    user,
                    AuthenticationConfiguration.MaximumSignInAttempts,
                    AuthenticationConfiguration.SignInResetMinutes))
                {
                    result.IsAuthenticated = false;
                    // we have exceeded the max attempts within a given time
                    result.AuthenticationStatusType = AuthenticationStatusType.ExceededMaxAttempts;
                }
                else
                {
                    result.IsAuthenticated = user.Password == password;
                    if (!result.IsAuthenticated) result.AuthenticationStatusType = AuthenticationStatusType.BadUsernamePassword;
                }
            }
            catch (Exception e)
            {
                result.IsAuthenticated = false;
                System.Diagnostics.EventLog.WriteEntry(this.ToString(), e.ToString());
            }

            if (result.IsAuthenticated)
            {
                // Create New Session
                Session session = new Session();
                session.User = user;
                session.TimeOutDays = AuthenticationConfiguration.AuthenticationServiceTimeoutDays;

                dataService.PersistSession(session);

                result.AuthenticationToken = session.Identifier;
                result.AuthenticationStatusType = AuthenticationStatusType.Good;

                dataService.UpdateSignInCredentials(username, true);                
            }
            else
            {
                dataService.UpdateSignInCredentials(username, false);
            }
            
            return result;
        }
    }

}