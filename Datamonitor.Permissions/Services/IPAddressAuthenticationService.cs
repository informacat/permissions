using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.Permissions.Entities;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.Providers;
using Datamonitor.DataModel;
using Datamonitor.Data.Hashing;
using Datamonitor.Permissions.Utilities;

namespace Datamonitor.Permissions.Services
{
    public class IPAddressAuthenticationService : IAuthenticationService
    {
        private long ipAddress;
        private List<string> targetServices;

        public IPAddressAuthenticationService(long ipAddress, int[] targetServices)
        {
            this.ipAddress = ipAddress;
            // Store services as list of strings
            this.targetServices = new List<int>(targetServices)
                .ConvertAll<string>(delegate(int service) { return service.ToString(); });
        }

        public AuthenticationResult Authenticate()
        {   
            AuthenticationResult result = new AuthenticationResult();

            IDataService dataService = ServiceProvider.GetDataService();

            Contract contract = GetContract(dataService);
            
            if (contract != null)
            {
                User singleSignOnUser = UserUtility.GetSingleSignOnUser(contract, dataService);

                Session session = new Session();
                session.User = singleSignOnUser;
                session.TimeOutDays = AuthenticationConfiguration.AuthenticationServiceTimeoutDays;

                dataService.PersistSession(session);

                result.IsAuthenticated = true;
                result.AuthenticationToken = session.Identifier;
                result.AuthenticationStatusType = AuthenticationStatusType.Good;
            }
            else
            {
                result.IsAuthenticated = false;
                result.AuthenticationStatusType = AuthenticationStatusType.Bad;
            }

            return result;
        }

        private Contract GetContract(IDataService dataService)
        {
            // Find matching contracts (might be more than one)
            Contract[] matchingContracts = dataService.GetContracts(ipAddress);

            // Choose one contract from potentials
            Contract contract = ChooseContract(dataService, matchingContracts);

            return contract;
        }

        /// <summary>
        /// Choose a contract to use.
        /// </summary>
        /// <param name="dataService">The data service to use to find contract services</param>
        /// <param name="potentialContracts">The list of potential contracts.</param>
        /// <returns>A suitable contract, or null if not found.</returns>
        private Contract ChooseContract(IDataService dataService, Contract[] potentialContracts)
        {
            // Choose first contract that has access to one or more target services
            Contract contract = null;
            foreach (Contract potentialContract in potentialContracts)
            {
                // Find all services for this contract
                ContractService[] services = dataService.GetActiveContractServices(potentialContract.Identifier);
                // Extract list of service ids
                List<string> serviceIds = new List<ContractService>(services)
                    .ConvertAll<string>(
                        delegate(ContractService service)
                        {
                            return service.Identifier;
                        }
                    );
                // Find intersection of this contract's services and the target services
                List<string> intersection = serviceIds.FindAll(this.targetServices.Contains);

                // Use this contract if it has any of the target services
                if (intersection.Count > 0)
                {
                    contract = potentialContract;
                    break;
                }
            }
            return contract;
        }
    }
}
