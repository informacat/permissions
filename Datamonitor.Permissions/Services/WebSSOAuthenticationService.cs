using System;
using System.Text.RegularExpressions;
using Datamonitor.DataModel;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.Providers;
using Datamonitor.Permissions.Entities;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.Permissions.SSOIntegration;
using Datamonitor.Permissions.Utilities;

namespace Datamonitor.Permissions.Services
{
    public class WebSSOAuthenticationService : IAuthenticationService
    {        
        private Guid contractId;
        
        public WebSSOAuthenticationService(string webSSOToken)
        {
            using (kcintegration_service service = new kcintegration_service())
            {
                string serviceValues = service.CheckvalidV2(webSSOToken) ?? String.Empty;//"NewSite|http://www.ne.com|{3043462F-45EA-40FD-8CE5-419A501F8B0A}"

                Regex regex = new Regex(@"\|(?<contractid>(\{|\}|\-|\w)+)$");
                Match match = regex.Match(serviceValues);

                if (match.Success)
                {
                    contractId = new Guid(match.Groups["contractid"].Value);
                }
            }
        }

        public AuthenticationResult Authenticate()
        {
            AuthenticationResult result = new AuthenticationResult();

            IDataService dataService = ServiceProvider.GetDataService();

            Contract contract = GetContract(dataService);

            if (contract != null)
            {
                User singleSignOnUser = UserUtility.GetSingleSignOnUser(contract, dataService);

                Session session = new Session();
                session.User = singleSignOnUser;
                session.TimeOutDays = AuthenticationConfiguration.AuthenticationServiceTimeoutDays;

                dataService.PersistSession(session);

                result.IsAuthenticated = true;
                result.AuthenticationToken = session.Identifier;
                result.AuthenticationStatusType = AuthenticationStatusType.Good;
            }
            else
            {
                result.IsAuthenticated = false;
                result.AuthenticationStatusType = AuthenticationStatusType.Bad;
            }

            return result;
        }

        private Contract GetContract(IDataService dataService)
        {
            Contract contract;
            try
            {
                contract = dataService.GetContract(contractId);
            }
            catch
            {
                contract = null;
            }

            return contract;
        }
    }
}
