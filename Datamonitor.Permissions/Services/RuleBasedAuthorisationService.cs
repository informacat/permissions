using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.DataModel;
using Datamonitor.Permissions.Entities;
using System.Collections;
using Datamonitor.Permissions.Rules;
using Datamonitor.DataModel.DataAccess;
using Datamonitor.DataModel.Providers;
using Datamonitor.DataModel.Interfaces;

namespace Datamonitor.Permissions.Services
{
    internal class RuleBasedAuthorisationService : IAuthorisationService
    {
        private IList<IAuthorisationRule> authorisationRules;

        public RuleBasedAuthorisationService(IList<IAuthorisationRule> authorisationRules)
        {
            this.authorisationRules = authorisationRules;
        }

        public AuthorisationResult Authorise(Guid authenticationToken)
        {
            Dictionary<string, bool> results = new Dictionary<string, bool>();  

            try
            {
                IDataService dataService = ServiceProvider.GetDataService();
                Session session = dataService.GetSession(authenticationToken);    

                foreach (IAuthorisationRule rule in authorisationRules)
                {
                    results.Add(rule.Name, rule.IsValid(session));
                }
            }
            catch (Exception)
            {
                results.Add("Invalid Session", false);
            }

            return new AuthorisationResult(results);
        }
    }

}

