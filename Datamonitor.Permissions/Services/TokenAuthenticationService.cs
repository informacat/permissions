using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.DataModel;
using Datamonitor.Permissions.Entities;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.Providers;

namespace Datamonitor.Permissions.Services
{
    public class TokenAuthenticationService : IAuthenticationService
    {
        private string authenticationToken;

        public TokenAuthenticationService(string authenticationToken)
        {
            this.authenticationToken = authenticationToken;
        }

        public Datamonitor.Permissions.Entities.AuthenticationResult Authenticate()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            Session session = dataService.GetSession(new Guid(authenticationToken));

            AuthenticationResult result = new AuthenticationResult();

            if (session != null)
            {
                result.IsAuthenticated = !session.IsExpired;
                result.AuthenticationStatusType = AuthenticationStatusType.Good;
            }
            else
            {
                result.AuthenticationStatusType = AuthenticationStatusType.BadAuthenticationToken;
            }

            return result;
        }

    }
}
