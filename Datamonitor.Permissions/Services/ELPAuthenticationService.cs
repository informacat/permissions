using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.DataModel;
using Datamonitor.Permissions.Providers;
using Datamonitor.Permissions.Entities;
using Datamonitor.Data;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.Providers;

namespace Datamonitor.Permissions.Services
{
    public class ELPAuthenticationService : IAuthenticationService
    {
        /// <summary>
        /// Null Initialization Vector
        /// </summary>
        private const string IV = "\0\0\0\0\0\0\0\0";

        private ICrypto crypto;

        private string encryptedLoginText;
        private string privateKey;

        public ELPAuthenticationService(string encryptedLoginText, string privateKey, ICrypto crypto)
        {
            this.crypto = crypto;

            this.encryptedLoginText = encryptedLoginText;
            this.privateKey = privateKey;
        }

        public AuthenticationResult Authenticate()
        {
            IDataService dataService = ServiceProvider.GetDataService();

            EncryptedLogin encryptedLogin = new EncryptedLogin(crypto);
            encryptedLogin.Decrypt(encryptedLoginText.Replace(" ","+"), privateKey);
            
            AuthenticationResult result = new AuthenticationResult();

            User user = null;
            try
            {
                user = dataService.GetUser(encryptedLogin.Username);

                if (user == null)
                {
                    result.IsAuthenticated = false;
                    result.AuthenticationStatusType = AuthenticationStatusType.BadUsername;
                }
                else if (user.Password == null)
                {
                    result.IsAuthenticated = false;
                    result.AuthenticationStatusType = AuthenticationStatusType.BadPassword;
                }
                else if (dataService.IsUserSignInLocked(
                    user,
                    AuthenticationConfiguration.MaximumSignInAttempts,
                    AuthenticationConfiguration.SignInResetMinutes))
                {
                    result.IsAuthenticated = false;
                    // we have exceeded the max attempts within a given time
                    result.AuthenticationStatusType = AuthenticationStatusType.ExceededMaxAttempts;
                }
                else
                {
                    result.IsAuthenticated = user.Password == encryptedLogin.Password;
                    if (!result.IsAuthenticated) result.AuthenticationStatusType = AuthenticationStatusType.BadUsernamePassword;
                }
            }
            catch (Exception e)
            {
                result.IsAuthenticated = false;
                System.Diagnostics.EventLog.WriteEntry(this.ToString(), e.ToString());
            }

            if (result.IsAuthenticated)
            {
                // Create New Session
                Session session = new Session();
                session.User = user;

                session.TimeOutDays = AuthenticationConfiguration.AuthenticationServiceTimeoutDays;

                dataService.PersistSession(session);

                result.AuthenticationToken = session.Identifier;
                result.AuthenticationStatusType = AuthenticationStatusType.Good;
            }
            else
            {
                result.AuthenticationStatusType = AuthenticationStatusType.BadELPText;
            }

            return result;
        }
    }
}
