using System;
using Datamonitor.DataModel;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.Providers;
using Datamonitor.Permissions.Entities;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.Permissions.Utilities;

namespace Datamonitor.Permissions.Services
{
    public class AthensAuthenticationService : IAuthenticationService
    {
        private string athensOrgId;
        private Guid companyID;
        private int[] services;

        public AthensAuthenticationService(int[] services, Guid companyID, string companyName, string athensOrgId)
        {
            this.athensOrgId = athensOrgId;
            this.companyID = companyID;
            this.services = services;
        }

        public AuthenticationResult Authenticate()
        {
            AuthenticationResult result = new AuthenticationResult();

            IDataService dataService = ServiceProvider.GetDataService();

            Contract contract = GetContract(dataService);

            if (contract != null)
            {
                User singleSignOnUser = UserUtility.GetSingleSignOnUser(contract, dataService);

                Session session = new Session();
                session.User = singleSignOnUser;
                session.TimeOutDays = AuthenticationConfiguration.AuthenticationServiceTimeoutDays;

                dataService.PersistSession(session);

                result.IsAuthenticated = true;
                result.AuthenticationToken = session.Identifier;
                result.AuthenticationStatusType = AuthenticationStatusType.Good;
            }
            else
            {
                result.IsAuthenticated = false;
                result.AuthenticationStatusType = AuthenticationStatusType.Bad;
            }

            return result;
        }

        private Contract GetContract(IDataService dataService)
        {
            Contract contract;
            try
            {
                contract = dataService.GetContract(companyID, athensOrgId);

                // Update this contract's services (to match the ones held in Athens)
                SetContractServices(dataService, contract);
            }
            catch
            {
                // Couldn't find contract so create a new one for this athens org
                contract = CreateSingleSignOnContract(dataService);
            }
            return contract;
        }
      
        private void SetContractServices(IDataService dataService, Contract contract)
        {
            dataService.SetContractServices(contract.Identifier, this.services, true, DateTime.Now, DateTime.Now.AddYears(1));
        }

        private Contract CreateSingleSignOnContract(IDataService dataService)
        {
            return dataService.CreateContract(Guid.NewGuid(), this.athensOrgId, true, DateTime.Now, DateTime.Now.AddYears(1), this.companyID,
                true, false, "Unknown", null, String.Join(",", Array.ConvertAll<int, string>(this.services, delegate(int i) { return i.ToString(); })), true);            
        }
    }
}
