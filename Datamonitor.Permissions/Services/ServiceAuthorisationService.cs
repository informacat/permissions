using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.DataModel;
using Datamonitor.Permissions.Utilities;
using Datamonitor.Permissions.Rules;
using Datamonitor.Permissions.Providers;
using Datamonitor.Permissions.Entities;
using System.Reflection;

namespace Datamonitor.Permissions.Services
{
    /// <summary>
    /// Knowledge centre authorisation service
    /// </summary>
    public class ServiceAuthorisationService : IAuthorisationService
    {
        private string _ServiceIdentifier;
        private long _ipAddress;

        /// <summary>
        /// Creates a new knowledge centre authorisation service
        /// </summary>
        /// <param name="serviceIdentifier">current service id</param>
        /// <param name="ipAddress">user's ip address</param>
        public ServiceAuthorisationService(string serviceIdentifier, long ipAddress)
        {
            this.ServiceIdentifier = serviceIdentifier;
            this.IPAddress = ipAddress;
        }

        /// <summary>
        /// Gets and sets the service id
        /// </summary>
        public string ServiceIdentifier
        {
            get { return _ServiceIdentifier; }
            private set { _ServiceIdentifier = value; }
        }

        /// <summary>
        /// Gets and sets the user's ip address
        /// </summary>
        public long IPAddress
        {
            get { return _ipAddress; }
            private set { _ipAddress = value; }
        }

        /// <summary>
        /// Returns the result of the rules that have been executed
        /// </summary>
        /// <param name="autenticationToken">current session token</param>
        /// <returns>Authorisation result for the rules run</returns>
        public AuthorisationResult Authorise(Guid autenticationToken)
        {
            IList<IAuthorisationRule> rules = GetRules();
            IAuthorisationService ruleBasedService = AuthorisationProvider.GetRuleBasedAuthorisation(rules);
            return ruleBasedService.Authorise(autenticationToken);
        }

        // PRIVATE METHODS

        /// <summary>
        /// Returns a list of rules that will be run when  session is being authorised
        /// </summary>
        /// <returns>All runable rules</returns>
        /// <remarks>
        /// 1. Get the excluded rules
        /// 2. Get all potential rules that can be run
        /// 3. Add all potential rules to list
        /// 4. Subtract all excluded rules from list
        /// 5. Return List := potential rules minus excluded rules (list = p - e)
        /// </remarks>
        private IList<IAuthorisationRule> GetRules()
        {
            //Upper case list included to ensure that freely written values in config are
            //run, though cannot account for spelling. Must go by class name regardless of case.
            List<IAuthorisationRule> rules = new List<IAuthorisationRule>();
            List<string> excludedRules = new List<string>(AuthorisationConfiguration.ExcludedRules);
            List<string> excludedRulesUpperCase = excludedRules.ConvertAll<string>(
                new Converter<string, string>(
                    delegate(string rule)
                    {
                        return rule.ToUpper();
                    }
                ));

            List<string> typeList = new List<string>();
            Type[] types = Assembly.GetExecutingAssembly().GetTypes();

            foreach (Type t in types)
            {
                if (t.IsClass && t.Namespace == "Datamonitor.Permissions.Rules" && t.Name != "BaseRule")
                {
                    typeList.Add(t.Name);
                }
            }

            foreach (string rule in typeList)
            {
                switch (rule)
                {
                    case "CompanyActiveRule":
                        rules.Add(new CompanyActiveRule(rule));
                        break;
                    case "ContractActiveRule":
                        rules.Add(new ContractActiveRule(rule));
                        break;
                    case "ContractDateRangeRule":
                        rules.Add(new ContractDateRangeRule(rule, DateTime.Now));
                        break;
                    case "IpAddressRangeRule":
                        rules.Add(new IpAddressRangeRule(rule, IPAddress));
                        break;
                    case "ReferrerRule":
                        rules.Add(new ReferrerRule(rule, String.Empty));
                        break;
                    case "ServiceActiveRule":
                        rules.Add(new ServiceActiveRule(rule, ServiceIdentifier));
                        break;
                    case "ServiceAuthorisedRule":
                        rules.Add(new ServiceAuthorisedRule(rule, ServiceIdentifier));
                        break;
                    case "ServiceDateRangeRule":
                        rules.Add(new ServiceDateRangeRule(rule, DateTime.Now, ServiceIdentifier));
                        break;
                    case "ServiceOnContractActiveRule":
                        rules.Add(new ServiceOnContractActiveRule(rule, ServiceIdentifier));
                        break;
                    case "SessionActiveRule":
                        rules.Add(new SessionActiveRule(rule));
                        break;
                    case "UserAccountActiveRule":
                        rules.Add(new UserAccountActiveRule(rule));
                        break;
                    case "UserIPRestrictRule":
                        rules.Add(new UserIPRestrictRule(rule, IPAddress));
                        break;
                }

            }

            rules.RemoveAll(delegate(IAuthorisationRule rule)
                {
                    return excludedRulesUpperCase.Contains(rule.Name.ToUpper());
                });

            return rules;
        }
    }
}
