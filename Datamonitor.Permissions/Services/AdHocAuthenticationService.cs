using System;
using Datamonitor.DataModel;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.Providers;
using Datamonitor.Permissions.Entities;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.Permissions.Utilities;

namespace Datamonitor.Permissions.Services
{
    public class AdHocAuthenticationService : IAuthenticationService
    {
        private string userEmail;
        private string companyName;
        private string lastName;
        private string firstName;        
        private string userpassword;
        private DateTime startDate;
        private DateTime endDate;
        private int[] services;

        public AdHocAuthenticationService(int[] services, string companyName, string userEmail,
            string firstName, string lastName, string userpassword, DateTime startDate, DateTime endDate)
        {
            this.userEmail = userEmail;
            this.firstName = firstName;
            this.lastName = lastName;
            this.userpassword = userpassword;
            this.startDate = startDate;
            this.endDate = endDate;            
            this.services = services;
            this.companyName = companyName;
        }

        public AuthenticationResult Authenticate()
        {
            AuthenticationResult result = new AuthenticationResult();

            IDataService dataService = ServiceProvider.GetDataService();

            Contract contract = GetContract(dataService);

            if (contract != null)
            {
                User adHocUser = UserUtility.GetAdHocUser(dataService, contract, userEmail, companyName, firstName, lastName, userpassword);
                
                Session session = new Session();
                session.User = adHocUser;
                session.TimeOutDays = AuthenticationConfiguration.AuthenticationServiceTimeoutDays;

                dataService.PersistSession(session);

                result.IsAuthenticated = true;
                result.AuthenticationToken = session.Identifier;
                result.AuthenticationStatusType = AuthenticationStatusType.Good;
            }
            else
            {
                result.IsAuthenticated = false;
                result.AuthenticationStatusType = AuthenticationStatusType.Bad;
            }

            return result;
        }

        private Contract GetContract(IDataService dataService)
        {
            Company company = CompanyUtility.BuildAdHocCompany(dataService, this.companyName); 
            Contract contract = CreateContract(dataService, company);
            SetContractServices(dataService, contract);

            return contract;
        }

        private void SetContractServices(IDataService dataService, Contract contract)
        {
            dataService.SetContractServices(contract.Identifier, this.services, true, DateTime.Now, DateTime.Now.AddYears(1));
        }

        private Contract CreateContract(IDataService dataService, Company company)
        {
            return dataService.CreateContract(Guid.NewGuid(), company.Name, true, this.startDate, this.endDate, company.Identifier,
                true, false, "Unknown", null, String.Join(",", Array.ConvertAll<int, string>(this.services, delegate(int i) { return i.ToString(); })), true);
        }
    }
}
