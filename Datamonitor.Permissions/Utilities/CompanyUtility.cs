using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel;
using Datamonitor.DataModel.Interfaces;

namespace Datamonitor.Permissions.Utilities
{
    internal static class CompanyUtility
    {


        internal static Company BuildAdHocCompany(IDataService dataService, string companyName)
        {
            //*********************            
            // Note for refactoring
            //*********************
            // Add these constants as arguments of this method as needed and replace within where applied.                                 
            const string TRADINGPARTNERNUMBER = "";
            const string ADMINCONTACT = "";
            const string RECEIVER = "";
            const string CATALOGSET = "";
            const string CHANGEDBY = "";
            const string PURCHASINGMANAGER = "";
            const string TYPE = "";
            const string STATUS = "Active";
            const string ADDRESS = "";
            const string CITY = "";
            const string STATE = "";
            const string COUNTRY = "";
            const string POSTCODE = "";

            bool companyExists = IsCompany(dataService, companyName);
            string companyNameTemp = companyName;
            if (companyExists)
            {
                int index = 1;
                do
                {
                    companyNameTemp = String.Format("{0}_{1}", companyName, index.ToString().PadLeft(3, '0'));
                    companyExists = IsCompany(dataService, companyNameTemp);
                    index++;
                }
                while (companyExists);

            }
            else { }

            return dataService.CreateCompany(
                Guid.NewGuid(),
                companyNameTemp, TRADINGPARTNERNUMBER, ADMINCONTACT, RECEIVER, CATALOGSET, CHANGEDBY,
                PURCHASINGMANAGER, DateTime.Now, DateTime.Now, TYPE, STATUS, ADDRESS, CITY, STATE, COUNTRY, POSTCODE);

        }

        private static bool IsCompany(IDataService dataService, string companyName)
        {
            Company company;

            try
            {
                company = dataService.GetCompany(companyName);
            }
            catch
            {
                company = null;
            }

            return company != null;



        }

    }
}
