using System;
using System.Collections.Generic;
using System.Text;
using System.Resources;
using System.Threading;

namespace Datamonitor.Permissions.Utilities
{
    internal sealed class Res
    {
        private ResourceManager resources;
        private static Res loader;
        private static object synchObject;

        /// <summary>
        /// 
        /// </summary>
        /// <seealso cref="http://thith.blogspot.com/2005/11/c-interlocked.html"/>        
        internal Res()
        {
            resources = new ResourceManager("Datamonitor.Permissions.Resources.Settings", this.GetType().Assembly);
        }
       
        public static ResourceManager Resources
        {
            get { return GetLoader().resources; }
        }

        public static string GetString(string key)
        {
            Res loader = GetLoader();

            if (loader == null)
            {
                return null;
            }

            return loader.resources.GetString(key);
        }

        private static Res GetLoader()
        {
            if (loader == null)
            {
                lock (InternalSyncObject)
                {
                    if (loader == null)
                    {
                        loader = new Res();
                    }
                }
            }

            return loader;
        }

        private static object InternalSyncObject
        {
            get
            {
                if (synchObject == null)
                {                    
                    object currObj = new object();
                    Interlocked.CompareExchange(ref synchObject, currObj, null);
                }

                return synchObject;
            }
        }
    }
}
