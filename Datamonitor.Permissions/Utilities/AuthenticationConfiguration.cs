using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Specialized;
using System.Resources;
using System.Reflection;
using Datamonitor.Permissions.Utilities;

namespace Datamonitor.Permissions
{
    /// <summary>
    /// Provides methods to get configuration values from the consuming application
    /// </summary>
    public static class AuthenticationConfiguration
    {       
        /// <summary>
        /// Gets the sign in credentials provider class
        /// </summary>
        /// <remarks>Value used by reflection methods</remarks>
        public static string SignInAuthenticationProvider
        {
            get
            {
                if (ConfigurationManager.AppSettings["Datamonitor.Permissions.SignInAuthenticationProvider"] != null)
                {
                    return ConfigurationManager.AppSettings["Datamonitor.Permissions.SignInAuthenticationProvider"];
                }
                else
                {
                    throw new ApplicationException(Res.GetString("SERVICEPROVIDER_EXCEPTION_MESSAGE"));
                }
            }
        }

        /// <summary>
        /// Gets the encrypted lofin password provider class
        /// </summary>
        /// <remarks>Value used by reflection methods</remarks>
        public static string ELPAuthenticationProvider
        {
            get
            {
                if (ConfigurationManager.AppSettings["Datamonitor.Permissions.ELPAuthenticationProvider"] != null)
                {
                    return ConfigurationManager.AppSettings["Datamonitor.Permissions.ELPAuthenticationProvider"];
                }
                else
                {
                    throw new ApplicationException(Res.GetString("SERVICEPROVIDER_EXCEPTION_MESSAGE"));
                }
            }
        }

        /// <summary>
        /// Gets the symetric key provider class
        /// </summary>
        /// <remarks>Value used by reflection methods</remarks>
        public static string SymetricKeyProvider
        {
            get
            {
                if (ConfigurationManager.AppSettings["Datamonitor.Permissions.SymetricKeyProvider"] != null)
                {
                    return ConfigurationManager.AppSettings["Datamonitor.Permissions.SymetricKeyProvider"];
                }
                else
                {
                    throw new ApplicationException(Res.GetString("SYMETRICKEYPROVIDER_EXCEPTION_MESSAGE"));
                }
            }
        }

        /// <summary>
        /// Gets the token authentication provider class
        /// </summary>
        /// <remarks>Value used by reflection methods</remarks>
        public static string TokenAuthenticationProvider
        {
            get
            {
                if (ConfigurationManager.AppSettings["Datamonitor.Permissions.TokenAuthenticationProvider"] != null)
                {
                    return ConfigurationManager.AppSettings["Datamonitor.Permissions.TokenAuthenticationProvider"];
                }
                else
                {
                    throw new ApplicationException(Res.GetString("TOKENAUTHENTICATIONPROVIDER_EXCEPTION_MESSAGE"));
                }
            }
        }

        /// <summary>
        /// Gets the number of days for a seesion to have been inactive for it to time out
        /// </summary>
        public static int AuthenticationServiceTimeoutDays
        {
            get
            {
                if (ConfigurationManager.AppSettings["Datamonitor.Permissions.AuthenticationServiceTimeoutDays"] != null)
                {
                    int outTimeOutDays;
                    int.TryParse(ConfigurationManager.AppSettings["Datamonitor.Permissions.AuthenticationServiceTimeoutDays"],
                        out outTimeOutDays);
                    return outTimeOutDays;
                }
                else
                {
                    throw new ApplicationException(Res.GetString("TIMEOUTDAYS_EXCEPTION_MESSAGE"));
                }
            }
        }

        /// <summary>
        /// Gets the number of minutes that has to pass before a user's locked account and be signed into again
        /// </summary>
        public static int SignInResetMinutes
        {
            get
            {
                if (ConfigurationManager.AppSettings["Datamonitor.Permissions.SignInResetMinutes"] != null)
                {
                    int outTimeOutMinutes;
                    int.TryParse(ConfigurationManager.AppSettings["Datamonitor.Permissions.SignInResetMinutes"],
                        out outTimeOutMinutes);
                    return outTimeOutMinutes;
                }
                else
                {
                    throw new ApplicationException(Res.GetString("SIGNINRESETMINUTES_EXCEPTION_MESSAGE"));
                }

            }

        }

        /// <summary>
        /// Gets the number sign in attempts a user can make before their account is locked
        /// </summary>
        public static int MaximumSignInAttempts
        {
            get
            {
                if (ConfigurationManager.AppSettings["Datamonitor.Permissions.MaximumSignInAttempts"] != null)
                {
                    int outTimeOutMinutes;
                    int.TryParse(ConfigurationManager.AppSettings["Datamonitor.Permissions.MaximumSignInAttempts"],
                        out outTimeOutMinutes);
                    return outTimeOutMinutes;
                }
                else
                {
                    throw new ApplicationException(Res.GetString("MAXIMUMSIGNINATTEMPTS_EXCEPTION_MESSAGE"));
                }

            }

        }

        /// <summary>
        /// Gets the timeout of the request change password signature
        /// </summary>
        public static int SignatureTimeoutDays
        {
            get
            {
                if (ConfigurationManager.AppSettings["Datamonitor.Permissions.SignatureTimeoutDays"] != null)
                {
                    int outTimeOutDays;
                    int.TryParse(ConfigurationManager.AppSettings["Datamonitor.Permissions.SignatureTimeoutDays"],
                        out outTimeOutDays);
                    return outTimeOutDays;
                }
                else
                {
                    throw new ApplicationException(Res.GetString("SIGNATURE_TIMEOUTDAYS_EXCEPTION_MESSAGE"));
                }
            }
        }

    }
}
