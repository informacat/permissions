using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Xml;

namespace Datamonitor.Permissions.Utilities
{
    public class ExclusionRuleSectionHandler : IConfigurationSectionHandler
    {
        public ExclusionRuleSectionHandler() { }

        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {   
            XmlNodeList ruleNodes = section.SelectNodes("Rule");
            List<string> rules = new List<string>(ruleNodes.Count);

            foreach (XmlNode ruleNode in ruleNodes)
            {
                rules.Add(ruleNode.Attributes["Name"].InnerText);                
            }

            return rules.ToArray();
        }

    }
}
