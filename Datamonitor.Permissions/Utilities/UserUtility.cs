using System;
using Datamonitor.DataModel;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.Data.Hashing;
using System.Collections.Generic;

namespace Datamonitor.Permissions.Utilities
{
    internal static class UserUtility
    {
        /// <summary>
        /// Get the single sign on user for a contract.  This will create it if it could
        /// not be found.
        /// </summary>
        /// <param name="contractId">The id of the contract to find the user for.</param>
        /// <param name="dataService">The data service storing users.</param>
        /// <returns>The user to use for the contract.</returns>
        internal static User GetSingleSignOnUser(Contract contract, IDataService dataService)
        {
            User singleSignOnUser = FindSharedUserForContract(dataService, contract.Identifier);

            if (singleSignOnUser == null)
            {
                // Couldn't find a user, so create a new one to use
                singleSignOnUser = BuildSingleSignOnUser(contract);
                singleSignOnUser = dataService.PersistUser(singleSignOnUser);
                singleSignOnUser = dataService.PersistUserAuthentication(singleSignOnUser);
            }

            return singleSignOnUser;
        }

        /// <summary>
        /// Try to find a user for a contract.
        /// </summary>
        /// <param name="dataService">The data service to use to find users.</param>
        /// <param name="contractId">The id of the contract to find a user for.</param>
        /// <returns>A suitable user for the contract, or null if not found.</returns>
        private static User FindSharedUserForContract(IDataService dataService, Guid contractId)
        {
            User user = null;

            // Find candidate users
            List<User> potentialUsers = dataService.GetSharedUsers(contractId);

            // Try to pick a user
            foreach (User possibleUser in potentialUsers)
            {
                if (dataService.GetUser(possibleUser.Username) != null)
                {
                    user = possibleUser;
                    break;
                }
            }

            return user;
        }

        /// <summary>
        /// Build a single sign on user.
        /// </summary>
        /// <param name="contract">The contract the user is for.</param>
        /// <returns>A new user object, not present in backing store.</returns>
        private static User BuildSingleSignOnUser(Contract contract)
        {
            User user = new User();
            user.Guid = Guid.NewGuid();
            user.Username = user.Guid.ToString();
            user.FirstName = "Shared";
            user.LastName = "User";
            user.Email = string.Empty;
            user.JobFunction = String.Empty;
            user.Address = String.Empty;
            user.City = String.Empty;
            user.PostCode = String.Empty;
            user.Telephone = String.Empty;
            user.ReceiveRegularUpdates = false;
            user.IsGatekeeper = false;
            user.IsSharedPassword = true;
            user.ContractID = contract.Identifier;
            user.IsIPRestrict = false;
            user.IsAccountActive = true;
            user.IsChangePasswordNextLogin = false;
            user.PurchasedAmountToDate = 0;
            user.PurchaseLimit = 0;
            user.IsBillingCodeRequired = false;
            user.PreferredCurrency = String.Empty;
            user.IsNewUser = true;
            user.IsAffiliateUser = false;
            user.IsPressUser = false;
            user.IsMarketingContact = false;
            user.IsNotifyOfNewProducts = false;
            user.UserType = 0;
            user.DateCreated = DateTime.Now;
            user.IsAutoLogin = false;
            user.PreferredEmailFormat = String.Empty;
            user.Interests = String.Empty;
            user.SectorForURLRedirection = String.Empty;
            user.SalesContact = String.Empty;
            user.JobTitle = String.Empty;
            user.State = String.Empty;
            user.Fax = String.Empty;
            user.Title = String.Empty;
            user.Country = String.Empty;
            user.Password = new Password(HashUtilities.GeneratePassword());

            return user;
        }

        internal static User GetAdHocUser(IDataService dataService, Contract contract, string userEmail, string companyName, string firstName, string lastName, string password)
        {
            User adHocUser = BuildAdHocUser(dataService, contract, userEmail, companyName, firstName, lastName, password);
            adHocUser = dataService.PersistUser(adHocUser);
            adHocUser = dataService.PersistUserAuthentication(adHocUser);

            return adHocUser;
        }

        private static User BuildAdHocUser(IDataService dataService, Contract contract, string userEmail, string companyName, string firstName, string lastName, string password)
        {
            User user = new User();
            user.Guid = Guid.NewGuid();
            user.Username = GeneratedUserName(dataService, firstName, lastName);
            user.FirstName = firstName;
            user.LastName = lastName;
            user.Email = userEmail;
            user.JobFunction = String.Empty;
            user.Address = companyName;
            user.City = String.Empty;
            user.PostCode = String.Empty;
            user.Telephone = String.Empty;
            user.ReceiveRegularUpdates = false;
            user.IsGatekeeper = false;
            user.IsSharedPassword = true;
            user.ContractID = contract.Identifier;
            user.IsIPRestrict = false;
            user.IsAccountActive = true;
            user.IsChangePasswordNextLogin = false;
            user.PurchasedAmountToDate = 0;
            user.PurchaseLimit = 0;
            user.IsBillingCodeRequired = false;
            user.PreferredCurrency = String.Empty;
            user.IsNewUser = true;
            user.IsAffiliateUser = false;
            user.IsPressUser = false;
            user.IsMarketingContact = false;
            user.IsNotifyOfNewProducts = false;
            user.UserType = 0;
            user.DateCreated = DateTime.Now;
            user.IsAutoLogin = false;
            user.PreferredEmailFormat = String.Empty;
            user.Interests = String.Empty;
            user.SectorForURLRedirection = String.Empty;
            user.SalesContact = String.Empty;
            user.JobTitle = String.Empty;
            user.State = String.Empty;
            user.Fax = String.Empty;
            user.Title = String.Empty;
            user.Country = String.Empty;
            user.Password = new Password(password);

            return user;
        }

        private static string GeneratedUserName(IDataService dataService, string firstName, string lastName)
        {
            const int NAME_ABBREV_LENGTH = 3;
            const string USERNAME_FORMAT = "{0}_{1}";
            const char PADDING = '0';

            int firstNameLength = firstName.Length;
            int lastNameLength = lastName.Length;

            string firstNameAbbrev = firstNameLength < NAME_ABBREV_LENGTH ? firstName : firstName.Substring(0, NAME_ABBREV_LENGTH);
            string lastNameAbbrev = lastNameLength < NAME_ABBREV_LENGTH ? lastName : lastName.Substring(0, NAME_ABBREV_LENGTH);

            string userName = string.Format(USERNAME_FORMAT, firstNameAbbrev, lastNameAbbrev);
            bool isUserNameExist = dataService.CheckUserExists(userName);
            string userNameTemp = userName;
            int index = 1;
            do
            {   
                if (isUserNameExist)
                {
                    userNameTemp = String.Format(USERNAME_FORMAT, userName, index.ToString().PadLeft(3, PADDING));
                    isUserNameExist = dataService.CheckUserExists(userNameTemp);
                    index++;
                }
            }
            while (isUserNameExist);

            return userNameTemp;
        }
    }
}
