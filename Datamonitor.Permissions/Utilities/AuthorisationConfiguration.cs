using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Datamonitor.Permissions.Utilities
{
    public static class AuthorisationConfiguration
    {
        public static string[] ExcludedRules
        {
            get
            {
                return ConfigurationManager.GetSection("Authorisation/ExclusionRules") as string[];
            }
        }
    }
}
