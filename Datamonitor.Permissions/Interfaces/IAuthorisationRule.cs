using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel;

namespace Datamonitor.Permissions.Interfaces
{
    [Flags]
    public enum RuleType
    {
        CompanyActiveRule = 0,
        ContractActiveRule = 1,
        IPAddressRangeRule = 2,
        ContractDateRangeRule = 4
    }

    public interface IAuthorisationRule
    {
        string Name { get; set; }
        bool IsValid(Session session);
    }
}
