using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel;
using Datamonitor.Permissions.Entities;

namespace Datamonitor.Permissions.Interfaces
{
    public interface IAuthenticationService
    {
        AuthenticationResult Authenticate();
        //AuthenticationResult Authenticate(string username, string password, Uri referrer);
        //AuthenticationResult Authenticate(string username, string password, long ipAddress);

        //List<Session> GetActiveSessions();
        //List<Session> GetActiveSessionsForUser(string username);
        //Session NewSession(string username, DateTime createDate, int timeOutMinutes);

        //User GetUser(string username);
        //User NewUserCredentials(string username, string password, string contractId);
        
        //bool IsValid(string sessionToken);
        //bool TerminateSession(string sessionToken);
    }
}
