using System;
using System.Collections.Generic;
using System.Text;

namespace Datamonitor.Permissions.Interfaces
{
    public interface ISymetricKey
    {
        string Encrypt(string unencodedText, string privateKey, string initVector);
        string Decrypt(string encodedText, string privateKey, string initVector);
    }
}
