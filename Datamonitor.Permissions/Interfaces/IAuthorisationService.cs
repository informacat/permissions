using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel;
using Datamonitor.Permissions.Entities;

namespace Datamonitor.Permissions.Interfaces
{
    public interface IAuthorisationService
    {
        AuthorisationResult Authorise(Guid authenticationToken);
    }
}
