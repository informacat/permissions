using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.DataModel;

namespace Datamonitor.Permissions.Rules
{
    public class CompanyActiveRule : BaseRule
    {
        public CompanyActiveRule(string name) : base(name) { }

        public override bool IsValid(Session session)
        {
            return session.User.Contract.Company.IsActive;
        }
    }
}
