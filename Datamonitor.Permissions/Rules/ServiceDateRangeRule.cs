using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel;

namespace Datamonitor.Permissions.Rules
{
    public class ServiceDateRangeRule : BaseRule
    {
        private DateTime dateTime;
        private string serviceIdentifier;

        public ServiceDateRangeRule(string name, DateTime dateTime, string serviceIdentifier)
            :base(name)
        {
            this.dateTime = dateTime;
            this.serviceIdentifier = serviceIdentifier;
        }

        public override bool IsValid(Session session)
        {
            foreach (ContractService service in session.User.Contract.Services)
            {
                if (service.Identifier == this.serviceIdentifier)
                {
                    return service.DateRange.IsBetween(this.dateTime);
                }
            }
            return false;
        }
    }
}
