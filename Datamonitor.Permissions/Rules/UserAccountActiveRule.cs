using System;
using System.Collections.Generic;
using System.Text;

namespace Datamonitor.Permissions.Rules
{
    /// <summary>
    /// Rule that checks if the session's User account is set to active
    /// </summary>
    public class UserAccountActiveRule : BaseRule
    {
        /// <summary>
        /// Creates a new rule with it's name
        /// </summary>
        /// <param name="name"></param>
        public UserAccountActiveRule(string name)
            :base(name) { }

        /// <summary>
        /// Returns true if a user account is active
        /// </summary>
        /// <param name="session">current session</param>
        /// <returns>true if active</returns>
        public override bool IsValid(Datamonitor.DataModel.Session session)
        {
            if (session != null)
            {
                return session.User == null ? false : session.User.IsAccountActive;
            }

            return false;
        }
    }
}
