using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.DataModel;

namespace Datamonitor.Permissions.Rules
{
    public class ContractDateRangeRule : BaseRule
    {
        private DateTime dateTime;
        public ContractDateRangeRule(string name, DateTime dateTime):base(name)
        {
            this.dateTime = dateTime;
        }

        public override bool IsValid(Session session)
        {
            return session.User.Contract.DateRange.IsBetween(this.dateTime);
        }
    }
}
