using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel;
using Datamonitor.Permissions.Rules;
using Datamonitor.DataModel.Structures;

namespace Datamonitor.Permissions.Rules
{
    /// <summary>
    /// Rule to check the ip address ranges set on a contract
    /// </summary>
    public class IpAddressRangeRule : BaseRule
    {
        private long ipAddress;

        /// <summary>
        /// Creates a new contract ip address ranges rule
        /// </summary>
        /// <param name="name">rule name</param>
        /// <param name="ipAddress">ip address as a int 64</param>
        public IpAddressRangeRule(string name, long ipAddress)
            : base(name)
        {
            this.ipAddress = ipAddress;
        }

        /// <summary>
        /// Returns true if the given address is in a range or if 
        /// the contract does not have an ip range
        /// </summary>
        /// <param name="session">current session</param>
        /// <returns>true if there is an ip in rane or if no ranges exist for contract</returns>
        public override bool IsValid(Session session)
        {
            // Only ip restricted if there are ranges set on a contract

                if (session.User.Contract.IPRanges.Length > 0 && session.User.Contract.IsIPRestrict)
                {
                    foreach (IPAddressRange range in session.User.Contract.IPRanges)
                    {
                        if (range.IsBetween(ipAddress))
                        {
                            return true;
                        }
                    }
                    return false;
                }
                else
                {
                    return true;
                }
        }
    }
}
