using System;
using System.Collections.Generic;
using System.Text;

namespace Datamonitor.Permissions.Rules
{
    public class SessionActiveRule : BaseRule
    {
        public SessionActiveRule(string name)
            :base(name) { }

        public override bool IsValid(Datamonitor.DataModel.Session session)
        {
            return !session.IsExpired;
        }
    }
}
