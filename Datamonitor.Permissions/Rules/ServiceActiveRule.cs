using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel;

namespace Datamonitor.Permissions.Rules
{
    /// <summary>
    /// Rule that checks that the service is active
    /// </summary>
    public class ServiceActiveRule : BaseRule
    {
        private string serviceIdentifier;

        /// <summary>
        /// Creates the new rule
        /// </summary>
        /// <param name="name">rule name as string</param>
        /// <param name="serviceIdentifier">string service identifier</param>
        public ServiceActiveRule(string name, string serviceIdentifier)
            :base(name)
        {
            this.serviceIdentifier = serviceIdentifier;
        }

        /// <summary>
        /// Returns true if the service is active
        /// </summary>
        /// <param name="session">current session</param>
        /// <returns>true if sercive is active</returns>        
        public override bool IsValid(Session session)
        {
            List<ContractService> csList = new List<ContractService>(session.User.Contract.Services);

            ContractService currentCS = csList.Find(
                delegate(ContractService cs)
                {
                    return cs.Identifier == serviceIdentifier;
                }
                );

            return currentCS == null ? false : currentCS.IsServiceActive;

        }
    }
}
