using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel;
using Datamonitor.DataModel.Structures;

namespace Datamonitor.Permissions.Rules
{
    /// <summary>
    /// Rule that checks if the session's User account is set be ip restricted
    /// </summary>
    public class UserIPRestrictRule : BaseRule
    {
        private long ipAddress;

        /// <summary>
        /// Creates a new ip restrict rule
        /// </summary>
        /// <param name="name">rule name as string</param>
        /// <param name="ipAddress">ip address as int 64</param>
        public UserIPRestrictRule(string name, long ipAddress)
            : base(name)
        {
            this.ipAddress = ipAddress;
        }

        /// <summary>
        /// Returns true if a user is not iprestrcit or if the given ip is in range
        /// </summary>
        /// <param name="session">current session</param>
        /// <returns>true if iprestrict is in range or if not set to iprestrict</returns>        
        public override bool IsValid(Session session)
        {
            if (session == null)
            {
                return false;
            }

            if (session.User == null)
            {
                return false;
            }

            // If there are no ipranges but the user is set
            // to ip resrtict then it must return false.
            // If there are ip ranges then they are checked.
            // If the user is not set to ip restrict then 
            // return as valid for the rule.

            if (session.User.IsIPRestrict)
            {
                if (session.User.Contract.IPRanges.Length > 0)
                {
                    foreach (IPAddressRange range in session.User.Contract.IPRanges)
                    {
                        if (range.IsBetween(ipAddress))
                        {
                            return true;
                        }
                    }
                    return false;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
            
        }
    }
}
