using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel;

namespace Datamonitor.Permissions.Rules
{
    public class ServiceAuthorisedRule : BaseRule
    {
        private string serviceIdentifier;

        public ServiceAuthorisedRule(string name, string serviceIdentifier)
            :base(name)
        {
            this.serviceIdentifier = serviceIdentifier;
        }

        public override bool IsValid(Session session)
        {
            foreach (ContractService service in session.User.Contract.Services)
            {
                if (service.Identifier == this.serviceIdentifier)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
