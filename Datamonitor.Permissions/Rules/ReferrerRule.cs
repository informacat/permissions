using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel;

namespace Datamonitor.Permissions.Rules
{
    public class ReferrerRule : BaseRule
    {
        private string referrer;

        public ReferrerRule(string name, string referrer)
            :base(name)
        {
            this.referrer = referrer;
        }

        public override bool IsValid(Session session)
        {
            return referrer == session.User.Contract.Referrer;
        }
    }
}
