using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel;

namespace Datamonitor.Permissions.Rules
{
    /// <summary>
    /// Rule that checks that the service is active for the user's contract
    /// </summary>
    public class ServiceOnContractActiveRule : BaseRule
    {
        private string serviceIdentifier;

        /// <summary>
        /// Creates the new rule
        /// </summary>
        /// <param name="name">rule name as string</param>
        /// <param name="serviceIdentifier">string service identifier</param>
        public ServiceOnContractActiveRule(string name, string serviceIdentifier)
            :base(name)
        {
            this.serviceIdentifier = serviceIdentifier;
        }

        /// <summary>
        /// Returns true if a user's service on a contract is set to active
        /// </summary>
        /// <param name="session">current session</param>
        /// <returns>true if sercive is active on a contract</returns>        
        public override bool IsValid(Session session)
        {
            List<ContractService> csList = new List<ContractService>(session.User.Contract.Services);

            ContractService currentCS = csList.Find(
                delegate(ContractService cs)
                {
                    return cs.Identifier == serviceIdentifier;
                }
                );

            return currentCS == null ? false : currentCS.IsActive;

        }
    }
}
