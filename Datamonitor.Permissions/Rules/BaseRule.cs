using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.DataModel;

namespace Datamonitor.Permissions.Rules
{
    public abstract class BaseRule : IAuthorisationRule
    {
        private string name;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public BaseRule(string name)
        {
            this.name = name;
        }

        public abstract bool IsValid(Session session);
    }
}
