@ECHO OFF
SETLOCAL

REM Read command line params
SET OutputFile=%1
SET ConfigFile=%2

REM Remove leading and trailing character from params (quotes)
SET OutputFileUnq=%OutputFile:~1,-1%
SET ConfigFileUnq=%ConfigFile:~1,-1%

:Main
FC "%OutputFileUnq%" "%ConfigFileUnq%"
IF ERRORLEVEL 1 (
	ECHO Files are not the same.
	ECHO Copying "%ConfigFileUnq" over "%OutputFileUnq"
	COPY "%ConfigFileUnq%" "%OutputFileUnq%" /Y
	)