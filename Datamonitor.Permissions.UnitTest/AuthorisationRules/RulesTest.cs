using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Datamonitor.Permissions.Rules;
using Datamonitor.Permissions.Entities;
using Datamonitor.DataModel;

namespace Datamonitor.Permissions.UnitTest.AuthorisationRules
{
    [TestFixture]
    public class Rules
    {
        private Session session;

        [SetUp]
        public void SetUp()
        {
            session = TestDataLoader.AuthenticatedSession();
        }

        [Test]
        public void Company_Is_Active()
        {
            CompanyActiveRule companyActive = new CompanyActiveRule("CompanyActiveRuleTest");
            Assert.IsTrue(companyActive.IsValid(session));
        }

        [Test]
        public void Contract_Is_Active()
        {
            ContractActiveRule contractActive = new ContractActiveRule("ContractActiveRuleTest");
            Assert.IsTrue(contractActive.IsValid(session));
        }

        [Test]
        public void Date_Is_Within_Contract_Date_Range()
        {
            ContractDateRangeRule contractDateRange = new ContractDateRangeRule("ContractDateRangeRuleTest", DateTime.Now);
            Assert.IsTrue(contractDateRange.IsValid(session));
        }

        [Test]
        public void IpAddress_Is_Within_IpAddress_Range()
        {
            IpAddressRangeRule ipAddressRange = new IpAddressRangeRule("IPAddressRangeRuleTest", 100000000011);
            Assert.IsTrue(ipAddressRange.IsValid(session));
        }

        [Test]
        public void Date_Is_Within_Service_Date_Range()
        {
            ServiceDateRangeRule serviceDateRange = new ServiceDateRangeRule("ServiceDateRangeRuleTest", DateTime.Now, "1");
            Assert.IsTrue(serviceDateRange.IsValid(session));
        }
    }
}
