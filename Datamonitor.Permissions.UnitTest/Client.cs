using System;
using System.Collections.Generic;
using System.Text;

namespace Datamonitor.Permissions.UnitTest
{
    public struct Client
    {
        public string Username;
        public string Password;
        public long IPAddress;
    }
}
