using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Datamonitor.Permissions.Entities;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.Permissions.Providers;
using Datamonitor.DataModel;

namespace Datamonitor.Permissions.UnitTest.Authorisation
{
    [TestFixture]
    public class KCAuthorisationService
    {
        private Session session;

        [SetUp]
        public void SetUp()
        {
            session = TestDataLoader.AuthenticatedSession();
        }


        [Test]
        public void User_Is_Authorised()
        {
            IAuthorisationService authService = AuthorisationProvider.GetKCAuthorisationService("1", 127000000001);
            AuthorisationResult result = authService.Authorise(session.Identifier);
            Assert.IsTrue(result.IsAuthorised);
        }

        [Test]
        public void User_Is_Not_Authorised()
        {
            IAuthorisationService authService = AuthorisationProvider.GetKCAuthorisationService("3", 127000000001);
            AuthorisationResult result = authService.Authorise(session.Identifier);
            Assert.IsFalse(result.IsAuthorised);
        }
    }
}
