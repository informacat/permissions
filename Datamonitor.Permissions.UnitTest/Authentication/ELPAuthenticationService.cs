using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Datamonitor.Permissions.Entities;
using System.Collections;
using Datamonitor.Permissions.Services;
using Datamonitor.Permissions.Utilities;
using Datamonitor.Permissions.Providers;
using Datamonitor.Permissions.Interfaces;
using System.Configuration;

namespace Datamonitor.Permissions.UnitTest.Authentication
{
    [TestFixture]
    public class ELPAuthenticationService
    {
        private IAuthenticationService authenticatedService;
        private IAuthenticationService nonAuthenticatedService;    
        private string elpKey;

        [SetUp]
        public void SetUp()
        {
            elpKey = ConfigurationManager.AppSettings["ekey"];

            authenticatedService = AuthenticationProvider.GetELPAuthenticationService(
                TestDataLoader.EncryptedLogins()[0], elpKey);

            nonAuthenticatedService = AuthenticationProvider.GetELPAuthenticationService(
                TestDataLoader.EncryptedLogins()[1], elpKey);
        }

        [Test]
        public void User_Is_Authenticated()
        {
            AuthenticationResult authResult = authenticatedService.Authenticate();
            Assert.IsTrue(authResult.IsAuthenticated);
        }

        [Test]
        public void User_Is_Provided_New_Authentication_Token()
        {
            AuthenticationResult authResult = authenticatedService.Authenticate();
            Assert.IsFalse(authResult.AuthenticationToken == Guid.Empty);
        }

        [Test]
        public void User_Is_Not_Authenticated()
        {
            AuthenticationResult authResult = nonAuthenticatedService.Authenticate();
            Assert.IsFalse(authResult.IsAuthenticated);
        }

        [Test]
        public void User_Is_Not_Provided_New_Authentication_Token()
        {
            AuthenticationResult authResult = nonAuthenticatedService.Authenticate();
            Assert.IsTrue(authResult.AuthenticationToken == Guid.Empty);
        }
    }
}
