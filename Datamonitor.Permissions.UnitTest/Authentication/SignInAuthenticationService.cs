using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Datamonitor.Permissions.Entities;
using System.Collections;
using Datamonitor.Permissions.Services;
using Datamonitor.Permissions.Utilities;
using Datamonitor.Permissions.Providers;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.DataModel;

namespace Datamonitor.Permissions.UnitTest.Authentication
{
    [TestFixture]
    public class SignInAuthenticationService
    {
        private IAuthenticationService authenticatedService;
        private IAuthenticationService nonAuthenticatedService;
        
        [SetUp]
        public void SetUp()
        {
            authenticatedService = AuthenticationProvider.GetSignInAuthenticationService(
                TestDataLoader.Users()[0].Username, TestDataLoader.Users()[0].Password);

            nonAuthenticatedService = AuthenticationProvider.GetSignInAuthenticationService(
                TestDataLoader.Users()[1].Username, TestDataLoader.Users()[1].Password);
        }

        [Test]
        public void User_Is_Authenticated()
        {
            AuthenticationResult authResult = authenticatedService.Authenticate();
            Assert.IsTrue(authResult.IsAuthenticated);
        }

        [Test]
        public void User_Is_Provided_New_Authentication_Token()
        {
            AuthenticationResult authResult = authenticatedService.Authenticate();
            Assert.IsTrue(authResult.AuthenticationToken != Guid.Empty);
        }
        [Test]
        public void User_Is_Not_Authenticated()
        {
            AuthenticationResult authResult = nonAuthenticatedService.Authenticate();
            Assert.IsFalse(authResult.IsAuthenticated);
        }

        [Test]
        public void User_Is_Not_Provided_New_Authentication_Token()
        {
            AuthenticationResult authResult = nonAuthenticatedService.Authenticate();
            Assert.IsTrue(authResult.AuthenticationToken == Guid.Empty);
        }
    }
}
