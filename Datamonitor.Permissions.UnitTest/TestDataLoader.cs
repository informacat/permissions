using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.Configuration;
using System.Collections;
using Datamonitor.Permissions.Entities;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.Permissions.Providers;
using Datamonitor.DataModel;
using Datamonitor.DataModel.DataAccess;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.Providers;

namespace Datamonitor.Permissions.UnitTest
{
    public static class TestDataLoader
    {

        public static Client[] Users()
        {
            List<Client> clients = new List<Client>();

            for (int i = 1; i < 10; i++)
            {
                Hashtable userDetail;
                string sectionName = String.Format("UserInformation/User{0}", i);
                object config = ConfigurationManager.GetSection(sectionName);
                if (config != null)
                {
                    userDetail = config as Hashtable;
                    Client client;
                    client.Username = userDetail["username"].ToString();
                    client.Password = userDetail["password"].ToString();
                    client.IPAddress = long.Parse(userDetail["clientipaddress"].ToString());
                    clients.Add(client);
                }
            }

            return clients.ToArray();

        }

        public static Session AuthenticatedSession()
        {
            IRepository repository = RepositoryProvider.GetRepository();

            return repository.GetSession(new Guid("{12FEB677-A123-4a23-9B9B-61F597AEA088}"));
        }

        public static string[] EncryptedLogins()
        {
            List<string> elps = new List<string>();
            
            for (int i = 1; i < 3; i++)
            {
                Hashtable userDetail;
                string sectionName = String.Format("UserInformation/ELP{0}", i);
                object config = ConfigurationManager.GetSection(sectionName);
                if (config != null)
                {
                    userDetail = config as Hashtable;
                    elps.Add(userDetail["elp"].ToString());
                }
            }

            return elps.ToArray();
        }
    }
}
