using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Datamonitor.Permissions.Interfaces;
using Datamonitor.Permissions.Providers;
using System.Configuration;
using Datamonitor.Permissions.Entities;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel;
using Datamonitor.DataModel.Providers;
using System.Reflection;
using System.Xml;

namespace Datamonitor.Permissions.UnitTest
{
    /// <summary>
    /// Provides asserting methods for testing with nunit
    /// </summary>
    [TestFixture]
    public class PermissionsUnitTest
    {
        private const string CONNECTIONSTRING_PARAM = "dmvProfiles";
        private const string EKEY_PARAM = "eKey";

        IAuthenticationService authService;

        [SetUp]
        public void SetUp()
        {

        }

        [Test]
        public void Authenticate_Credentials_Test()
        {
            authService = AuthenticationProvider.GetSignInAuthenticationService("usertest1@dm.com", "trythis");
            AuthenticationResult result = authService.Authenticate();
            Assert.IsTrue(result.IsAuthenticated);
        }

        [Test]
        public void Authenticate_IPServices_Test()
        {
            authService = AuthenticationProvider.GetIPAddressAuthenticationService(127000000001, new int[2] { 1, 2 });
            AuthenticationResult TResult = authService.Authenticate();

            IAuthorisationService authService = AuthorisationProvider.GetServiceAuthorisationService("2", 127000000001);
            AuthorisationResult SResult = authService.Authorise(TResult.AuthenticationToken);


            Assert.IsTrue(SResult.IsAuthorised);
        }

        [Test]
        public void Authenticate_EncryptedLogin_Test()
        {
            authService =
                AuthenticationProvider.GetELPAuthenticationService("QZE4xCKYHSRxDpunaxfCyPpD8gL1oTxl", ConfigurationManager.AppSettings[EKEY_PARAM]);
            AuthenticationResult result = authService.Authenticate();
            Assert.IsNotNull(result);
        }

        [Test]
        public void Authenticate_Token_Test()
        {
            authService = AuthenticationProvider.GetTokenAuthenticationService("{22FEB677-A123-4A23-9B9B-61F597AEA088}");
            AuthenticationResult result = authService.Authenticate();
            Assert.IsTrue(result.IsAuthenticated);
        }

        [Test]
        public void Authorise_Test()
        {
            IAuthorisationService authService = AuthorisationProvider.GetServiceAuthorisationService("2", 127000000001);
            AuthorisationResult result = authService.Authorise(new Guid("{32FEB677-A123-4A23-9B9B-61F597AEA088}"));
        }

        [Test]
        public void Is_User_Verified_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            User user = dataService.GetUser("usertest8@dm.com");
            Assert.IsNotNull(user);
        }

        [Test]
        public void Get_User_Exist_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            bool isThere = dataService.CheckUserExists("10134");
            Assert.IsTrue(isThere);
        }

        [Test]
        public void Get_User_Not_Exist_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            bool isThere = dataService.CheckUserExists("101.34");
            Assert.IsFalse(isThere);
        }

        [Test]
        public void Get_User_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            User user = dataService.GetUser("10134");
            Assert.IsNotNull(user);
            DebugShowObject(user);
        }

        [Test]
        public void Update_User_Autentication_Test()
        {
            string username = "usertest6@dm.com";

            IDataService dataService = ServiceProvider.GetDataService();
            User user = dataService.GetUser(username);
            User updatedUser = dataService.PersistUserAuthentication(user);
            Assert.IsNotNull(updatedUser);
            Assert.AreEqual(username, updatedUser.Username);
        }

        [Test]
        public void Get_Gatekeepers_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            User[] gatekeepers = dataService.GetGatekeepers(new Guid("{75DFD1C8-1C34-4e59-911E-149520A10759}"));
            Assert.IsNotNull(gatekeepers);
            Assert.IsNotEmpty(gatekeepers);
        }

        [Test]
        public void Get_Users_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            User[] users = dataService.GetUsers(new Guid("{75DFD1C8-1C34-4e59-911E-149520A10759}"));
            Assert.IsNotNull(users);
            Assert.IsNotEmpty(users);
        }

        [Test]
        public void Update_User_Test()
        {
            string username = "10169";
            string newAddress = "No.2 London";
            IDataService dataService = ServiceProvider.GetDataService();
            User user = dataService.GetUser(username);

            Assert.IsNotNull(user);

            user.FirstName = "chris";
            user.LastName = "clarke";
            user.JobFunction = "developer";
            user.Address = newAddress;
            user.City = "london";
            user.PostCode = "ec1r 3da";
            user.Country = "uk";
            user.Telephone = "1234";

            User updatedUser = dataService.PersistUser(user);

            Assert.IsNotNull(updatedUser);
            Assert.AreEqual(newAddress, updatedUser.Address);

            DebugShowObject(updatedUser);
        }

        [Test]
        public void Register_New_User_Test()
        {
            User user = new User();
            user.Guid = Guid.NewGuid();
            user.Username = "10134";
            user.FirstName = "firstname";
            user.LastName = "lastname";
            user.JobFunction = "jobFunction";
            user.Address = "address";
            user.City = "city";
            user.PostCode = "postCode";
            user.Country = "country";
            user.Telephone = "telephone";
            user.Email = "email";
            user.SalesContact = "salesContact";
            user.ContractID = new Guid("{44A3F29C-F407-412c-8341-811D36A8C9D4}");
            user.ReceiveRegularUpdates = true;
            user.IsGatekeeper = true;
            user.IsSharedPassword = true;
            user.IsIPRestrict = true;
            user.IsAccountActive = true;
            user.IsChangePasswordNextLogin = true;
            user.PurchasedAmountToDate = 0;
            user.PurchaseLimit = 0;
            user.IsBillingCodeRequired = true;
            user.PreferredCurrency = "";
            user.IsNewUser = true;
            user.IsAffiliateUser = true;
            user.IsPressUser = true;
            user.IsMarketingContact = true;
            user.IsNotifyOfNewProducts = true;
            user.UserType = 2;
            user.DateCreated = DateTime.Now;
            user.IsAutoLogin = true;
            user.PreferredEmailFormat = "";
            user.Interests = "";
            user.SectorForURLRedirection = "";
            user.Password = new Password("hash", "12345678");

            IDataService dataService = ServiceProvider.GetDataService();

            User persistedUser = dataService.PersistUser(user);
            Assert.IsNotNull(persistedUser);
        }

        [Test]
        public void Get_Session_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            //Session session = dataService.GetSession(new Guid(authenticationToken));
        }

        [Test]
        public void Get_Contract_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            Contract contract = dataService.GetContract("213");
        }

        [Test]
        public void Get_Contract_Services_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            //ContractService[] contractServices = dataService.GetContractServices(contractId);
        }

        [Test]
        public void Get_Company_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            //Company company = dataService.GetCompany(companyId);
        }

        [Test]
        public void Get_Signature_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            //Signature signature = dataService.GetSignature(signatureId);
        }

        [Test]
        public void Persist_Session_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            //Session session = dataService.GetSession(new Guid(authenticationToken));
            //session.TimeOutDays = AuthenticationConfiguration.AuthenticationServiceTimeoutDays;
            //dataService.PersistSession(session);
        }

        [Test]
        public void Create_Signature_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();

            //Signature signature = new Signature();
            //signature.UserName = userName;
            //signature.TimeOutDays = AuthenticationConfiguration.SignatureTimeoutDays;
            //signature.IsActive = true;
        }

        [Test]
        public void Update_Signature_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            //return dataService.UpdateSignature(signature);
        }

        [Test]
        public void Kill_Session_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            //Session session = dataService.GetSession(new Guid(authenticationToken));
            //dataService.KillSession(session);
        }

        [Test]
        public void Kill_Sessions_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            //User user = dataService.GetUser(username);
            //dataService.KillAllUserSessions(user);
        }

        [Test]
        public void Deactivate_User_Test()
        {
            IDataService dataService = ServiceProvider.GetDataService();
            //return dataService.DeactivateUser(username, contractId);
        }

        private void DebugShowObject(IModel model)
        {
            Type modelType = model.GetType();

            Console.WriteLine(model.GetType().ToString());

            foreach (PropertyInfo pi in modelType.GetProperties())
            {
                try
                {
                    Console.WriteLine(string.Format("{0} = \"{1}\";", pi.Name, pi.GetValue(model, null)));
                }
                catch { }
            }

        }
    }
}
