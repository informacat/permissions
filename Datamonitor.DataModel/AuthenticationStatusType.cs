using System;
using System.Collections.Generic;
using System.Text;

namespace Datamonitor.DataModel
{    
    /// <summary>
    /// Specifies the staus of an authentication result
    /// </summary>
    public enum AuthenticationStatusType
    {
        /// <summary>
        /// The authentication is bad
        /// </summary>
        Bad,
        /// <summary>
        /// The authentication is good
        /// </summary>
        Good,
        /// <summary>
        /// The authentication used a bad password
        /// </summary>
        BadPassword,
        /// <summary>
        /// The authentication used a bad username
        /// </summary>
        BadUsername,
        /// <summary>
        /// The authentication used a bad username and password combination
        /// </summary>
        BadUsernamePassword,
        /// <summary>
        /// The authentication attempt has reached its maximum
        /// </summary>
        ExceededMaxAttempts,
        /// <summary>
        /// The encrypted login text cannot be decrypted or used
        /// </summary>
        BadELPText,
        /// <summary>
        /// The authentication token is not valid
        /// </summary>
        BadAuthenticationToken,
        /// <summary>
        /// The user is inactive
        /// </summary>
        UserInactive
    }
}
