using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.DataAccess;
using Datamonitor.DataModel.Utilities;

namespace Datamonitor.DataModel.Providers
{
    public static class RepositoryProvider
    {
        public static IRepository GetRepository()
        {   
            Type repositoryType = Type.GetType(ModelConfiguration.RepositoryProvider);
            Object repositoryObject = Activator.CreateInstance(repositoryType);
            return repositoryObject as IRepository;          
        }
    }
}
