using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.DataAccess;

namespace Datamonitor.DataModel.Providers
{
    public static class ServiceProvider
    {
        public static IDataService GetDataService()
        {
            return new DataService();
        }
    }
}
