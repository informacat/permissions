using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.Providers;

namespace Datamonitor.DataModel
{
    public abstract class BaseModel : IModel
    {
        private IDataService dataService;
        public IDataService DataService
        {
            get
            {
                if (dataService == null)
                {
                    dataService = ServiceProvider.GetDataService();
                }
                return dataService;
            }
        }
    }
}
