using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.Data.Hashing;

namespace Datamonitor.DataModel
{
    public class Password
    {
        private Hash Hash;
        public string PasswordHash;
        public string Salt;
        public int SignInAttempts;
        public DateTime LastSignInAttempt;

        // Constructors
        public Password(string plainTextPassword)
        {
            this.Salt = HashUtilities.GenerateSalt();
            this.Hash = HashProvider.GetHash(this.Salt);
            this.PasswordHash = Hash.ComputeHash(plainTextPassword);
        }

        public Password(string hashedPassword, string salt)
        {
            this.Salt = salt;
            this.Hash = HashProvider.GetHash(this.Salt);
            this.PasswordHash = hashedPassword;
        }

        public Password() { }

        // * Overloaded Operators
        public static bool operator ==(Password password, string passwordString)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(password, passwordString))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)password == null) || ((object)passwordString == null))
            {
                return false;
            }

            // Return true if the password hash match
            return password.Hash.VerifyHash(passwordString, password.PasswordHash);
        }

        public static bool operator !=(Password password, string passwordString)
        {
            return !(password == passwordString);
        }

        // Method Overrides
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
    }
}
