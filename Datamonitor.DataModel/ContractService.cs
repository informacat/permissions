using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel.Structures;

namespace Datamonitor.DataModel
{
    public class ContractService : IEquatable<ContractService>
    {
        private string name;
        private string identifier;
        private DateRange dateRange;
        private bool isActive;
        private bool isServiceActive;
        private bool isOwnershipRestricted;

        public ContractService(){}

         public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public string Identifier
        {
            get { return this.identifier; }
            set { this.identifier = value; }
        }

        public DateRange DateRange
        {
            get { return this.dateRange; }
            set { this.dateRange = value; }
        }

        public bool IsActive
        {
            get { return this.isActive; }
            set { this.isActive = value; }
        }

        public bool IsServiceActive
        {
            get { return this.isServiceActive; }
            set { this.isServiceActive = value; }
        }

        public bool IsOwnershipRestricted
        {
            get { return this.isOwnershipRestricted; }
            set { this.isOwnershipRestricted = value; }
        }

        public override string ToString()
        {
            return this.Name;
        }

        #region IEquatable<ContractService> Members

        public bool Equals(ContractService other)
        {
            return Identifier == other.Identifier;
        }

        #endregion
    }
}
