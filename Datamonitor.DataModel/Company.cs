using System;
using System.Collections.Generic;
using System.Text;

namespace Datamonitor.DataModel
{
    public class Company : IEquatable<Company>
    {
        private string name;
        private Guid identifier;
        private bool isActive;
        private string type;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public Guid Identifier
        {
            get { return this.identifier; }
            set { this.identifier = value; }
        }

        public bool IsActive
        {
            get { return this.isActive; }
            set { this.isActive = value; }
        }

        public string Type
        {
            get { return this.type; }
            set { this.type = value; }
        }

        public Company()
        {
        }

        public override string ToString()
        {
            return this.Name;
        }

        #region IEquatable<Company> Members

        public bool Equals(Company other)
        {
            return this.Identifier == other.Identifier;
        }

        #endregion
    }
}
