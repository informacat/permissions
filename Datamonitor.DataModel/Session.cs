using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Xml;

namespace Datamonitor.DataModel
{
    public class Session : IEquatable<Session>, IComparable<Session>
    {
        private Guid identifier;
        private DateTime createDate;
        private DateTime expiryDate;
        private User user;
        private int timeoutDays;
        private bool isExpired;

        public Session(Guid identifier)
        {
            this.identifier = identifier;           
        }

        public Session() { }

        public Guid Identifier
        {
            get
            {
                return this.identifier;
            }
            set
            {
                this.identifier = value;
            }
        }
        public DateTime CreateDate
        {
            get
            {
                return this.createDate;
            }
            set
            {
                this.createDate = value;
            }
        }
        public int TimeOutDays
        {
            get
            {
                return this.timeoutDays;
            }
            set
            {
                this.timeoutDays = value;
            }
        }
        public DateTime ExpiryDate
        {
            get
            {
                return this.expiryDate;
            }
            set
            {
                this.expiryDate = value;
            }
        }
        public bool IsExpired
        {
            get
            {
                return this.isExpired;
            }
            set
            {
                this.isExpired = value;
            }
        }
        public User User
        {
            get
            {
                return this.user;
            }
            set
            {
                this.user = value;
            }
        }
        public override string ToString()
        {
            return this.Identifier.ToString();
        }

        #region IEquatable<Session> Members

        public bool Equals(Session other)
        {
            return this.Identifier == other.Identifier;
        }

        #endregion

        #region IComparable<Session> Members

        public int CompareTo(Session other)
        {
            return this.CreateDate.CompareTo(other.CreateDate);
        }

        #endregion

    }
}
