using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.Data.Hashing;

namespace Datamonitor.DataModel
{
    public class User : BaseModel, IEquatable<User>
    {
        // Private Fields
        private Guid guid;
        private string username;
        private string firstname;
        private string lastname;
        private string jobFunction;
        private string address;
        private string city;
        private string postCode;
        private string country;
        private string telephone;
        private string email;
        private string salesContact;
        private Guid contractId; 
        private string preferredCurrency;
        private string preferredEmailFormat;
        private string interests;
        private string sectorForURLRedirection;
        private string jobTitle;
        private string state;
        private string fax;
        private string title;

        private decimal purchasedAmountToDate;
        private decimal purchaseLimit;
        private int userType;

        private bool receiveRegularUpdates;
        private bool isGatekeeper;
        private bool isSharedPassword;
        private bool isIPRestrict;        
        private bool isAccountActive;
        private bool isChangePasswordNextLogin;
        private bool isBillingCodeRequired;
        private bool isNewUser;
        private bool isAffiliateUser;
        private bool isPressUser;
        private bool isMarketingContact;
        private bool isNotifyOfNewProducts;
        private bool isAutoLogin;


        private DateTime dateCreated;

        private Password password;
        private Contract contract;

        private bool isProfileUpdated;
        private UserService userService;

     
        // Constructor
        public  User() { }

        public UserService UserService
        {
            get{return this.userService;}
            set { this.userService = value; }
        }   

        // Public Properties
        public Guid Guid
        {
            get { return this.guid; }
            set { this.guid = value; }
        }
        public string Username
        {
            get { return this.username; }
            set { this.username = value; }
        }
        public string FirstName
        {
            get { return this.firstname; }
            set { this.firstname = value; }    
        }
        public string LastName
        {
            get { return this.lastname; }
            set { this.lastname = value; }
        }
        public string JobFunction
        {
            get { return this.jobFunction; }
            set { this.jobFunction = value; }
        }
        public string Address
        {
            get { return this.address; }
            set { this.address = value; }
        }
        public string City
        {
            get { return this.city; }
            set { this.city = value; }
        }
        public string PostCode
        {
            get { return this.postCode; }
            set { this.postCode = value; }
        }
        public string Country
        {
            get { return this.country; }
            set { this.country = value; }
        }
        public string Telephone
        {
            get { return this.telephone; }
            set { this.telephone = value; }
        }
        public string Email
        {
            get { return this.email; }
            set { this.email = value; }
        }
        public string SalesContact
        {
            get { return this.salesContact; }
            set { this.salesContact = value; }
        }
        public Guid ContractID
        {
            get { return this.contractId; }
            set { this.contractId = value; }
        }
        public string JobTitle
        {
            get { return this.jobTitle; }
            set { this.jobTitle = value; }
        }
        public string State
        {
            get { return this.state; }
            set { this.state = value; }
        }
        public string Fax
        {
            get { return this.fax; }
            set { this.fax = value; }
        }
        public string Title
        {
            get { return this.title; }
            set { this.title = value; }
        }
        public bool ReceiveRegularUpdates
        {
            get { return this.receiveRegularUpdates; }
            set { this.receiveRegularUpdates = value; }
        }
        public bool IsGatekeeper
        {
            get { return this.isGatekeeper; }
            set { this.isGatekeeper = value; }
        }
        public bool IsSharedPassword
        {
            get { return this.isSharedPassword; }
            set { this.isSharedPassword = value; }
        }
        public Password Password
        {
            get { return this.password; }
            set { this.password = value; }
        }
        public bool IsIPRestrict
        {
            get { return this.isIPRestrict; }
            set { this.isIPRestrict = value; }
        }
        public bool IsAccountActive
        {
            get { return this.isAccountActive; }
            set { this.isAccountActive = value; }
        }
        public bool IsChangePasswordNextLogin
        {
            get { return this.isChangePasswordNextLogin; }
            set { this.isChangePasswordNextLogin = value; }
        }
        public decimal PurchasedAmountToDate
        {
            get { return this.purchasedAmountToDate; }
            set { this.purchasedAmountToDate = value; }
        }
        public decimal PurchaseLimit
        {
            get { return this.purchaseLimit; }
            set { this.purchaseLimit = value; }
        }
        public bool IsBillingCodeRequired
        {
            get { return this.isBillingCodeRequired; }
            set { this.isBillingCodeRequired = value; }
        }
        public string PreferredCurrency
        {
            get { return this.preferredCurrency; }
            set { this.preferredCurrency = value; }
        }
        public bool IsNewUser
        {
            get { return this.isNewUser; }
            set { this.isNewUser = value; }
        }
        public bool IsAffiliateUser
        {
            get { return this.isAffiliateUser; }
            set { this.isAffiliateUser = value; }
        }
        public bool IsPressUser
        {
            get { return this.isPressUser; }
            set { this.isPressUser = value; }
        }
        public bool IsMarketingContact
        {
            get { return this.isMarketingContact; }
            set { this.isMarketingContact = value; }
        }
        public bool IsNotifyOfNewProducts
        {
            get { return this.isNotifyOfNewProducts; }
            set { this.isNotifyOfNewProducts = value; }
        }
        public int UserType
        {
            get { return this.userType; }
            set { this.userType = value; }
        }
        public DateTime DateCreated
        {
            get { return this.dateCreated; }
            set { this.dateCreated = value; }
        }
        public bool IsAutoLogin
        {
            get { return this.isAutoLogin; }
            set { this.isAutoLogin = value; }
        }
        public string PreferredEmailFormat
        {
            get { return this.preferredEmailFormat; }
            set { this.preferredEmailFormat = value; }
        }
        public string Interests
        {
            get { return this.interests; }
            set { this.interests = value; }
        }
        public string SectorForURLRedirection
        {
            get { return this.sectorForURLRedirection; }
            set { this.sectorForURLRedirection = value; }
        }
        public bool IsProfileUpdated
        {
            get { return this.isProfileUpdated; }
            set { this.isProfileUpdated = value; }
        }

     
       
        // Lazy Loaded Contract
        public Contract Contract
        {
            get 
            {
                if (this.contract == null)
                {
                    if (this.contractId != null)
                    {
                        this.contract = DataService.GetContract(this.contractId);
                    }
                }
                else
                {
                    if (this.contract.Identifier != this.contractId)
                    {
                        this.contract = DataService.GetContract(this.contractId);
                    }
                }

                return this.contract;
            }
        }

        public override string ToString()
        {
            return this.Username;
        }
        
        public bool Equals(User other)
        {
            return this.Username == other.Username && this.Password == other.Password;
        }

    }
}
