using System;
using System.Collections.Generic;
using System.Text;

namespace Datamonitor.DataModel
{
    public class UserService
    {
        private Guid userId;
        private int serviceId;
        private string interests;

        public Guid UserId
        {
            get { return this.userId; }
            set { this.userId = value; }
        }

        public int ServiceId
        {
            get { return this.serviceId; }
            set { this.serviceId = value; }
        }
        public string Interests
        {
            get { return this.interests; }
            set { this.interests = value; }
        }
    }
}
