using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using Datamonitor.DataModel.Structures;

namespace Datamonitor.DataModel
{
    public class Contract : BaseModel, IEquatable<Contract>
    {
        // Private Fields
        private string name;
        private Guid identifier;
        private string referrer;
        private Guid companyId;
        private string salesContact;
        private string contractUsername;

        private bool isActive;
        private bool isSSOContract;
        private bool isIPRestrict;

        private Company company;
        private DateRange dateRange;
        private IPAddressRange[] ipRanges;
        private ContractService[] services;

        // Constructor
        public Contract() { }

        // Public Properties
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public Guid Identifier
        {
            get { return this.identifier; }
            set { this.identifier = value; }
        }
        public string Referrer
        {
            get { return this.referrer; }
            set { this.referrer = value; }
        }
        public Guid CompanyID
        {
            get { return this.companyId; }
            set { this.companyId = value; }
        }
        public string SalesContact
        {
            get { return this.salesContact; }
            set { this.salesContact = value; }
        }
        public string ContractUsername
        {
            get { return this.contractUsername; }
            set { this.contractUsername = value; }
        }
        public bool IsActive
        {
            get { return isActive; }
            set { this.isActive = value; }
        }
        public bool IsSSOContract
        {
            get { return isSSOContract; }
            set { this.isSSOContract = value; }
        }
        public bool IsIPRestrict
        {
            get { return isIPRestrict; }
            set { this.isIPRestrict = value; }
        }
        public DateRange DateRange
        {
            get { return this.dateRange; }
            set { this.dateRange = value; }
        }

        // Lazy Loaded Company
        public Company Company
        {
            get
            {
                if (this.company == null)
                {
                    if (this.companyId != null)
                    {
                        this.company = this.DataService.GetCompany(this.companyId);
                    }
                }
                else
                {
                    if (this.company.Identifier != this.companyId)
                    {
                        this.company = this.DataService.GetCompany(this.companyId);
                    }
                }

                return this.company;
            }
            
        }

        // Lazy Loaded IPRanges
        public IPAddressRange[] IPRanges
        {
            get 
            {
                if (this.ipRanges == null)
                {
                    if (this.identifier != null)
                    {
                        this.ipRanges = this.DataService.GetIPRanges(this.identifier);
                    }
                }

                return this.ipRanges;
            }
        }

        // Lazy Loaded Contract Services
        public ContractService[] Services
        {
            get
            {
                if (this.services == null)
                {
                    if (this.identifier != null)
                    {
                        this.services = this.DataService.GetContractServices(this.identifier);
                    }
                }

                return this.services;
            }
        }        
      
        public override string ToString()
        {
            return this.Name;
        }

        public bool Equals(Contract other)
        {
            return this.Identifier == other.Identifier;
        }

    }
}
