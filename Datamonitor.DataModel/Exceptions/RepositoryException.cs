using System;
using System.Collections.Generic;
using System.Text;

namespace Datamonitor.DataModel.Exceptions
{
    public enum RepositoryStateActionType
    {
        Add = 0,
        Get = 1,
        Update = 2,
        Delete = 3
    }

    public class RepositoryException : Exception
    {
        private const string MESSAGE = "Unable to {0} {1}.";

        public RepositoryException(RepositoryStateActionType action, object obj)
            : base(String.Format(MESSAGE, obj.ToString(), action.ToString()))
        {
            
        }
    }
}
