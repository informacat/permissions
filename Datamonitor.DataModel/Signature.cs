using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Xml;

namespace Datamonitor.DataModel
{
    public class Signature 
    {
        private Guid _identifier;
        private string _userName;
        private DateTime _dateCreated;
        private int _timeoutDays;
        private bool _isActive;

        #region Constructors

        public Signature()
        {
        }

        public Signature(Guid identifier)
        {
            this._identifier = identifier;
        }

        #endregion

        #region Properties

        public Guid Identifier
        {
            get { return _identifier; }
            set { _identifier = value; }
        }

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }
        public int TimeOutDays
        {
            get { return _timeoutDays; }
            set { _timeoutDays = value; }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        #endregion

        #region Public Methdods

        public bool IsCorrectGuid(Guid identifier)
        {
            if (this._identifier == identifier)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

    }
}
