using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel.Structures;
using System.Collections.Specialized;

namespace Datamonitor.DataModel.Interfaces
{
    public interface IDataService
    {
        // Gets
        bool CheckUserExists(string username);
        Session GetSession(Guid authenticationToken);
        User GetUser(string username);
        User GetUserByUserGUID(Guid userGUID);
        User[] GetGatekeepers(Guid contractId);
        User[] GetUsers(Guid contractId);
        List<User> GetSharedUsers(Guid contractId);
        Contract GetContract(Guid contractId);
        Contract GetContract(string contractUsername);
        Contract GetContract(long ipAddress);
        Contract[] GetContracts(long ipAddress);
        Contract[] GetContractsForHCServices(long ipAddress);
        Contract[] GetContractsForVerdictServices(long ipAddress);
        Contract GetContract(Guid companyIdentifier, string contractName);
        Contract CreateContract(
               Guid contractIdentifier,
               string contractName,
               bool contractStatus,
               DateTime contractStartDate,
               DateTime contractEndDate,
               Guid companyID,
               bool isSSOContract,
               bool contractIPRestrict,
               string contractSalesContact,
               string username,
               string serviceIDs,
               bool contractServiceStatus);
        Company GetCompany(Guid companyId);
        Company GetCompany(string companyName);
        Company CreateCompany(
            Guid companyId,
            string name,
            string tradingPartnerNumber,
            string adminContact,
            string receiver,
            string catalogSet,
            string changedBy,
            string purchasingManager,
            DateTime lastChanged,
            DateTime created,
            string type,
            string status,
            string address,
            string city,
            string state,
            string country,
            string postCode);
        IPAddressRange[] GetIPRanges(Guid contractId);
        ContractService[] GetContractServices(Guid contractId);
        void SetContractServices(Guid contractId, int[] serviceIds, bool status, DateTime start, DateTime end);
        ContractService[] GetActiveContractServices(Guid contractId);
        bool IsUserSignInLocked(User user, int maxLoginAttempts, int signInResetMinutes);
        Signature GetSignature(Guid signatureId);
        User[] GetActiveUsersByEmail(string email);
        User[] GetActiveUsersByEmailAndService(string email, int[] serviceIds);
        string GetSessionKeyValue(Session session, string keyName);
        bool InsertSessionKeyValue(Session session, string keyName, string keyValue);
        ContractService[] GetContractServicesByIPAddress(long ipAddress);
        ContractService[] GetActiveContractServicesByIPAddress(long ipAddress);
        ContractService[] GetContractServicesByUsername(string username);
        ContractService[] GetActiveContractServicesByUsername(string username);
        ContractService[] GetActiveContractServicesByUserGUID(Guid userGUID);
        Contract[] GetContractByUsername(string username);
        string GetUserInterests(Guid userId, int serviceId);
        // Sets
        User PersistUser(User user);
        User PersistUserAuthentication(User user);
        Session PersistSession(Session session);
        /// <summary>
        /// Updates the user's sign in credentials for last login and login attempts
        /// </summary>
        /// <param name="username">The current username</param>
        /// <param name="p">Is the sign in successfull</param>     
        void UpdateSignInCredentials(string username, bool p);
        Signature CreateSignature(Signature signature);
        Signature UpdateSignature(Signature signature);
        void SetUserInterests(Guid  userId, int serviceId, string interests);


        // Updates
        bool DeactivateUser(string username, Guid contractId);

        // Delete
        void KillSession(Session session);
        void KillAllUserSessions(User user);
        
    }
}
