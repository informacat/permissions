using System;
using System.Collections.Generic;
using System.Text;

namespace Datamonitor.DataModel.Interfaces
{
    public interface IModel
    {
        IDataService DataService { get; }
    }
}
