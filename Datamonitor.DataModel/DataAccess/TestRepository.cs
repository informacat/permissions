using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.Structures;

namespace Datamonitor.DataModel.DataAccess
{
    public class TestRepository : IRepository
    {
        public Session GetSession(Guid authenticationToken)
        {
            Company company = new Company("Company1", "Company1", true);

            IPAddressRange ipRange = new IPAddressRange();
            ipRange.LowerBound = 100000000001;
            ipRange.UpperBound = 100000000100;

            DateRange dateRange = new DateRange();
            dateRange.Start = DateTime.Parse("2007-01-01");
            dateRange.End = DateTime.Parse("2011-02-02");

            ContractService contractService = new ContractService("Service1", "Service1", dateRange);

            Contract contract = new Contract("Contract1", "Contract1", true, new Uri("www.google.com"), company, new IPAddressRange[1] { ipRange }, new ContractService[1] { contractService }, dateRange);

            User user = new User("User1", "Password1", contract);

            Session session = new Session();
            session.CreateDate = DateTime.Now;
            session.TimeoutMinutes = 60000;
            session.User = user;

            return session;
        }

        public User GetUser(string username)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
