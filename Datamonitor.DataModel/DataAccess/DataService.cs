using System;
using System.Collections.Generic;
using System.Text;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.Providers;
using System.Collections.Specialized;

namespace Datamonitor.DataModel.DataAccess
{
    public class DataService : IDataService
    {
        private IRepository repository;

        public DataService()
            : this(RepositoryProvider.GetRepository()) { }

        public DataService(IRepository repository)
        {
            this.repository = repository;
        }

        // Get
        public bool CheckUserExists(string username)
        {
            return repository.CheckUserExists(username);
        }
        public Session GetSession(Guid authenticationToken)
        {
            return repository.GetSession(authenticationToken);
        }

        public User GetUser(string username)
        {
            return repository.GetUser(username);
        }

        public User GetUserByUserGUID(Guid userGUID)
        {
            return repository.GetUserByUserGUID(userGUID);
        }

        public User[] GetGatekeepers(Guid contractId)
        {
            return repository.GetGatekeepers(contractId);
        }

        public User[] GetUsers(Guid contractId)
        {
            return repository.GetUsers(contractId);
        }

        public User[] GetActiveUsersByEmail(string email)
        {
            return repository.GetActiveUsersByEmail(email);
        }

        public User[] GetActiveUsersByEmailAndService(string email, int[] serviceIds)
        {
            return repository.GetActiveUsersByEmailAndService(email, serviceIds);
        }

        public string GetSessionKeyValue(Session session, string keyName)
        {
            return repository.GetSessionKeyValue(session, keyName);
        }

        public bool InsertSessionKeyValue(Session session, string keyName, string keyValue)
        {
            return repository.InsertSessionKeyValue(session, keyName, keyValue);
        }

        public List<User> GetSharedUsers(Guid contractId)
        {
            return repository.GetSharedUsers(contractId);
        }

        public Contract GetContract(Guid contractId)
        {
            return repository.GetContract(contractId);
        }

        public Contract GetContract(string username)
        {
            return repository.GetContract(username);
        }

        public Contract GetContract(long ipAddress)
        {
            return repository.GetContract(ipAddress);
        }

        public Contract[] GetContracts(long ipAddress)
        {
            return repository.GetContracts(ipAddress);
        }

        public Contract[] GetContractsForHCServices(long ipAddress)
        {
            return repository.GetContractsForHCServices(ipAddress);
        }

        public Contract[] GetContractsForVerdictServices(long ipAddress)
        {
            return repository.GetContractsForVerdictServices(ipAddress);
        }

        public Contract[] GetContractByUsername(string username)
        {
            return repository.GetContractByUsername(username);
        }

        public Contract GetContract(Guid companyIdentifier, string contractName)
        {
            return repository.GetContract(companyIdentifier, contractName);
        }

        public Company GetCompany(Guid companyIdentifier)
        {
            return repository.GetCompany(companyIdentifier);
        }

        public Company GetCompany(string companyName)
        {
            return repository.GetCompany(companyName);
        }

        public Company CreateCompany(
            Guid companyId,
            string name,
            string tradingPartnerNumber,
            string adminContact,
            string receiver,
            string catalogSet,
            string changedBy,
            string purchasingManager,
            DateTime lastChanged,
            DateTime created,
            string type,
            string status,
            string address,
            string city,
            string state,
            string country,
            string postCode)
        {
            return repository.CreateCompany(companyId, name, tradingPartnerNumber,
                adminContact, receiver, catalogSet, changedBy, purchasingManager, lastChanged, created, type, status, address, city, state, country, postCode);
        }

        public Datamonitor.DataModel.Structures.IPAddressRange[] GetIPRanges(Guid contractId)
        {
            return repository.GetIPRanges(contractId);
        }

        public ContractService[] GetContractServices(Guid contractId)
        {
            return repository.GetContractServices(contractId);
        }

        public ContractService[] GetContractServicesByIPAddress(long ipAddress)
        {
            return repository.GetContractServicesByIPAddress(ipAddress);
        }

        public ContractService[] GetActiveContractServicesByIPAddress(long ipAddress)
        {
            return repository.GetActiveContractServicesByIPAddress(ipAddress);
        }

        public ContractService[] GetContractServicesByUsername(string username)
        {
            return repository.GetContractServicesByUsername(username);
        }

        public ContractService[] GetActiveContractServicesByUsername(string username)
        {
            return repository.GetActiveContractServicesByUsername(username);
        }

        public ContractService[] GetActiveContractServicesByUserGUID(Guid userGUID)
        {
            return repository.GetActiveContractServicesByUserGUID(userGUID);
        }

        public void SetContractServices(Guid contractId, int[] serviceIds, bool status, DateTime start, DateTime end)
        {
            repository.SetContractServices(contractId, serviceIds, status, start, end);
        }

        public ContractService[] GetActiveContractServices(Guid contractId)
        {
            List<ContractService> activeContractServices = new List<ContractService>();

            ContractService[] contractServices = repository.GetContractServices(contractId);

            foreach (ContractService service in contractServices)
            {
                if (service.IsServiceActive && service.IsActive && service.DateRange.IsBetween(DateTime.Now))
                {
                    activeContractServices.Add(service);
                }
            }

            return activeContractServices.ToArray();
        }
        public Signature GetSignature(Guid signatureId)
        {
            return repository.GetSignature(signatureId);
        }

        /// <summary>
        /// Determines if a user's credentials are temporarily locked
        /// </summary>
        /// <param name="user">The user</param>
        /// <param name="maxLoginAttempts">The applications maximum login attempts</param>
        /// <param name="signInResetMinutes">The number of minutes before the account 
        /// can be signed into again once the maximum number of attempts is reached</param>
        /// <returns>true id their credentials are locked</returns>
        /// <remarks>NOTE: DateTime.Now is used here when, in other places, GetDate() from 
        /// the database is prodominantly used. However, here, it saves another database call.
        /// To implement GetDate() and DateTime property of when the object was loaded into
        /// memory would probably have to be used.</remarks>
        public bool IsUserSignInLocked(User user, int maxLoginAttempts, int signInResetMinutes)
        {
            if (user != null && user.Password != null)
            {
                bool isMaxAttemptsReached = user.Password.SignInAttempts >= maxLoginAttempts;

                if (isMaxAttemptsReached)
                {
                    return DateTime.Now < user.Password.LastSignInAttempt.AddMinutes(signInResetMinutes);
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        public string GetUserInterests(Guid userId, int serviceId)
        {
            return repository.GetUserInterests(userId, serviceId);
        }
        /// <summary>
        /// Updates the user's sign in credentials for last login and login attempts
        /// </summary>
        /// <param name="user">The current user</param>
        /// <param name="p">Is the sign in successfull</param>        
        public void UpdateSignInCredentials(string username, bool p)
        {
            repository.UpdateSignInCredentials(username, p);
        }

        // Set
        public User PersistUser(User user)
        {
            return repository.PersistUser(user);
        }

        public User PersistUserAuthentication(User user)
        {
            return repository.PersistUserAuthentication(user);
        }

        public Session PersistSession(Session session)
        {
            return repository.PersistSession(session);
        }

        public Signature CreateSignature(Signature signature)
        {
            return repository.CreateSignature(signature);
        }

        public Signature UpdateSignature(Signature signature)
        {
            return repository.UpdateSignature(signature);
        }

        public void SetUserInterests(Guid userId, int serviceId, string interests)
        {
            repository.SetUserInterests(userId, serviceId, interests);
        }

        // Updates
        public bool DeactivateUser(string username, Guid contractId)
        {
            return repository.DeactivateUser(username, contractId);
        }

        // Delete
        public void KillSession(Session session)
        {
            repository.DeleteSession(session);
        }

        public void KillAllUserSessions(User user)
        {
            repository.DeleteSessions(user);
        }

        public Contract CreateContract(Guid contractIdentifier, string contractName, bool contractStatus, DateTime contractStartDate, DateTime contractEndDate, Guid companyIdentifier, bool isSSOContract, bool contractIPRestrict, string contractSalesContact, string username, string serviceIDs, bool contractServiceStatus)
        {
            return repository.CreateContract(contractIdentifier, contractName, contractStatus, contractStartDate, contractEndDate, companyIdentifier, isSSOContract, contractIPRestrict, contractSalesContact, username, serviceIDs, contractServiceStatus);
        }
    }
}
