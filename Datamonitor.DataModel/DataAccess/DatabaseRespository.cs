using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.Utilities;
using Datamonitor.DataModel.Exceptions;
using Datamonitor.DataModel.Structures;
using System.Collections.Specialized;

namespace Datamonitor.DataModel.DataAccess
{
    public class DatabaseRepository : IRepository
    {
        private string connectionString;
        
        public DatabaseRepository()
        {
            this.connectionString = ModelConfiguration.ConnectionString;
        }

        public DatabaseRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        // Gets
        public bool CheckUserExists(string username)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetUserAuthentication", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter usernameParam = new SqlParameter("@username", SqlDbType.NVarChar);
                usernameParam.Value = username;
                usernameParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(usernameParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable userTable = new DataTable();

                adapter.Fill(userTable);

                if (userTable.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public User GetUser(string username)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetUserAuthentication", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter usernameParam = new SqlParameter("@username", SqlDbType.NVarChar);
                usernameParam.Value = username;
                usernameParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(usernameParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable userTable = new DataTable();

                adapter.Fill(userTable);

                if (userTable.Rows.Count > 0)
                {
                    DataRow row = userTable.Rows[0];

                    return PopulateUser(row);
                }
                else
                {
                    return null;
                }
            }
        }

        public User GetUserByUserGUID(Guid userGUID)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetUserAuthenticationByUserGUID", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter userGUIDParam = new SqlParameter("@userGUID", SqlDbType.UniqueIdentifier);
                userGUIDParam.Value = userGUID;
                userGUIDParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(userGUIDParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable userTable = new DataTable();

                adapter.Fill(userTable);

                if (userTable.Rows.Count > 0)
                {
                    DataRow row = userTable.Rows[0];

                    return PopulateUser(row);
                }
                else
                {
                    return null;
                }
            }
        }

        private static User PopulateUser(DataRow row)
        {
            string Guid = row["g_user_id"].ToString();
            string Username = row["u_logon_name"].ToString();
            string FirstName = row["u_first_name"].ToString();
            string LastName = row["u_last_name"].ToString();
            string JobFunction = row["jobFunction"].ToString();
            string Address = row["address"].ToString();
            string City = row["city"].ToString();
            string PostCode = row["postCode"].ToString();
            string Country = row["country"].ToString();
            string Telephone = row["u_tel_number"].ToString();
            string Email = row["u_email_address"].ToString();
            string ReceiveRegularUpdates = row["receiveRegularUpdates"].ToString();
            string IsGateKeeper = row["gatekeeperUser"].ToString();
            string IsSharedPassword = row["sharedPassword"].ToString();
            Guid ContractID = new Guid(row["Contract_ID"].ToString());
            string IsIPRestrict = row["ipRestrict"].ToString();
            string IsAccountActive = row["i_account_status"].ToString();
            string IsChangePasswordNextLogin = row["changePasswordNextLogin"].ToString();
            string PurchasedAmountToDate = row["purchasedAmountToDate"].ToString();
            string PurchaseLimit = row["purchaseLimit"].ToString();
            string IsBillingCodeRequired = row["billingCodeRequired"].ToString();
            string PreferredCurrency = row["preferrredCurrency"].ToString();
            string IsNewUser = row["newUser"].ToString();
            string IsAffiliateUser = row["affiliateUser"].ToString();
            string IsPressUser = row["pressUser"].ToString();
            string IsMarketingContact = row["marketingContact"].ToString();
            string IsNotifyOfNewProducts = row["notifyOfNewProducts"].ToString();
            string UserType = row["i_user_type"].ToString();
            string DateCreated = row["d_date_created"].ToString();
            string IsAutoLogin = row["autoLogin"].ToString();
            string PreferredEmailFormat = row["preferredEmailFormat"].ToString();
            string Interests = row["interest"].ToString();
            string SectorForURLRedirection = row["sectorForURLRedirection"].ToString();
            string SalesContact = row["salesContact"].ToString();
            string JobTitle = row["jobTitle"].ToString();
            string State = row["state"].ToString();
            string Fax = row["u_fax_number"].ToString();
            string Title = row["u_user_title"].ToString();
            string updateProfile = row["portfolio1"].ToString();
            int IsProfileUpdated;
            int.TryParse(updateProfile, out IsProfileUpdated);
            // The following fields are depending on authentication
            string PasswordHash = row.Table.Columns.Contains("PasswordHash") ? row["PasswordHash"].ToString() : string.Empty;
            string PasswordSalt = row.Table.Columns.Contains("PasswordSalt") ? row["PasswordSalt"].ToString() : string.Empty;
            string PasswordTimeStamp = row.Table.Columns.Contains("PasswordTimeStamp") ? row["PasswordTimeStamp"].ToString() : string.Empty;
            string SignInAttempts = row.Table.Columns.Contains("SignInAttempts") ? row["SignInAttempts"].ToString() : string.Empty;
            string LastSignInAttempt = row.Table.Columns.Contains("LastSignInAttempt") ? row["LastSignInAttempt"].ToString() : string.Empty;
            
            // Create user and populate fields
            User user = new User();
            user.Guid = new Guid(Guid);
            user.Username = Username;
            user.FirstName = FirstName;
            user.LastName = LastName;
            user.JobFunction = JobFunction;
            user.Address = Address;
            user.City = City;
            user.PostCode = PostCode;
            user.Country = Country;
            user.Telephone = Telephone;
            user.Email = Email;
            user.ContractID = ContractID;
            user.PreferredCurrency = PreferredCurrency;
            user.PreferredEmailFormat = PreferredEmailFormat;
            user.Interests = Interests;
            user.SectorForURLRedirection = SectorForURLRedirection;
            user.SalesContact = SalesContact;
            user.JobTitle = JobTitle;
            user.State = State;
            user.Fax = Fax;
            user.Title = Title;
            user.IsProfileUpdated = Convert.ToBoolean( IsProfileUpdated);


            // Password
            if (!string.IsNullOrEmpty(PasswordHash))
            {
                Password password = new Password(PasswordHash, PasswordSalt);
                user.Password = password;

                int outSignInAttempts;
                int.TryParse(SignInAttempts, out outSignInAttempts);
                password.SignInAttempts = outSignInAttempts;

                DateTime outLastLoginDate;
                DateTime.TryParse(LastSignInAttempt, out outLastLoginDate);
                password.LastSignInAttempt = outLastLoginDate;
            }

            int outUserType;
            int.TryParse(UserType, out outUserType);
            user.UserType = outUserType;

            // Convert DB smallint's to bool's

            // Is Gate Keeper User
            int outIsGateKeeper;
            int.TryParse(IsGateKeeper, out outIsGateKeeper);
            user.IsGatekeeper = Convert.ToBoolean(outIsGateKeeper);

            // Receives Regular Updates
            int outReceiveRegularUpdates;
            int.TryParse(ReceiveRegularUpdates, out outReceiveRegularUpdates);
            user.ReceiveRegularUpdates = Convert.ToBoolean(outReceiveRegularUpdates);

            int outIsAccountActive;
            int.TryParse(IsAccountActive, out outIsAccountActive);
            user.IsAccountActive = Convert.ToBoolean(outIsAccountActive);

            int outIsChangePasswordNextLogin;
            int.TryParse(IsChangePasswordNextLogin, out outIsChangePasswordNextLogin);
            user.IsChangePasswordNextLogin = Convert.ToBoolean(outIsChangePasswordNextLogin);

            int outIsBillingCodeRequired;
            int.TryParse(IsBillingCodeRequired, out outIsBillingCodeRequired);
            user.IsBillingCodeRequired = Convert.ToBoolean(outIsBillingCodeRequired);

            int outIsNewUser;
            int.TryParse(IsNewUser, out outIsNewUser);
            user.IsNewUser = Convert.ToBoolean(outIsNewUser);

            int outIsAffiliateUser;
            int.TryParse(IsAffiliateUser, out outIsAffiliateUser);
            user.IsAffiliateUser = Convert.ToBoolean(outIsAffiliateUser);

            int outIsPressUser;
            int.TryParse(IsPressUser, out outIsPressUser);
            user.IsPressUser = Convert.ToBoolean(outIsPressUser);

            int outIsMarketingContact;
            int.TryParse(IsMarketingContact, out outIsMarketingContact);
            user.IsMarketingContact = Convert.ToBoolean(outIsMarketingContact);

            int outIsNotifyOfNewProducts;
            int.TryParse(IsNotifyOfNewProducts, out outIsNotifyOfNewProducts);
            user.IsNotifyOfNewProducts = Convert.ToBoolean(outIsNotifyOfNewProducts);

            int outIsAutoLogin;
            int.TryParse(IsAutoLogin, out outIsAutoLogin);
            user.IsAutoLogin = Convert.ToBoolean(outIsAutoLogin);

            // Convert DB bit's to bool's

            // Is Shared Password
            bool outIsSharedPassword;
            bool.TryParse(IsSharedPassword, out outIsSharedPassword);
            user.IsSharedPassword = outIsSharedPassword;

            bool outIsIPRestrict;
            bool.TryParse(IsIPRestrict, out outIsIPRestrict);
            user.IsIPRestrict = outIsIPRestrict;

            // Decimal Conversion's

            decimal outPurchasedAmountToDate;
            decimal.TryParse(PurchasedAmountToDate, out outPurchasedAmountToDate);
            user.PurchasedAmountToDate = outPurchasedAmountToDate;

            decimal outPurchaseLimit;
            decimal.TryParse(PurchaseLimit, out outPurchaseLimit);
            user.PurchaseLimit = outPurchaseLimit;

            // Convert DateTime's

            DateTime outDateCreated;
            DateTime.TryParse(DateCreated, out outDateCreated);
            user.DateCreated = outDateCreated;

            return user;
        }

        public User[] GetGatekeepers(Guid contractId)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetGatekeepersByContract", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter usernameParam = new SqlParameter("@contractId", SqlDbType.UniqueIdentifier);
                usernameParam.Value = contractId;
                usernameParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(usernameParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable userTable = new DataTable();

                adapter.Fill(userTable);

                User[] users = new User[userTable.Rows.Count];
                int index = 0;

                foreach (DataRow row in userTable.Rows)
                {
                    users[index] = PopulateUser(row);
                    index++;
                }

                return users;
            }
        }

        public User[] GetUsers(Guid contractId)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetActiveUsersByContract", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter usernameParam = new SqlParameter("@contractId", SqlDbType.UniqueIdentifier);
                usernameParam.Value = contractId;
                usernameParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(usernameParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable userTable = new DataTable();

                adapter.Fill(userTable);

                User[] users = new User[userTable.Rows.Count];
                int index = 0;

                foreach (DataRow row in userTable.Rows)
                {
                    users[index] = PopulateUser(row);
                    index++;
                }

                return users;
            }
        }

        public User[] GetActiveUsersByEmail(string email)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetActiveUsersByEmail", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter emailParam = new SqlParameter("@email", SqlDbType.VarChar);
                emailParam.Value = email;
                emailParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(emailParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable userTable = new DataTable();

                adapter.Fill(userTable);

                User[] users = new User[userTable.Rows.Count];
                int index = 0;

                foreach (DataRow row in userTable.Rows)
                {
                    users[index] = PopulateUser(row);
                    index++;
                }

                return users;
            }
        }

        public User[] GetActiveUsersByEmailAndService(string email, int[] serviceIds)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetActiveUsersByEmailAndService", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter emailParam = new SqlParameter("@email", SqlDbType.VarChar);
                emailParam.Value = email;
                emailParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(emailParam);

                // turn the integer array of serviceIds into a string array
                List<int> serviceIdList = new List<int>(serviceIds);
                List<string> serviceIdToPass = serviceIdList.ConvertAll<string>(delegate(int i) { return i.ToString(); });
                SqlParameter serviceIdParam = new SqlParameter("@serviceIds", SqlDbType.VarChar);
                // join the individual serviceId strings with a "," to pass to the SP
                serviceIdParam.Value = string.Join(",",serviceIdToPass.ToArray());
                serviceIdParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(serviceIdParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable userTable = new DataTable();

                adapter.Fill(userTable);

                User[] users = new User[userTable.Rows.Count];
                int index = 0;

                foreach (DataRow row in userTable.Rows)
                {
                    users[index] = PopulateUser(row);
                    index++;
                }

                return users;
            }
        }

        public string GetSessionKeyValue(Session session, string keyName)
        {
            string sessionKeyValue = string.Empty;

            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetSessionKeyValue", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter sessionGuidParam = new SqlParameter("@sessionId", SqlDbType.UniqueIdentifier);
                sessionGuidParam.Value = session.Identifier;
                sessionGuidParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(sessionGuidParam);

                SqlParameter sessionKeyName = new SqlParameter("@key", SqlDbType.VarChar);
                sessionKeyName.Value = keyName;
                sessionKeyName.Direction = ParameterDirection.Input;

                command.Parameters.Add(sessionKeyName);
                
                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable dtSessionKeyValue = new DataTable();

                try
                {
                    adapter.Fill(dtSessionKeyValue);
                    foreach (DataRow row in dtSessionKeyValue.Rows)
                    {
                        sessionKeyValue = row["value"].ToString();
                    }
                }
                catch (SqlException sqlEx)
                {
                    throw new Exception("Key|Value pair DB retrieval failed", sqlEx.InnerException);
                }
                
                connection.Close();
            }

            return sessionKeyValue;
        }

        public bool InsertSessionKeyValue(Session session, string keyName, string keyValue)
        {
            bool insertSuccess = false;

            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("dbo.pwsInsertSessionKeyValue", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter sessionGuidParam = new SqlParameter("@sessionId", SqlDbType.UniqueIdentifier);
                sessionGuidParam.Value = session.Identifier;
                sessionGuidParam.Direction = ParameterDirection.Input;
                command.Parameters.Add(sessionGuidParam);

                SqlParameter sessionKeyName = new SqlParameter("@key", SqlDbType.VarChar);
                sessionKeyName.Value = keyName;
                sessionKeyName.Direction = ParameterDirection.Input;
                command.Parameters.Add(sessionKeyName);

                SqlParameter sessionKeyValue = new SqlParameter("@value", SqlDbType.VarChar);
                sessionKeyValue.Value = keyValue;
                sessionKeyValue.Direction = ParameterDirection.Input;
                command.Parameters.Add(sessionKeyValue);

                try
                {
                    insertSuccess = (command.ExecuteNonQuery() == 1);
                }
                catch (SqlException sqlEx)
                {
                    throw new Exception("Insert|Update of key|value failed", sqlEx.InnerException);
                }

                connection.Close();
            }

            return insertSuccess;
        }
        
        public List<User> GetSharedUsers(Guid contractId)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetActiveSharedUsersByContract", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter contractIdParam = command.Parameters.Add("@contractId", SqlDbType.UniqueIdentifier);
                contractIdParam.Direction = ParameterDirection.Input;
                contractIdParam.Value = contractId;

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable userTable = new DataTable();
                adapter.Fill(userTable);

                List<User> users = new List<User>(userTable.Rows.Count);

                foreach (DataRow row in userTable.Rows)
                {
                    users.Add(PopulateUser(row));
                }

                return users;
            }
        }

        public Session GetSession(Guid authenticationToken)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetSession", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter sessionIdParam = new SqlParameter("@sessionId", SqlDbType.UniqueIdentifier);
                sessionIdParam.Value = authenticationToken;
                sessionIdParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(sessionIdParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable sessionTable = new DataTable();

                adapter.Fill(sessionTable);

                if (sessionTable.Rows.Count > 0)
                {
                    DataRow row = sessionTable.Rows[0];

                    string SessionID = row["sessionId"].ToString();
                    string Username = row["u_logon_name"].ToString();
                    string CreateDate = row["CreateDate"].ToString();
                    string ExpiryDate = row["ExpiryDate"].ToString();
                    bool IsExpired = Convert.ToBoolean(row["IsExpired"]);

                    Session session = new Session();
                    session.Identifier = new Guid(SessionID);
                    session.User = GetUser(Username);

                    DateTime outExpiryDate;
                    DateTime.TryParse(ExpiryDate, out outExpiryDate);
                    session.ExpiryDate = outExpiryDate;

                    DateTime outCreateDate;
                    DateTime.TryParse(CreateDate, out outCreateDate);
                    session.CreateDate = outCreateDate;

                    session.IsExpired = IsExpired;

                    return session;
                }
                else
                {
                    throw new Exception("No Such Session");
                }
            }
        }

        public Contract CreateContract(
                Guid contractIdentifier,
                string contractName,                
                bool contractStatus,
                DateTime contractStartDate,
                DateTime contractEndDate,
                Guid companyIdentifier,
                bool isSSOContract,
                bool contractIPRestrict,
                string contractSalesContact,
                string username,
                string serviceIDs,
                bool contractServiceStatus)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand("dbo.pwsInsertContractAndServices", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    List<SqlParameter> parameters = new List<SqlParameter>();
                    parameters.Add(new SqlParameter("@contractIdentifier", contractIdentifier));
                    parameters.Add(new SqlParameter("@name", contractName));
                    parameters.Add(new SqlParameter("@status", contractStatus ? "Active" : "Inactive"));
                    parameters.Add(new SqlParameter("@startDate", contractStartDate));
                    parameters.Add(new SqlParameter("@endDate", contractEndDate));
                    parameters.Add(new SqlParameter("@companyIdentifier", companyIdentifier));
                    parameters.Add(new SqlParameter("@isSSOContract", isSSOContract));
                    parameters.Add(new SqlParameter("@ipRestrict", contractIPRestrict));
                    parameters.Add(new SqlParameter("@salesContact", contractSalesContact));
                    parameters.Add(new SqlParameter("@contractUsername", username));
                    parameters.Add(new SqlParameter("@serviceIDs", serviceIDs));
                    parameters.Add(new SqlParameter("@contractServiceStatus", contractServiceStatus));
                    command.Parameters.AddRange(parameters.ToArray());

                    command.ExecuteNonQuery();
                }

                return GetContract(contractIdentifier);
            }
        }

        public Contract GetContract(Guid contractIdentifier)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetContract", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter contractIdParam = new SqlParameter("@contractIdentifier", SqlDbType.UniqueIdentifier);
                contractIdParam.Direction = ParameterDirection.Input;
                contractIdParam.Value = contractIdentifier;

                command.Parameters.Add(contractIdParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable contractTable = new DataTable();

                adapter.Fill(contractTable);

                if (contractTable.Rows.Count > 0)
                {
                    DataRow row = contractTable.Rows[0];

                    Contract contract = ExtractContract(row);

                    return contract;
                }
                else
                {
                    throw new Exception("No Such Contract");
                }
            }
        }

        public Contract GetContract(string username)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetContractByContractUsername", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter contractIdParam = new SqlParameter("@contractUsername", SqlDbType.NVarChar);
                contractIdParam.Direction = ParameterDirection.Input;
                contractIdParam.Value = username;

                command.Parameters.Add(contractIdParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable contractTable = new DataTable();

                adapter.Fill(contractTable);

                if (contractTable.Rows.Count > 0)
                {
                    DataRow row = contractTable.Rows[0];

                    Contract contract = ExtractContract(row);

                    return contract;
                }
                else
                {
                    throw new Exception("No Such Contract");
                }
            }
        }

        public Contract GetContract(long ipAddress)
        {
            Contract[] contracts = GetContracts(ipAddress);
            if (contracts.Length > 0)
            {
                return contracts[0];
            }
            else
            {
                throw new Exception("No Such Contract");
            }
        }

        public Contract[] GetContracts(long ipAddress)
        {
            List<Contract> contracts = new List<Contract>();

            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetContractsByIPAddress", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter contractIdParam = new SqlParameter("@ipAddress", SqlDbType.BigInt);
                contractIdParam.Direction = ParameterDirection.Input;
                contractIdParam.Value = ipAddress;

                command.Parameters.Add(contractIdParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable contractTable = new DataTable();

                adapter.Fill(contractTable);

                foreach (DataRow row in contractTable.Rows)
                {
                    Contract contract = ExtractContract(row);
                    contracts.Add(contract);
                }
            }

            return contracts.ToArray();
        }

        public Contract[] GetContractsForHCServices(long ipAddress)
        {
            List<Contract> contracts = new List<Contract>();

            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetContractsByIPAddressForHCServices", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter contractIdParam = new SqlParameter("@ipAddress", SqlDbType.BigInt);
                contractIdParam.Direction = ParameterDirection.Input;
                contractIdParam.Value = ipAddress;

                command.Parameters.Add(contractIdParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable contractTable = new DataTable();

                adapter.Fill(contractTable);

                foreach (DataRow row in contractTable.Rows)
                {
                    Contract contract = ExtractContract(row);
                    contracts.Add(contract);
                }
            }

            return contracts.ToArray();
        }

        public Contract[] GetContractsForVerdictServices(long ipAddress)
        {
            List<Contract> contracts = new List<Contract>();

            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetContractsByIPAddressForVerdictServices", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter contractIdParam = new SqlParameter("@ipAddress", SqlDbType.BigInt);
                contractIdParam.Direction = ParameterDirection.Input;
                contractIdParam.Value = ipAddress;

                command.Parameters.Add(contractIdParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable contractTable = new DataTable();

                adapter.Fill(contractTable);

                foreach (DataRow row in contractTable.Rows)
                {
                    Contract contract = ExtractContract(row);
                    contracts.Add(contract);
                }
            }

            return contracts.ToArray();
        }

        public Contract[] GetContractByUsername(string username)
        {
            List<Contract> contracts = new List<Contract>();

            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetContractByUsername", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter usernameParam = new SqlParameter("@username", SqlDbType.NVarChar);
                usernameParam.Direction = ParameterDirection.Input;
                usernameParam.Value = username;

                command.Parameters.Add(usernameParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable contractTable = new DataTable();

                adapter.Fill(contractTable);

                foreach (DataRow row in contractTable.Rows)
                {
                    Contract contract = ExtractContract(row);
                    contracts.Add(contract);
                }
            }

            return contracts.ToArray();
        }

        public Contract GetContract(Guid companyIdentifier, string contractName)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetContractByCompanyIdentifierAndName", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter companyNameParam = new SqlParameter("@companyIdentifier", SqlDbType.UniqueIdentifier);
                companyNameParam.Direction = ParameterDirection.Input;
                companyNameParam.Value = companyIdentifier;

                SqlParameter nameParam = new SqlParameter("@contractName", SqlDbType.NVarChar);
                nameParam.Direction = ParameterDirection.Input;
                nameParam.Value = contractName;

                command.Parameters.Add(companyNameParam);
                command.Parameters.Add(nameParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable contractTable = new DataTable();

                adapter.Fill(contractTable);

                if (contractTable.Rows.Count > 0)
                {
                    DataRow row = contractTable.Rows[0];

                    Contract contract = ExtractContract(row);

                    return contract;
                }
                else
                {
                    throw new Exception("No Such Contract");
                }
            }
        }

        private static Contract ExtractContract(DataRow row)
        {
            string name = row["ContractName"].ToString();
            Guid id = new Guid(row["ContractID"].ToString());
            string status = row["ContractStatus"].ToString();
            string startDate = row["ContractStartDate"].ToString();
            string endDate = row["ContractEndDate"].ToString();
            Guid companyid = new Guid(row["CompanyID"].ToString());
            string isSSOContract = row["IsSSOContract"].ToString();
            string ipRestrict = row["IPRestrict"].ToString();
            string salesContact = row["SalesContact"].ToString();
            string contractUsername = row["ContractUsername"].ToString();

            Contract contract = new Contract();
            contract.Identifier = id;
            contract.Name = name;
            contract.SalesContact = salesContact;
            contract.ContractUsername = contractUsername;
            contract.CompanyID = companyid;

            // Contract IsActive
            if (status.ToUpper().Equals("ACTIVE"))
            {
                contract.IsActive = true;
            }
            else
            {
                contract.IsActive = false;
            }

            // Contract Start & End Date
            DateRange contractDateRange = new DateRange();

            DateTime ContractStartDateOut;
            DateTime.TryParse(startDate, out ContractStartDateOut);
            contractDateRange.Start = ContractStartDateOut;

            DateTime ContractEndDateOut;
            DateTime.TryParse(endDate, out ContractEndDateOut);
            contractDateRange.End = ContractEndDateOut;

            contract.DateRange = contractDateRange;

            // Contract IsSSOContract
            bool boolOut;
            bool.TryParse(isSSOContract, out boolOut);
            contract.IsSSOContract = boolOut;

            // Contract IPRestrict
            bool outIsIPRestrict;
            bool.TryParse(ipRestrict, out outIsIPRestrict);
            contract.IsIPRestrict = outIsIPRestrict;

            return contract;
        }

        public Company GetCompany(Guid companyIdentifier)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetCompany", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter compnanyIdParam = new SqlParameter("@companyIdentifier", SqlDbType.UniqueIdentifier);
                compnanyIdParam.Value = companyIdentifier;
                compnanyIdParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(compnanyIdParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable companyTable = new DataTable();

                adapter.Fill(companyTable);

                if (companyTable.Rows.Count > 0)
                {
                    DataRow row = companyTable.Rows[0];

                    string name = row["CompanyName"].ToString();
                    Guid id = (Guid)row["CompanyID"];
                    string status = row["CompanyStatus"].ToString();
                    string type = row["CompanyType"].ToString();

                    Company company = new Company();
                    company.Identifier = id;
                    company.Name = name;
                    company.Type = type;

                    if (status.ToUpper().Equals("ACTIVE"))
                    {
                        company.IsActive = true;
                    }
                    else
                    {
                        company.IsActive = false;
                    }

                    return company;
                }
                else
                {
                    throw new Exception("No Such Company");
                }
            }
        }

        public Company GetCompany(string companyName)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetCompanyByName", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter compnanyIdParam = new SqlParameter("@companyName", SqlDbType.NVarChar);
                compnanyIdParam.Value = companyName;
                compnanyIdParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(compnanyIdParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable companyTable = new DataTable();

                adapter.Fill(companyTable);

                if (companyTable.Rows.Count > 0)
                {
                    DataRow row = companyTable.Rows[0];

                    string name = row["CompanyName"].ToString();
                    Guid id = (Guid)row["CompanyID"];
                    string status = row["CompanyStatus"].ToString();
                    string type = row["CompanyType"].ToString();

                    Company company = new Company();
                    company.Identifier = id;
                    company.Name = name;
                    company.Type = type;

                    if (status.ToUpper().Equals("ACTIVE"))
                    {
                        company.IsActive = true;
                    }
                    else
                    {
                        company.IsActive = false;
                    }

                    return company;
                }
                else
                {
                    throw new Exception("No Such Company");
                }
            }
        }

        public Company CreateCompany(
            Guid companyId,
            string name,
            string tradingPartnerNumber,
            string adminContact,
            string receiver,
            string catalogSet,
            string changedBy,
            string purchasingManager,
            DateTime lastChanged,
            DateTime created,
            string type,
            string status,
            string address,
            string city,
            string state,
            string country,
            string postCode)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                connection.Open();
                
                SqlCommand command = new SqlCommand("dbo.pwsInsertCompany", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@companyIdentifier", companyId));
                command.Parameters.Add(new SqlParameter("@companyName", name));
                command.Parameters.Add(new SqlParameter("@tradingPartnerNumber", tradingPartnerNumber));
                command.Parameters.Add(new SqlParameter("@adminContact", adminContact));
                command.Parameters.Add(new SqlParameter("@receiver", receiver));
                command.Parameters.Add(new SqlParameter("@catalogSet", catalogSet));
                command.Parameters.Add(new SqlParameter("@changedBy", changedBy));
                command.Parameters.Add(new SqlParameter("@purchasingManager", purchasingManager));
                command.Parameters.Add(new SqlParameter("@lastChanged", lastChanged));
                command.Parameters.Add(new SqlParameter("@created", created));
                command.Parameters.Add(new SqlParameter("@type", type));
                command.Parameters.Add(new SqlParameter("@status", status));
                command.Parameters.Add(new SqlParameter("@address", address));
                command.Parameters.Add(new SqlParameter("@city", city));
                command.Parameters.Add(new SqlParameter("@state", state));
                command.Parameters.Add(new SqlParameter("@country", country));
                command.Parameters.Add(new SqlParameter("@postCode", postCode));
                
               command.ExecuteNonQuery();
            }

            return GetCompany(companyId);
        }

        public IPAddressRange[] GetIPRanges(Guid contractId)
        {
            IPAddressRange[] ipRanges = new IPAddressRange[0];

            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetIPAddressRangesByContract", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@contractIdentifier", contractId));

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable ipRangeTable = new DataTable();

                adapter.Fill(ipRangeTable);

                ipRanges = new IPAddressRange[ipRangeTable.Rows.Count];

                int index = 0;
                foreach (DataRow row in ipRangeTable.Rows)
                {
                    IPAddressRange range = new IPAddressRange();
                    range.LowerBound = long.Parse(row["Lower_IP"].ToString());
                    range.UpperBound = long.Parse(row["Upper_IP"].ToString());
                    ipRanges[index] = range;
                    index++;
                }
            }

            return ipRanges;
        }

        public ContractService[] GetContractServices(Guid contractId)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetContractServices", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter contractIdParam = new SqlParameter("@contractIdentifier", SqlDbType.UniqueIdentifier);
                contractIdParam.Value = contractId;
                contractIdParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(contractIdParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable servicesTable = new DataTable();

                adapter.Fill(servicesTable);

                ContractService[] contractServices = new ContractService[servicesTable.Rows.Count];

                int index = 0;
                foreach (DataRow row in servicesTable.Rows)
                {
                    string ServiceID = row["ServiceID"].ToString();
                    string ServiceName = row["ServiceName"].ToString();
                    string ServiceStatus = row["ServiceStatus"].ToString();
                    string StartDate = row["StartDate"].ToString();
                    string EndDate = row["EndDate"].ToString();
                    string ContractServiceStatus = row["ContractServiceStatus"].ToString();
                    string IsOwnershipRestricted = row["IsOwnershipRestricted"].ToString();

                    ContractService service = new ContractService();
                    service.Identifier = ServiceID;
                    service.Name = ServiceName;

                    bool serviceActiveOut;
                    bool.TryParse(ServiceStatus, out serviceActiveOut);
                    service.IsServiceActive = serviceActiveOut;

                    bool serviceContractActiveOut;
                    bool.TryParse(ContractServiceStatus, out serviceContractActiveOut);
                    service.IsActive = serviceContractActiveOut;

                    bool isOwnershipRestrictedOut;
                    bool.TryParse(IsOwnershipRestricted, out isOwnershipRestrictedOut);
                    service.IsOwnershipRestricted = isOwnershipRestrictedOut;

                    DateRange range = new DateRange();

                    DateTime startDateOut;
                    DateTime.TryParse(StartDate, out startDateOut);
                    range.Start = startDateOut;

                    DateTime endDateOut;
                    DateTime.TryParse(EndDate, out endDateOut);
                    range.End = endDateOut;

                    service.DateRange = range;

                    contractServices[index] = service;
                    index++;
                }

                return contractServices;
            }
        }

        public ContractService[] GetContractServicesByIPAddress(long ipAddress)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetContractServicesByIPAddress", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter ipAddressParam = new SqlParameter("@ipAddress", SqlDbType.BigInt);
                ipAddressParam.Value = ipAddress;
                ipAddressParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(ipAddressParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable servicesTable = new DataTable();

                adapter.Fill(servicesTable);

                ContractService[] contractServices = new ContractService[servicesTable.Rows.Count];

                int index = 0;
                foreach (DataRow row in servicesTable.Rows)
                {
                    string ServiceID = row["ServiceID"].ToString();
                    string ServiceName = row["ServiceName"].ToString();
                    string ServiceStatus = row["ServiceStatus"].ToString();
                    string StartDate = row["StartDate"].ToString();
                    string EndDate = row["EndDate"].ToString();
                    string ContractServiceStatus = row["ContractServiceStatus"].ToString();
                    string IsOwnershipRestricted = row["IsOwnershipRestricted"].ToString();

                    ContractService service = new ContractService();
                    service.Identifier = ServiceID;
                    service.Name = ServiceName;

                    bool serviceActiveOut;
                    bool.TryParse(ServiceStatus, out serviceActiveOut);
                    service.IsServiceActive = serviceActiveOut;

                    bool serviceContractActiveOut;
                    bool.TryParse(ContractServiceStatus, out serviceContractActiveOut);
                    service.IsActive = serviceContractActiveOut;

                    bool isOwnershipRestrictedOut;
                    bool.TryParse(IsOwnershipRestricted, out isOwnershipRestrictedOut);
                    service.IsOwnershipRestricted = isOwnershipRestrictedOut;

                    DateRange range = new DateRange();

                    DateTime startDateOut;
                    DateTime.TryParse(StartDate, out startDateOut);
                    range.Start = startDateOut;

                    DateTime endDateOut;
                    DateTime.TryParse(EndDate, out endDateOut);
                    range.End = endDateOut;

                    service.DateRange = range;

                    contractServices[index] = service;
                    index++;
                }

                return contractServices;
            }
        }

        public ContractService[] GetActiveContractServicesByIPAddress(long ipAddress)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetActiveContractServicesByIPAddress", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter ipAddressParam = new SqlParameter("@ipAddress", SqlDbType.BigInt);
                ipAddressParam.Value = ipAddress;
                ipAddressParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(ipAddressParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable servicesTable = new DataTable();

                adapter.Fill(servicesTable);

                ContractService[] contractServices = new ContractService[servicesTable.Rows.Count];

                int index = 0;
                foreach (DataRow row in servicesTable.Rows)
                {
                    string ServiceID = row["ServiceID"].ToString();
                    string ServiceName = row["ServiceName"].ToString();
                    string ServiceStatus = row["ServiceStatus"].ToString();
                    string StartDate = row["StartDate"].ToString();
                    string EndDate = row["EndDate"].ToString();
                    string ContractServiceStatus = row["ContractServiceStatus"].ToString();
                    string IsOwnershipRestricted = row["IsOwnershipRestricted"].ToString();

                    ContractService service = new ContractService();
                    service.Identifier = ServiceID;
                    service.Name = ServiceName;

                    bool serviceActiveOut;
                    bool.TryParse(ServiceStatus, out serviceActiveOut);
                    service.IsServiceActive = serviceActiveOut;

                    bool serviceContractActiveOut;
                    bool.TryParse(ContractServiceStatus, out serviceContractActiveOut);
                    service.IsActive = serviceContractActiveOut;

                    bool isOwnershipRestrictedOut;
                    bool.TryParse(IsOwnershipRestricted, out isOwnershipRestrictedOut);
                    service.IsOwnershipRestricted = isOwnershipRestrictedOut;

                    DateRange range = new DateRange();

                    DateTime startDateOut;
                    DateTime.TryParse(StartDate, out startDateOut);
                    range.Start = startDateOut;

                    DateTime endDateOut;
                    DateTime.TryParse(EndDate, out endDateOut);
                    range.End = endDateOut;

                    service.DateRange = range;

                    contractServices[index] = service;
                    index++;
                }

                return contractServices;
            }
        }

        public ContractService[] GetContractServicesByUsername(string username)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetContractServicesByUsername", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter usernameParam = new SqlParameter("@username", SqlDbType.NVarChar);
                usernameParam.Value = username;
                usernameParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(usernameParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable servicesTable = new DataTable();

                adapter.Fill(servicesTable);

                ContractService[] contractServices = new ContractService[servicesTable.Rows.Count];

                int index = 0;
                foreach (DataRow row in servicesTable.Rows)
                {
                    string ServiceID = row["ServiceID"].ToString();
                    string ServiceName = row["ServiceName"].ToString();
                    string ServiceStatus = row["ServiceStatus"].ToString();
                    string StartDate = row["StartDate"].ToString();
                    string EndDate = row["EndDate"].ToString();
                    string ContractServiceStatus = row["ContractServiceStatus"].ToString();
                    string IsOwnershipRestricted = row["IsOwnershipRestricted"].ToString();

                    ContractService service = new ContractService();
                    service.Identifier = ServiceID;
                    service.Name = ServiceName;

                    bool serviceActiveOut;
                    bool.TryParse(ServiceStatus, out serviceActiveOut);
                    service.IsServiceActive = serviceActiveOut;

                    bool serviceContractActiveOut;
                    bool.TryParse(ContractServiceStatus, out serviceContractActiveOut);
                    service.IsActive = serviceContractActiveOut;

                    bool isOwnershipRestrictedOut;
                    bool.TryParse(IsOwnershipRestricted, out isOwnershipRestrictedOut);
                    service.IsOwnershipRestricted = isOwnershipRestrictedOut;

                    DateRange range = new DateRange();

                    DateTime startDateOut;
                    DateTime.TryParse(StartDate, out startDateOut);
                    range.Start = startDateOut;

                    DateTime endDateOut;
                    DateTime.TryParse(EndDate, out endDateOut);
                    range.End = endDateOut;

                    service.DateRange = range;

                    contractServices[index] = service;
                    index++;
                }

                return contractServices;
            }
        }

        public ContractService[] GetActiveContractServicesByUsername(string username)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetActiveContractServicesByUsername", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter usernameParam = new SqlParameter("@username", SqlDbType.NVarChar);
                usernameParam.Value = username;
                usernameParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(usernameParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable servicesTable = new DataTable();

                adapter.Fill(servicesTable);

                ContractService[] contractServices = new ContractService[servicesTable.Rows.Count];

                int index = 0;
                foreach (DataRow row in servicesTable.Rows)
                {
                    string ServiceID = row["ServiceID"].ToString();
                    string ServiceName = row["ServiceName"].ToString();
                    string ServiceStatus = row["ServiceStatus"].ToString();
                    string StartDate = row["StartDate"].ToString();
                    string EndDate = row["EndDate"].ToString();
                    string ContractServiceStatus = row["ContractServiceStatus"].ToString();
                    string IsOwnershipRestricted = row["IsOwnershipRestricted"].ToString();

                    ContractService service = new ContractService();
                    service.Identifier = ServiceID;
                    service.Name = ServiceName;

                    bool serviceActiveOut;
                    bool.TryParse(ServiceStatus, out serviceActiveOut);
                    service.IsServiceActive = serviceActiveOut;

                    bool serviceContractActiveOut;
                    bool.TryParse(ContractServiceStatus, out serviceContractActiveOut);
                    service.IsActive = serviceContractActiveOut;

                    bool isOwnershipRestrictedOut;
                    bool.TryParse(IsOwnershipRestricted, out isOwnershipRestrictedOut);
                    service.IsOwnershipRestricted = isOwnershipRestrictedOut;

                    DateRange range = new DateRange();

                    DateTime startDateOut;
                    DateTime.TryParse(StartDate, out startDateOut);
                    range.Start = startDateOut;

                    DateTime endDateOut;
                    DateTime.TryParse(EndDate, out endDateOut);
                    range.End = endDateOut;

                    service.DateRange = range;

                    contractServices[index] = service;
                    index++;
                }

                return contractServices;
            }
        }

        public ContractService[] GetActiveContractServicesByUserGUID(Guid userGUID)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetActiveContractServicesByUserGUID", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter userGUIDParam = new SqlParameter("@userGUID", SqlDbType.UniqueIdentifier);
                userGUIDParam.Value = userGUID;
                userGUIDParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(userGUIDParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable servicesTable = new DataTable();

                adapter.Fill(servicesTable);

                ContractService[] contractServices = new ContractService[servicesTable.Rows.Count];

                int index = 0;
                foreach (DataRow row in servicesTable.Rows)
                {
                    string ServiceID = row["ServiceID"].ToString();
                    string ServiceName = row["ServiceName"].ToString();
                    string ServiceStatus = row["ServiceStatus"].ToString();
                    string StartDate = row["StartDate"].ToString();
                    string EndDate = row["EndDate"].ToString();
                    string ContractServiceStatus = row["ContractServiceStatus"].ToString();
                    string IsOwnershipRestricted = row["IsOwnershipRestricted"].ToString();

                    ContractService service = new ContractService();
                    service.Identifier = ServiceID;
                    service.Name = ServiceName;

                    bool serviceActiveOut;
                    bool.TryParse(ServiceStatus, out serviceActiveOut);
                    service.IsServiceActive = serviceActiveOut;

                    bool serviceContractActiveOut;
                    bool.TryParse(ContractServiceStatus, out serviceContractActiveOut);
                    service.IsActive = serviceContractActiveOut;

                    bool isOwnershipRestrictedOut;
                    bool.TryParse(IsOwnershipRestricted, out isOwnershipRestrictedOut);
                    service.IsOwnershipRestricted = isOwnershipRestrictedOut;

                    DateRange range = new DateRange();

                    DateTime startDateOut;
                    DateTime.TryParse(StartDate, out startDateOut);
                    range.Start = startDateOut;

                    DateTime endDateOut;
                    DateTime.TryParse(EndDate, out endDateOut);
                    range.End = endDateOut;

                    service.DateRange = range;

                    contractServices[index] = service;
                    index++;
                }

                return contractServices;
            }
        }

        public void SetContractServices(
            Guid contractId, 
            int[] serviceIds, 
            bool status,
            DateTime contractStartDate,
            DateTime contractEndDate)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsUpdatedContractServices", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@contractIdentifier", contractId));
                command.Parameters.Add(new SqlParameter("@serviceIDs", String.Join(",", Array.ConvertAll<int, string>(serviceIds, delegate(int i) { return i.ToString(); }))));
                command.Parameters.Add(new SqlParameter("@startDate", contractStartDate));
                command.Parameters.Add(new SqlParameter("@endDate", contractEndDate));
                command.Parameters.Add(new SqlParameter("@status", status));


                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
            }
            
        }

        /// <summary>
        /// Updates the user's sign in credentials for last login and login attempts
        /// </summary>
        /// <param name="username">The username of the user</param>
        /// <param name="p">Is the sign in successfull</param>
        public void UpdateSignInCredentials(string username, bool p)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsUpdateUserAuthenticationCredentials", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@u_logon_name", username));
                command.Parameters.Add(new SqlParameter("@isSuccesfulSignIn", p));
                connection.Open();

                command.ExecuteNonQuery();

                connection.Close();
            }
        }

        // Sets
        public Session PersistSession(Session session)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsUpdateSession", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter guidOutput = new SqlParameter("@sessionId", SqlDbType.UniqueIdentifier);
                guidOutput.Direction = ParameterDirection.InputOutput;
                if (session.Identifier != null)
                {
                    guidOutput.Value = session.Identifier;
                }

                SqlParameter createDateOutput = new SqlParameter("@CreateDate", SqlDbType.DateTime);
                createDateOutput.Direction = ParameterDirection.Output;

                SqlParameter expiryDateOutput = new SqlParameter("@ExpiryDate", SqlDbType.DateTime);
                expiryDateOutput.Direction = ParameterDirection.Output;

                SqlParameter timeSpanDaysParam = new SqlParameter("@timeSpanDays", session.TimeOutDays);
                SqlParameter logonnameParam = new SqlParameter("@logonname", session.User.Username);

                command.Parameters.Add(guidOutput);
                command.Parameters.Add(createDateOutput);
                command.Parameters.Add(expiryDateOutput);
                command.Parameters.Add(timeSpanDaysParam);
                command.Parameters.Add(logonnameParam);

                connection.Open();

                command.ExecuteNonQuery();

                connection.Close();

                session.Identifier = new Guid(guidOutput.Value.ToString());
                session.ExpiryDate = DateTime.Parse(expiryDateOutput.Value.ToString());
                session.CreateDate = DateTime.Parse(createDateOutput.Value.ToString());
            }

            return session;
        }

        public User PersistUserAuthentication(User user)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsInsertUserAuthentication", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter usernameParam = new SqlParameter("@u_logon_name", SqlDbType.NVarChar);
                usernameParam.Value = user.Username;
                usernameParam.Direction = ParameterDirection.Input;

                SqlParameter passwordParam = new SqlParameter("@hash", SqlDbType.NVarChar);
                passwordParam.Value = user.Password.PasswordHash;
                passwordParam.Direction = ParameterDirection.Input;

                SqlParameter passwordSaltParam = new SqlParameter("@salt", SqlDbType.NVarChar);
                passwordSaltParam.Value = user.Password.Salt;
                passwordSaltParam.Direction = ParameterDirection.Input;

                SqlParameter timeParam = new SqlParameter("@timeStamp", SqlDbType.DateTime);
                timeParam.Value = DateTime.Now;
                timeParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(usernameParam);
                command.Parameters.Add(passwordParam);
                command.Parameters.Add(passwordSaltParam);
                command.Parameters.Add(timeParam);

                connection.Open();

                command.ExecuteNonQuery();

                connection.Close();

                return user;
            }
        }

        public User PersistUser(User user)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsUpdateUser", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@Username", user.Username));
                command.Parameters.Add(new SqlParameter("@UserId", user.Guid));
                command.Parameters.Add(new SqlParameter("@FirstName", user.FirstName));
                command.Parameters.Add(new SqlParameter("@LastName", user.LastName));
                command.Parameters.Add(new SqlParameter("@Email", user.Email));
                command.Parameters.Add(new SqlParameter("@JobFunction", user.JobFunction));
                command.Parameters.Add(new SqlParameter("@Address", user.Address));
                command.Parameters.Add(new SqlParameter("@City", user.City));
                command.Parameters.Add(new SqlParameter("@PostCode", user.PostCode));
                command.Parameters.Add(new SqlParameter("@Telephone", user.Telephone));
                command.Parameters.Add(new SqlParameter("@ReceiveRegularUpdates", user.ReceiveRegularUpdates));
                command.Parameters.Add(new SqlParameter("@GateKeeperUser", user.IsGatekeeper));
                command.Parameters.Add(new SqlParameter("@SharedPassword", user.IsSharedPassword));
                command.Parameters.Add(new SqlParameter("@ContractID", user.ContractID));
                command.Parameters.Add(new SqlParameter("@IPRestrict", user.IsIPRestrict));
                command.Parameters.Add(new SqlParameter("@IsAccountActive", user.IsAccountActive));
                command.Parameters.Add(new SqlParameter("@ChangePasswordNextLogin", user.IsChangePasswordNextLogin));
                command.Parameters.Add(new SqlParameter("@PurchasedAmountToDate", user.PurchasedAmountToDate));
                command.Parameters.Add(new SqlParameter("@PurchaseLimit", user.PurchaseLimit));
                command.Parameters.Add(new SqlParameter("@IsBillingCodeRequired", user.IsBillingCodeRequired));
                command.Parameters.Add(new SqlParameter("@PreferredCurrency", user.PreferredCurrency));
                command.Parameters.Add(new SqlParameter("@IsNewUser", user.IsNewUser));
                command.Parameters.Add(new SqlParameter("@IsAffiliateUser", user.IsAffiliateUser));
                command.Parameters.Add(new SqlParameter("@IsPressUser", user.IsPressUser));
                command.Parameters.Add(new SqlParameter("@IsMarketingContact", user.IsMarketingContact));
                command.Parameters.Add(new SqlParameter("@IsNotifyOfNewProducts", user.IsNotifyOfNewProducts));
                command.Parameters.Add(new SqlParameter("@UserType", user.UserType));
                command.Parameters.Add(new SqlParameter("@DateCreated", user.DateCreated));
                command.Parameters.Add(new SqlParameter("@IsAutoLogin", user.IsAutoLogin));
                command.Parameters.Add(new SqlParameter("@PreferredEmailFormat", user.PreferredEmailFormat));
                command.Parameters.Add(new SqlParameter("@Interests", user.Interests));
                command.Parameters.Add(new SqlParameter("@SectorForURLRedirection", user.SectorForURLRedirection));
                command.Parameters.Add(new SqlParameter("@SalesContact", user.SalesContact));
                command.Parameters.Add(new SqlParameter("@JobTitle", user.JobTitle));
                command.Parameters.Add(new SqlParameter("@State", user.State));
                command.Parameters.Add(new SqlParameter("@Fax", user.Fax));
                command.Parameters.Add(new SqlParameter("@Title", user.Title));
                command.Parameters.Add(new SqlParameter("@Country", user.Country));
                command.Parameters.Add(new SqlParameter("@IsProfileUpdated", user.IsProfileUpdated ? 1 : 0) );

                connection.Open();

                command.ExecuteNonQuery();

                connection.Close();

                return user;
            }
        }

        // Updates
        public bool DeactivateUser(string username, Guid contractId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
                {
                    SqlCommand command = new SqlCommand("dbo.pwsDeactivateUser", connection);
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter usernameParam = new SqlParameter("@username", SqlDbType.NVarChar);
                    usernameParam.Direction = ParameterDirection.Input;
                    usernameParam.Value = username;

                    SqlParameter contractIdParam = new SqlParameter("@contractId", SqlDbType.UniqueIdentifier);
                    contractIdParam.Direction = ParameterDirection.Input;
                    contractIdParam.Value = contractId;

                    command.Parameters.Add(usernameParam);
                    command.Parameters.Add(contractIdParam);

                    connection.Open();

                    command.ExecuteNonQuery();

                    connection.Close();
                }

                return true;
            }
            catch (Exception) { return false; }
        }

        // Deletes
        public void DeleteSession(Session session)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsDeleteSession", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter sessionGuidParam = new SqlParameter("@sessionId", SqlDbType.UniqueIdentifier);
                sessionGuidParam.Value = session.Identifier;
                sessionGuidParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(sessionGuidParam);

                connection.Open();

                command.ExecuteNonQuery();

                connection.Close();
            }
        }

        public void DeleteSessions(User user)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsDeleteSessionsByUsername", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter usernameParam = new SqlParameter("@username", SqlDbType.NVarChar);
                usernameParam.Value = user.Username;
                usernameParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(usernameParam);

                connection.Open();

                command.ExecuteNonQuery();

                connection.Close();
            }
        }

        public string GetUserInterests(Guid userId, int  serviceId)
        {
            string interests = string .Empty ;
        
                using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
                {
                    SqlCommand command = new SqlCommand("pwsGetUserInterests", connection);
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter usernameParam = new SqlParameter("@UserId", SqlDbType.UniqueIdentifier);
                    usernameParam.Direction = ParameterDirection.Input;
                    usernameParam.Value = userId;

                    SqlParameter kcnameParam = new SqlParameter("@ServiceId", SqlDbType.Int);
                    kcnameParam.Direction = ParameterDirection.Input;
                    kcnameParam.Value = serviceId ;

                    command.Parameters.Add(usernameParam);
                    command.Parameters.Add(kcnameParam);

                    connection.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    DataTable dtInterests = new DataTable();

                    try
                    {
                        adapter.Fill(dtInterests);
                        foreach (DataRow row in dtInterests.Rows)
                        {
                            interests  = row["interests"].ToString();
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        throw new Exception("Interests DB retrieval failed", sqlEx.InnerException);
                    }
                    
                    connection.Close();
                }

               return interests;
          }

  

        public void SetUserInterests(Guid userId, int  serviceId,  string interests)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("pwsUpdateUserInterests", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter usernameParam = new SqlParameter("@UserId", SqlDbType.UniqueIdentifier);
                usernameParam.Direction = ParameterDirection.Input;
                usernameParam.Value = userId;

                SqlParameter serviceIdParam = new SqlParameter("@ServiceId", SqlDbType.Int);
                serviceIdParam.Direction = ParameterDirection.Input;
                serviceIdParam.Value = serviceId ;

                SqlParameter interestsParam = new SqlParameter("@Interests", SqlDbType.VarChar);
                interestsParam.Direction = ParameterDirection.Input;
                interestsParam.Value = interests;

                command.Parameters.Add(usernameParam);
                command.Parameters.Add(serviceIdParam);
                command.Parameters.Add(interestsParam);

                connection.Open();

                command.ExecuteNonQuery();

                connection.Close();
            }

        }
           

        #region Signature DataAccess

        public Signature GetSignature(Guid identifier)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsGetSignature", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter signatureIdParam = new SqlParameter("@signatureId", SqlDbType.UniqueIdentifier);
                signatureIdParam.Value = identifier;
                signatureIdParam.Direction = ParameterDirection.Input;

                command.Parameters.Add(signatureIdParam);

                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataTable signatureTable = new DataTable();

                adapter.Fill(signatureTable);

                if (signatureTable.Rows.Count > 0)
                {
                    DataRow row = signatureTable.Rows[0];

                    string userName = row["UserName"].ToString();
                    string dateCreated = row["DateCreated"].ToString();
                    string timeOutDays = row["timeOutDays"].ToString();
                    string  isActive = row["isActive"].ToString();

                    Signature  signature  = new Signature();
                    signature.Identifier = identifier;
                    signature.UserName = userName;

                    DateTime outdateCreated ;
                    DateTime.TryParse(dateCreated, out outdateCreated);
                    signature.DateCreated = outdateCreated;

                    int outtimeOutDays;
                    int.TryParse(timeOutDays,out outtimeOutDays);
                    signature.TimeOutDays = outtimeOutDays;

                    bool outIsActive;
                    bool.TryParse(isActive, out outIsActive);
                    signature.IsActive = outIsActive;

                    return signature;
                }
                else
                {
                    return null;
                }
            }
        }

        public Signature  CreateSignature(Signature signature)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsCreateSignature", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter signatureId = new SqlParameter("@signatureId", SqlDbType.UniqueIdentifier);
                signatureId.Direction = ParameterDirection.Output;
                SqlParameter username = new SqlParameter("@UserName", signature.UserName);
                SqlParameter timeOutDays = new SqlParameter("@timeOutDays", signature.TimeOutDays);
                SqlParameter isactive = new SqlParameter("@isActive", Convert.ToByte(signature.IsActive));
                SqlParameter dateCreated = new SqlParameter("@dateCreated", SqlDbType.DateTime);
                dateCreated.Direction = ParameterDirection.Output;

                command.Parameters.Add(signatureId);
                command.Parameters.Add(username);
                command.Parameters.Add(timeOutDays);
                command.Parameters.Add(isactive);
                command.Parameters.Add(dateCreated);

                connection.Open();
                int rowCount = 0;
                rowCount = command.ExecuteNonQuery();
                connection.Close();

                
                if (rowCount > 0)
                {
                    signature.DateCreated = DateTime.Parse(dateCreated.Value.ToString()); 
                    signature.Identifier =  new Guid (signatureId.Value.ToString());
                    return signature;
                }
                else
                {
                    return null;
                }


            }
        }

        public Signature UpdateSignature(Signature signature)
        {
            using (SqlConnection connection = new SqlConnection(ModelConfiguration.ConnectionString))
            {
                SqlCommand command = new SqlCommand("dbo.pwsUpdateSignature", connection);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[2];

                parameters[0] = new SqlParameter("@signatureId", signature.Identifier.ToString());
                parameters[1] = new SqlParameter("@isActive", Convert.ToByte( signature.IsActive));

                command.Parameters.AddRange(parameters);

                connection.Open();
                int rowCount = 0;
                rowCount = command.ExecuteNonQuery();
                connection.Close();

                if (rowCount > 0)
                    return signature;
                else
                    throw new Exception ("Unable to update signature");

            }
        }
        #endregion

    }
}
