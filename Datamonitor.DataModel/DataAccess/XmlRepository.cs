using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Datamonitor.DataModel.Interfaces;
using Datamonitor.DataModel.Utilities;
using Datamonitor.DataModel.Exceptions;
using Datamonitor.DataModel.Structures;
using System.Collections;
using System.Reflection;
using System.Collections.Specialized;

namespace Datamonitor.DataModel.DataAccess
{

    public class XmlRepository : IRepository
    {
        private XmlDocument xmlDocument;

        public XmlRepository()
        {
            this.xmlDocument = new XmlDocument();
            this.xmlDocument.Load(ModelConfiguration.ConnectionString);
        }

        private XmlDocument XmlData
        {
            get { return this.xmlDocument; }
        }

        public bool CheckUserExists(string username)
        {
            throw new Exception("Method CheckUserExists(string username) not implemented");
        }

        public User GetUserByUserGUID(Guid userGUID)
        {
            throw new Exception("Method GetUserByUserGUID(Guid userGUID) not implemented");
        }

        public User GetUser(string username)
        {

            string xPath = String.Format("UnitTestData/Users/User[@Username = '{0}']", username);
            XmlNode objectNode = XmlData.SelectSingleNode(xPath);

            if (objectNode == null)
            {
                return null;
            }
            else
            {
                User user = new User();

                user.Address = objectNode.Attributes["Address"].InnerText;
                user.City = objectNode.Attributes["City"].InnerText;                
                user.ContractID = new Guid(objectNode.Attributes["ContractID"].InnerText);
                user.Country = objectNode.Attributes["Country"].InnerText;
                user.DateCreated = DateTime.Now;
                user.Email = objectNode.Attributes["Email"].InnerText;
                user.FirstName = objectNode.Attributes["FirstName"].InnerText;
                user.Guid = new Guid(objectNode.Attributes["Guid"].InnerText);
                user.Interests = objectNode.Attributes["Interests"].InnerText;
                user.IsAccountActive = bool.Parse(objectNode.Attributes["IsAccountActive"].InnerText);
                user.IsAffiliateUser = bool.Parse(objectNode.Attributes["IsAffiliateUser"].InnerText);
                user.IsAutoLogin = bool.Parse(objectNode.Attributes["IsAutoLogin"].InnerText);
                user.IsBillingCodeRequired = bool.Parse(objectNode.Attributes["IsBillingCodeRequired"].InnerText);
                user.IsChangePasswordNextLogin = bool.Parse(objectNode.Attributes["IsChangePasswordNextLogin"].InnerText);
                user.IsGatekeeper = bool.Parse(objectNode.Attributes["IsGatekeeper"].InnerText);
                user.IsIPRestrict = bool.Parse(objectNode.Attributes["IsIPRestrict"].InnerText);
                user.IsMarketingContact = bool.Parse(objectNode.Attributes["IsMarketingContact"].InnerText);
                user.IsNewUser = bool.Parse(objectNode.Attributes["IsNewUser"].InnerText);
                user.IsNotifyOfNewProducts = bool.Parse(objectNode.Attributes["IsNotifyOfNewProducts"].InnerText);
                user.IsPressUser = bool.Parse(objectNode.Attributes["IsPressUser"].InnerText);
                user.IsSharedPassword = bool.Parse(objectNode.Attributes["IsSharedPassword"].InnerText);
                user.JobFunction = objectNode.Attributes["JobFunction"].InnerText;
                user.LastName = objectNode.Attributes["LastName"].InnerText;                
                user.Password = new Password(objectNode.SelectSingleNode("Password").Attributes["PasswordHash"].InnerText, objectNode.SelectSingleNode("Password").Attributes["Salt"].InnerText);
                user.PostCode = objectNode.Attributes["PostCode"].InnerText;
                user.PreferredCurrency = objectNode.Attributes["PreferredCurrency"].InnerText;
                user.PreferredEmailFormat = objectNode.Attributes["PreferredEmailFormat"].InnerText;
                user.PurchasedAmountToDate = int.Parse(objectNode.Attributes["PurchasedAmountToDate"].InnerText);
                user.PurchaseLimit = int.Parse(objectNode.Attributes["PurchaseLimit"].InnerText);
                user.ReceiveRegularUpdates = bool.Parse(objectNode.Attributes["ReceiveRegularUpdates"].InnerText);
                user.SalesContact = objectNode.Attributes["SalesContact"].InnerText;
                user.SectorForURLRedirection = objectNode.Attributes["SectorForURLRedirection"].InnerText;
                user.Telephone = objectNode.Attributes["Telephone"].InnerText;
                user.Username = objectNode.Attributes["Username"].InnerText;
                user.UserType = int.Parse(objectNode.Attributes["UserType"].InnerText);

                //As long as the username property has been set! It's only xpath.
                user.Password = GetPassword(user);
                
                return user;

            }

        }

        public Contract GetContract(Guid contractGuid)
        {
            string xPath = String.Format("UnitTestData/Contracts/Contract[@Identifier = '{0}']", contractGuid.ToString("B"));

            XmlNode objectNode = XmlData.SelectSingleNode(xPath);

            if (objectNode == null)
            {
                return null;
            }
            else
            {

                DateRange range;
                range.Start = DateTime.Parse(objectNode.Attributes["StartDate"].InnerText);
                range.End = DateTime.Parse(objectNode.Attributes["EndDate"].InnerText);

                string identAtt = objectNode.Attributes["Identifier"].InnerText;
                string nameAtt = objectNode.Attributes["Name"].InnerText;
                string refAtt = objectNode.Attributes["Referrer"].InnerText;
                string comAtt = objectNode.Attributes["Company"].InnerText;
                DateTime startAtt = DateTime.Parse(objectNode.Attributes["StartDate"].InnerText);
                DateTime endAtt = DateTime.Parse(objectNode.Attributes["EndDate"].InnerText);
                bool activeAtt = bool.Parse(objectNode.Attributes["IsActive"].InnerText);

                Contract contract = new Contract();
                contract.Identifier = new Guid(identAtt);
                contract.Name = nameAtt;
               
                DateRange dateRange = new DateRange();
                dateRange.Start = startAtt;
                dateRange.End = endAtt;

                contract.DateRange = dateRange;
                contract.IsActive = activeAtt;
                //contract.IPRanges = GetIPRanges(identAtt);
                //contract.Services = GetContractServices(identAtt);

                return contract;
            }
        }

        public Contract GetContract(string username)
        {
            throw new Exception("Method GetContract(string username) not implemented");
        }

        public Contract GetContract(Guid companyIdentifier, string contractName)
        {
            return GetContract(companyIdentifier);
        }

        public Contract GetContract(long ipAddress)
        {
            throw new Exception("Method GetContract(long ipAddress) not implemented");
        }

        public Contract[] GetContracts(long ipAddress)
        {
            throw new Exception("Method GetContracts(long ipAddress) not implemented");
        }

        public Contract[] GetContractsForHCServices(long ipAddress)
        {
            throw new Exception("Method GetContractsForHCServices(long ipAddress) not implemented");
        }

        public Contract[] GetContractsForVerdictServices(long ipAddress)
        {
            throw new Exception("Method GetContractsForVerdictServices(long ipAddress) not implemented");
        }

        public Session GetSession(string sessionToken)
        {
            string xPath = String.Format("UnitTestData/Sessions/Session[@Identifier = '{0}']", sessionToken);

            XmlNode objectNode = XmlData.SelectSingleNode(xPath);

            if (objectNode == null)
            {
                return null;
            }
            else           
            {
                string identAtt = objectNode.Attributes["Identifier"].InnerText;
                string userAtt = objectNode.Attributes["User"].InnerText;

                Session session = new Session(new Guid(identAtt));
                session.User = GetUser(userAtt);
                session.CreateDate = DateTime.Now;
                session.ExpiryDate = DateTime.Now.AddDays(30); 
                return session;
            }
        }

        public List<Session> GetSessions()
        {
            List<Session> sessions = new List<Session>();
            string xPath = String.Format("UnitTestData/Sessions/Session");

            XmlNodeList objectNodes = XmlData.SelectNodes(xPath);

            if (objectNodes == null)
            {
                return null;
            }
            else
            {
                foreach (XmlNode objectNode in objectNodes)
                {
                    string identAtt = objectNode.Attributes["Identifier"].InnerText;
                    int timespanMinutesAtt = int.Parse(objectNode.Attributes["CreateDateTimespanMinutes"].InnerText);
                    string userAtt = objectNode.Attributes["User"].InnerText;
                    sessions.Add(new Session(new Guid(identAtt)));
                }
            }

            return sessions;
        }
        
        public void TerminateSession(Session session)
        {

            string xPath = String.Format("UnitTestData/Sessions/Session[@Identifier = '{0}']", session.Identifier);

            XmlNode objectNode = XmlData.SelectSingleNode(xPath);

            XmlData.SelectSingleNode("UnitTestData/Sessions").RemoveChild(objectNode);
        }

        public User PersistUser(User user)
        {


            XmlAttribute[] attributes = new XmlAttribute[50];

            attributes[0] = XmlData.CreateAttribute("Address");
            attributes[0].InnerText = user.Address;
            attributes[1] = XmlData.CreateAttribute("City");
            attributes[1].InnerText = user.City;
            attributes[6] = XmlData.CreateAttribute("ContractID");
            attributes[6].InnerText = user.ContractID.ToString("B").ToUpper();
            attributes[7] = XmlData.CreateAttribute("Country");
            attributes[7].InnerText = user.Country;
            attributes[8] = XmlData.CreateAttribute("DateCreated");
            attributes[8].InnerText = user.DateCreated.ToString();
            attributes[9] = XmlData.CreateAttribute("Email");
            attributes[9].InnerText = user.Email;
            attributes[10] = XmlData.CreateAttribute("FirstName");
            attributes[10].InnerText = user.FirstName;
            attributes[11] = XmlData.CreateAttribute("Guid");
            attributes[11].InnerText = user.Guid.ToString();
            attributes[12] = XmlData.CreateAttribute("Interests");
            attributes[12].InnerText = user.Interests;
            attributes[13] = XmlData.CreateAttribute("IsAccountActive");
            attributes[13].InnerText = user.IsAccountActive.ToString();
            attributes[14] = XmlData.CreateAttribute("IsAffiliateUser");
            attributes[14].InnerText = user.IsAffiliateUser.ToString();
            attributes[15] = XmlData.CreateAttribute("IsAutoLogin");
            attributes[15].InnerText = user.IsAutoLogin.ToString();
            attributes[16] = XmlData.CreateAttribute("IsBillingCodeRequired");
            attributes[16].InnerText = user.IsBillingCodeRequired.ToString();
            attributes[17] = XmlData.CreateAttribute("IsChangePasswordNextLogin");
            attributes[17].InnerText = user.IsChangePasswordNextLogin.ToString();
            attributes[18] = XmlData.CreateAttribute("IsGatekeeper");
            attributes[18].InnerText = user.IsGatekeeper.ToString();
            attributes[19] = XmlData.CreateAttribute("IsIPRestrict");
            attributes[19].InnerText = user.IsIPRestrict.ToString();
            attributes[20] = XmlData.CreateAttribute("IsMarketingContact");
            attributes[20].InnerText = user.IsMarketingContact.ToString();
            attributes[21] = XmlData.CreateAttribute("IsNewUser");
            attributes[21].InnerText = user.IsNewUser.ToString();
            attributes[22] = XmlData.CreateAttribute("IsNotifyOfNewProducts");
            attributes[22].InnerText = user.IsNotifyOfNewProducts.ToString();
            attributes[23] = XmlData.CreateAttribute("IsPressUser");
            attributes[23].InnerText = user.IsPressUser.ToString();
            attributes[24] = XmlData.CreateAttribute("IsSharedPassword");
            attributes[24].InnerText = user.IsSharedPassword.ToString();
            attributes[25] = XmlData.CreateAttribute("JobFunction");
            attributes[25].InnerText = user.JobFunction;
            attributes[26] = XmlData.CreateAttribute("LastName");
            attributes[26].InnerText = user.LastName;
            attributes[28] = XmlData.CreateAttribute("PostCode");
            attributes[28].InnerText = user.PostCode;
            attributes[29] = XmlData.CreateAttribute("PreferredCurrency");
            attributes[29].InnerText = user.PreferredCurrency;
            attributes[30] = XmlData.CreateAttribute("PreferredEmailFormat");
            attributes[30].InnerText = user.PreferredEmailFormat;
            attributes[31] = XmlData.CreateAttribute("PurchasedAmountToDate");
            attributes[31].InnerText = user.PurchasedAmountToDate.ToString();
            attributes[32] = XmlData.CreateAttribute("PurchaseLimit");
            attributes[32].InnerText = user.PurchaseLimit.ToString();
            attributes[33] = XmlData.CreateAttribute("ReceiveRegularUpdates");
            attributes[33].InnerText = user.ReceiveRegularUpdates.ToString();
            attributes[34] = XmlData.CreateAttribute("SalesContact");
            attributes[34].InnerText = user.SalesContact;
            attributes[35] = XmlData.CreateAttribute("SectorForURLRedirection");
            attributes[35].InnerText = user.SectorForURLRedirection;
            attributes[36] = XmlData.CreateAttribute("Telephone");
            attributes[36].InnerText = user.Telephone;
            attributes[37] = XmlData.CreateAttribute("UserType");
            attributes[37].InnerText = user.UserType.ToString();

            XmlNode usersNode = XmlData.SelectSingleNode("UnitTestData/Users");

            XmlElement newUserElement = XmlData.CreateElement("User");

            foreach (XmlAttribute attribute in attributes)
            {
                if (attribute != null)
                {
                    newUserElement.Attributes.Append(attribute);
                }
            }

            if (GetUser(user.Username) == null)
            {              

                XmlNode passwordElement = XmlData.CreateElement("Password");
                XmlAttribute lastSignInAttemptAttribute = XmlData.CreateAttribute("LastSignInAttempt");
                XmlAttribute signInAttemptsAttribute = XmlData.CreateAttribute("SignInAttempts");
                XmlAttribute hashAttribute = XmlData.CreateAttribute("PasswordHash");
                XmlAttribute saltAttribute = XmlData.CreateAttribute("Salt");
                passwordElement.Attributes.Append(lastSignInAttemptAttribute);
                passwordElement.Attributes.Append(signInAttemptsAttribute);
                passwordElement.Attributes.Append(hashAttribute);
                passwordElement.Attributes.Append(saltAttribute);

                XmlNode usersForNewNode = XmlData.SelectSingleNode("UnitTestData/Users");
                newUserElement.AppendChild(passwordElement);
                usersForNewNode.AppendChild(newUserElement);
            }
            else
            {
                string xPath = String.Format("UnitTestData/Users/User[@Username = '{0}']", user.Username);
                XmlNode objectNode = XmlData.SelectSingleNode(xPath);
                //XmlData.ReplaceChild(newUserElement, objectNode);
            }

            return user;

        }



        public Session PersistSession(Session session)
        {
            Guid sessionId = Guid.NewGuid();
            
            TimeSpan spanExpiry = new TimeSpan(session.ExpiryDate.Ticks);
            TimeSpan spanCreate = new TimeSpan(session.CreateDate.Ticks);
            TimeSpan spanDifference = spanExpiry.Subtract(spanCreate);
            int totalMinutes = Convert.ToInt32(spanDifference.TotalMinutes);

            XmlNode sessionsNode = XmlData.SelectSingleNode("UnitTestData/Sessions");
            XmlElement sessionElement = XmlData.CreateElement("Session");
            XmlAttribute idAtt = XmlData.CreateAttribute("Identifier");
            XmlAttribute minutesAtt = XmlData.CreateAttribute("CreateDateTimespanMinutes");
            XmlAttribute userAtt = XmlData.CreateAttribute("User");

            idAtt.InnerText = sessionId.ToString();
            minutesAtt.InnerText = totalMinutes.ToString();
            userAtt.InnerText = session.User.Username;
            
            sessionElement.Attributes.Append(idAtt);
            sessionElement.Attributes.Append(minutesAtt);
            sessionElement.Attributes.Append(userAtt);
            
            sessionsNode.AppendChild(sessionElement);

            if (session.Identifier == Guid.Empty)
            {
                session.Identifier = sessionId;
                session.CreateDate = DateTime.Now;
            }

            return session;
        }
        
        public Company GetCompany(Guid companyIdentifier)
        {            
            string xPath = String.Format("UnitTestData/Companies/Company[@Identifier = '{0}']", companyIdentifier.ToString("B"));

            XmlNode objectNode = XmlData.SelectSingleNode(xPath);

            if (objectNode == null)           
            {

                return null;
            }
            else
            {

                string identAtt = objectNode.Attributes["Identifier"].InnerText;
                string nameAtt = objectNode.Attributes["Name"].InnerText;
                bool isActAtt = bool.Parse(objectNode.Attributes["IsActive"].InnerText);
                
                Company company = new Company();
                company.Identifier = new Guid(identAtt);
                company.Name = nameAtt;
                company.IsActive = isActAtt;

                return company;
            }

        }

        public Company GetCompany(string companyName)
        {
            throw new NotImplementedException();
        }

        public IPAddressRange[] GetIPRanges(Guid contractId)
        {
            List<IPAddressRange> ranges = new List<IPAddressRange>();
            string xPath = String.Format("UnitTestData/IpAdddressRanges/Range[@Contract= '{0}']", contractId);
            XmlNodeList objectNodes = XmlData.SelectNodes(xPath);

            if (objectNodes == null)
            {
                return null;
            }
            else
            {
                foreach (XmlNode objectNode in objectNodes)
                {
                    long lowerBound = long.Parse(objectNode.Attributes["Lower"].InnerText);
                    long upperBound = long.Parse(objectNode.Attributes["Upper"].InnerText);
                    IPAddressRange range;
                    range.LowerBound = lowerBound;
                    range.UpperBound = upperBound;
                    ranges.Add(range);
                }
            }

            return ranges.ToArray();
        }



        public ContractService[] GetContractServices(Guid contractId)
        {

            List<ContractService> services = new List<ContractService>();

            string xPath = String.Format("UnitTestData/ContractServices/ContractService[@Contract= '{0}']", contractId);

            XmlNodeList objectNodes = XmlData.SelectNodes(xPath);

            if (objectNodes == null)
            {
                return null;
            }
            else
            {                
                foreach (XmlNode objectNode in objectNodes)
                {

                    DateRange range;
                    range.Start = DateTime.Parse(objectNode.Attributes["StartDate"].InnerText);
                    range.End = DateTime.Parse(objectNode.Attributes["EndDate"].InnerText);
                    string idenAtt = objectNode.Attributes["Service"].InnerText;
                    bool isActiveAtt = bool.Parse(objectNode.Attributes["IsActive"].InnerText);

                    ContractService contractService = new ContractService();
                    contractService.Identifier = idenAtt;
                    contractService.DateRange = range;
                    contractService.IsActive = isActiveAtt;

                    services.Add(contractService);
                }
            }

            return services.ToArray();

        }

        public void SetContractServices(
            Guid contractId,
            int[] serviceIds,
            bool status,
            DateTime contractStartDate,
            DateTime contractEndDate)
        {
            // TODO: Implement xml service setting
        }

        private string GetServiceName(string serviceId)
        {

            string xPath = String.Format("UnitTestData/Services/Service[@Identifier= '{0}']", serviceId);
            XmlNode serviceNode = XmlData.SelectSingleNode(xPath);
            
            if (serviceNode == null)
            {
                return null;                
            }
            else
            {
                return serviceNode.Attributes["Name"].InnerText;
            }
        }

        public Session GetSession(Guid authenticationToken)
        {
            string xPath = String.Format("UnitTestData/Sessions/Session[@Identifier='{0}']", authenticationToken.ToString("B").ToUpper());
            XmlNode objNode = XmlData.SelectSingleNode(xPath);
            Session session = new Session(authenticationToken);
            session.CreateDate = DateTime.Parse(objNode.Attributes["CreateDate"].InnerText);            
            session.User = GetUser(objNode.Attributes["User"].InnerText);
                        
            return session;

        }
                 
        public void DeleteSession(Session session)
        {
            string xPath = String.Format("UnitTestData/Sessions/Session[@Identifier='{0}']", session.Identifier.ToString());
            XmlNode objNode = XmlData.SelectSingleNode(xPath);
            XmlData.RemoveChild(objNode);
        }

        public void DeleteSessions(User user)
        {
            string xPath = String.Format("UnitTestData/Sessions/Session[@User='{0}']", user.Username);
            XmlNodeList objNodes = XmlData.SelectNodes(xPath);
            foreach (XmlNode objNode in objNodes)
            {
                XmlData.RemoveChild(objNode);
            }            
        }

        public User PersistUserAuthentication(User user)
        {
            UpdatePassword(user, user.Password);
            return this.GetUser(user.Username);
        }

        public User[] GetGatekeepers(Guid contractId)
        {
            List<User> users = new List<User>();
            string xPath = String.Format("UnitTestData/Users/User[@ContractID = '{0}']", contractId);
            XmlNodeList objNodes = XmlData.SelectNodes(xPath);
            foreach (XmlNode objNode in objNodes)
            {
                User user = GetUser(objNode.Attributes["Username"].InnerText);
                if (user.IsGatekeeper)
                {
                    users.Add(user);
                }
            }

            return users.ToArray();
        }

        public Signature GetSignature(Guid signatureId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool IsUserSignInLocked(User user, int maxLoginAttempts, int signInResetMinutes)
        {
            bool isMaxAttemptsReached = user.Password.SignInAttempts >= maxLoginAttempts;

            if (isMaxAttemptsReached)
            {
                return DateTime.Now < user.Password.LastSignInAttempt.AddMinutes(signInResetMinutes);
            }
            else
            {
                return false;
            }
        }  

        public void UpdateSignInCredentials(string username, bool p)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public Signature CreateSignature(Signature signature)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public Signature UpdateSignature(Signature signature)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        private Password GetPassword(User user)
        {
            string xPath = String.Format("UnitTestData/Users/User[@Username = '{0}']/Password", user.Username);
            XmlNode objectNode = XmlData.SelectSingleNode(xPath);

            DateTime lastSignIn = DateTime.Parse(objectNode.Attributes["LastSignInAttempt"].InnerText);
            int signInAttempts = int.Parse(objectNode.Attributes["SignInAttempts"].InnerText);
            string hash = objectNode.Attributes["PasswordHash"].InnerText;
            string salt = objectNode.Attributes["Salt"].InnerText;

            Password password = new Password(hash, salt);
            password.LastSignInAttempt = lastSignIn;
            password.SignInAttempts = signInAttempts;

            return password;
        }

        private void UpdatePassword(User user, Password password)
        {
            string xPath = String.Format("UnitTestData/Users/User[@Username = '{0}']/Password", user.Username);
            XmlNode objectNode = XmlData.SelectSingleNode(xPath);

            if (objectNode != null)
            {

                XmlAttribute lastSignInAttemptAttribute = XmlData.CreateAttribute("LastSignInAttempt");
                lastSignInAttemptAttribute.InnerText = password.LastSignInAttempt.ToString();

                XmlAttribute signInAttemptsAttribute = XmlData.CreateAttribute("SignInAttempts");
                signInAttemptsAttribute.InnerText = password.SignInAttempts.ToString();

                XmlAttribute hashAttribute = XmlData.CreateAttribute("PasswordHash");
                hashAttribute.InnerText = password.PasswordHash;

                XmlAttribute saltAttribute = XmlData.CreateAttribute("Salt");
                saltAttribute.InnerText = password.Salt.ToString();

                XmlAttribute ran1 = objectNode.Attributes["LastSignInAttempt"];
                XmlAttribute ran2 = objectNode.Attributes["SignInAttempts"];
                XmlAttribute ran3 = objectNode.Attributes["PasswordHash"];
                XmlAttribute ran4 = objectNode.Attributes["Salt"];

                objectNode.Attributes.Remove(ran1);
                objectNode.Attributes.Remove(ran2);
                objectNode.Attributes.Remove(ran3);
                objectNode.Attributes.Remove(ran4);

                objectNode.Attributes.Append(lastSignInAttemptAttribute);
                objectNode.Attributes.Append(signInAttemptsAttribute);
                objectNode.Attributes.Append(hashAttribute);
                objectNode.Attributes.Append(saltAttribute);
            }
        }

        


        public User[] GetUsers(Guid contractId)
        {
            List<User> users = new List<User>();
            string xPath = String.Format("UnitTestData/Users/User[@ContractID = '{0}']", contractId);
            XmlNodeList objectNodes = XmlData.SelectNodes(xPath);

            if (objectNodes == null)
            {
                return null;
            }
            else
            {
                foreach (XmlNode objectNode in objectNodes)
                {
                    User user = GetUser(objectNode.Attributes["Username"].InnerText);
                    if (user != null)
                    {
                        users.Add(user);
                    }
                }
            }

            return users.ToArray();
        }

        public User[] GetActiveUsersByEmail(string email)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public User[] GetActiveUsersByEmailAndService(string email, int[] serviceIds)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool DeactivateUser(string username, Guid contractId)
        {
            User user = this.GetUser(username);

            if (user.ContractID == contractId)
            {
                user.IsAccountActive = false;
                this.PersistUser(user);
                return true;
            }

            return false;
        }

        public Contract CreateContract(Guid contractIdentifier, string contractName, bool contractStatus, DateTime contractStartDate, DateTime contractEndDate, Guid companyIdentifier, bool isSSOContract, bool contractIPRestrict, string contractSalesContact, string username, string serviceIDs, bool contractServiceStatus)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<User> GetSharedUsers(Guid contractId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public string GetSessionKeyValue(Session session, string keyName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool InsertSessionKeyValue(Session session, string keyName, string keyValue)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ContractService[] GetContractServicesByIPAddress(long ipAddress)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ContractService[] GetActiveContractServicesByIPAddress(long ipAddress)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ContractService[] GetContractServicesByUsername(string username)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ContractService[] GetActiveContractServicesByUsername(string username)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ContractService[] GetActiveContractServicesByUserGUID(Guid userGUID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public Contract[] GetContractByUsername(string username)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public string GetUserInterests(Guid   userId, int serviceId)
        {
            throw new NotImplementedException();
        }

        public void SetUserInterests(Guid   userId, int serviceId, string interests)
        {
            throw new NotImplementedException();
        }

        public Company CreateCompany(
            Guid companyId,
            string name,
            string tradingPartnerNumber,
            string adminContact,
            string receiver,
            string catalogSet,
            string changedBy,
            string purchasingManager,
            DateTime lastChanged,
            DateTime created,
            string type,
            string status,
            string address,
            string city,
            string state,
            string country,
            string postCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

    }

}