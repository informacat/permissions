using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Datamonitor.DataModel.Utilities
{
    public static class ModelConfiguration
    {
        private const string REPOSITORYPROVIDER_EXCEPTION_MESSAGE =
            "The application configuration does not declare an application setting for \"Datamonitor.DataModel.Providers.RepositoryProvider\"";

        private const string CONNECTIONSTRING_EXCEPTION_MESSAGE =
            "The application configuration does not declare a connection string setting for \"Datamonitor.DataModel.ConnectionString\"";

        public static string RepositoryProvider
        {
            get
            {
                string parameter = ConfigurationManager.AppSettings["Datamonitor.DataModel.Providers.RepositoryProvider"];
                if (parameter != null)
                {
                    return parameter;
                }
                else
                {
                    throw new ApplicationException(REPOSITORYPROVIDER_EXCEPTION_MESSAGE);
                }
                
            }
        }

        public static string ConnectionString
        {
            get
            {
                if (ConfigurationManager.ConnectionStrings["Datamonitor.DataModel.ConnectionString"] != null)
                {
                    return ConfigurationManager.ConnectionStrings["Datamonitor.DataModel.ConnectionString"].ConnectionString;
                }
                else
                {
                    throw new ApplicationException(CONNECTIONSTRING_EXCEPTION_MESSAGE);
                }
                
            }
        }

    }
}
