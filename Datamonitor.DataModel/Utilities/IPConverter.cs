using System;

namespace Datamonitor.DataModel.Utilities
{
    /// <summary>
    /// Utility class for handling IP address conversion.
    /// </summary>
    public static class IPConverter
    {
        /// <summary>
        /// Parse the given ip address to a number by shifting the segments by
        /// powers of 1000.
        /// </summary>
        /// <param name="ipAddress">An ip address in the form
        /// "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}" </param>
        /// <returns>A long number representing the address.</returns>
        public static long ParseIP(string ipAddress)
        {
            string[] ipAddressSections = ipAddress.Split(new char[1] { '.' });
            long[] multipliers = { 1000000000,
                                   1000000,
                                   1000,
                                   1
                                 };
            long ipAddressTotal = 0;

            for (int i = 0; i < ipAddressSections.Length; i++)
            {
                string ipComp = ipAddressSections[i];
                long multiplier = multipliers[i];
                long outIPComp;

                if (long.TryParse(ipComp, out outIPComp))
                {
                    ipAddressTotal += multiplier * outIPComp;
                }
            }

            return ipAddressTotal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public static string ParseIP(long ipAddress)
        {
            //127000000001 := 127.0.0.1
            // 10034144151 := 10.34.144.151

            string[] ipAddressV4 = new string[4] { "0", "0", "0", "0" };

            decimal[] dividers = { 1000000000,
                                   1000000,
                                   1000,
                                   1
                                 };
            decimal x = Convert.ToDecimal(ipAddress);

            for(int i = 0; i<dividers.Length;i++)
            {
                decimal divider = dividers[i];

                // divider = 1000000000
                decimal a = x / divider; // a = 10.034144151
                decimal c = decimal.Truncate(a); // c = 10
                decimal d = divider * c; // d = 10,000,000,000
                x = x - d; // x = 34144151
                
                ipAddressV4[i] = string.Format("{0}", c);
            }

            return string.Join(".", ipAddressV4);
        }

    }
}
