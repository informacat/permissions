using System;
using System.Collections.Generic;
using System.Text;

namespace Datamonitor.DataModel.Structures
{
    /// <summary>
    /// Represents a pair of ip addresses in 12 digit numbers as an upper and lower bound range
    /// </summary>
    public struct IPAddressRange
    {
        /// <summary>
        /// Upper bound internet protocol address
        /// </summary>
        public long UpperBound;
        /// <summary>
        /// Lower bound internet protocol address
        /// </summary>
        public long LowerBound;

        /// <summary>
        /// Returns true if the given parameter is in the struct's range
        /// </summary>
        /// <param name="ipAddress">12 digit int64</param>
        /// <returns>boolean if in range</returns>
        public bool IsBetween(long ipAddress)
        {
            return ipAddress >= LowerBound && ipAddress <= UpperBound;
        }
    }
}
