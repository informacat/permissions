using System;
using System.Collections.Generic;
using System.Text;

namespace Datamonitor.DataModel.Structures
{
    /// <summary>
    /// Represents a pair of dates as an upper and lower bound range
    /// </summary>
    public struct DateRange
    {
        /// <summary>
        /// Upper bound date
        /// </summary>
        public DateTime Start;
        /// <summary>
        /// Lower bound date
        /// </summary>
        public DateTime End;

        /// <summary>
        /// Returns true if the given parameter is in the struct's range
        /// </summary>
        /// <param name="ipAddress">date</param>
        /// <returns>boolean if in range</returns>
        public bool IsBetween(DateTime dateTime)
        {
            return dateTime <= End && dateTime >= Start;
        }
    }
}
